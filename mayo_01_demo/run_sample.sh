#!/bin/sh
# ------------------------------------------------------------------------------
# get pathname of current directory
my_pathname=`pwd`

# get current directory name from pathname
my_basename=$(basename "$my_pathname")

# get current date time
# my_rundate=$(date '+%Y-%m-%d-%H.%M.%S')
my_rundate=$(date '+%Y-%m-%d-%H%M')

# ------------------------------------------------------------------------------
# generate log filename
my_logname="$my_basename"_"$my_rundate".log

# ------------------------------------------------------------------------------
# initialize log
echo "Rundate: $my_rundate"  | tee -a $my_logname

echo "pathname=$my_pathname" | tee -a $my_logname

echo "basename=$my_basename" | tee -a $my_logname
echo "==================================" | tee -a  $my_logname
echo "Job description:" | tee -a $my_logname
echo "----------------------------------" | tee -a  $my_logname
cat sample_desc.txt | tee -a $my_logname
echo "==================================" | tee -a  $my_logname
echo "run_info.txt:" | tee -a $my_logname
echo "----------------------------------" | tee -a  $my_logname
if [ -e "sample_config/run_info.txt" ]
then 
  cat sample_config/run_info.txt >> $my_logname
else
  echo "ERROR not found" | tee -a $my_logname
fi
echo "==================================" | tee -a  $my_logname
echo "sample_info.txt:" | tee -a $my_logname
echo "----------------------------------" | tee -a  $my_logname
if [ -e "sample_config/sample_info.txt" ]
then 
  cat sample_config/sample_info.txt >> $my_logname
else
  echo "ERROR not found" | tee -a $my_logname
fi
echo "----------------------------------" | tee -a  $my_logname
echo "==================================" | tee -a  $my_logname
echo "tool_info_hg19.txt:" | tee -a $my_logname
echo "----------------------------------" | tee -a  $my_logname
if [ -e "sample_config/tool_info_hg19.txt" ]
then 
  cat sample_config/tool_info_hg19.txt >> $my_logname
else
  echo "ERROR not found" | tee -a $my_logname
fi
# ------------------------------------------------------------------------------
# start job
../miRNA/CAP-miRseq.sh sample_config/run_info.txt 2>&1 | tee -a $my_logname
# ------------------------------------------------------------------------------
# Preview log file
# extract main log messages from .log file into .tsv file
cat $my_logname | grep '^XXXXX' > $my_logname.tsv
# open log.tsv file with spreadsheet program in separate process so I can quit
libreoffice -o $my_logname.tsv &
# ------------------------------------------------------------------------------
# done