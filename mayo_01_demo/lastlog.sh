#!/bin/sh
# list *.log, latest first, get name of first file
filename=$( ls -1r *.log | head -1 )
# make sure filename exists
if [ -e $filename ]
then
	cat $filename | grep '^XXXXX' > $filename.tsv
	libreoffice -o $filename.tsv &
else
	echo error creating $filename
	echo
fi