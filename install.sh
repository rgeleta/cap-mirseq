#!/bin/bash

set -u 
set -e


INSTALL_SCRIPT=$(readlink -f $0)
INSTALL_DIRECTORY=`dirname "$INSTALL_SCRIPT"`
SCRIPT_DIRECTORY="$INSTALL_DIRECTORY/scripts"
CONFIG_DIRECTORY="$INSTALL_DIRECTORY/sample_config"
SAMPLE_DIRECTORY="$INSTALL_DIRECTORY/sample_data"
REF_DIRECTORY="$INSTALL_DIRECTORY/small_ref"


# Software Sources

SQUID_URL="ftp://selab.janelia.org/pub/software/squid/squid-1.9g.tar.gz"
SQUID_ARCHIVE=`basename "$SQUID_URL"`
SQUID_BUILD_DIR=`basename "$SQUID_ARCHIVE" .tar.gz`

RANDFOLD_URL="http://bioinformatics.psb.ugent.be/supplementary_data/erbon/nov2003/downloads/randfold-2.0.tar.gz"
RANDFOLD_ARCHIVE=`basename "$RANDFOLD_URL"`
RANDFOLD_BUILD_DIR=`basename "$RANDFOLD_ARCHIVE" .tar.gz`

BOWTIE_URL="http://downloads.sourceforge.net/project/bowtie-bio/bowtie/0.12.7/bowtie-0.12.7-linux-x86_64.zip"
BOWTIE_ARCHIVE=`basename "$BOWTIE_URL"`
BOWTIE_BUILD_DIR="bowtie-0.12.7"

MIRDEEP_URL="https://www.mdc-berlin.de/38350089/en/research/research_teams/systems_biology_of_gene_regulatory_elements/projects/miRDeep/mirdeep2_0_0_5.zip"
MIRDEEP_ARCHIVE=`basename "$MIRDEEP_URL"`
MIRDEEP_BUILD_DIR="mirdeep2"

#VIENNA_RNA_URL="http://www.tbi.univie.ac.at/~ivo/RNA/ViennaRNA-1.8.4.tar.gz"
VIENNA_RNA_URL="http://www.tbi.univie.ac.at/RNA/packages/source/ViennaRNA-1.8.4.tar.gz"
VIENNA_RNA_ARCHIVE=`basename "$VIENNA_RNA_URL"`
VIENNA_RNA_BUILD_DIR=`basename "$VIENNA_RNA_ARCHIVE" .tar.gz`

PDFAPI2_URL="http://search.cpan.org/CPAN/authors/id/A/AR/AREIBENS/PDF-API2-0.73.tar.gz"
PDFAPI2_ARCHIVE=`basename "$PDFAPI2_URL"`
PDFAPI2_BUILD_DIR=`basename "$PDFAPI2_ARCHIVE" .tar.gz`

BEDTOOLS_URL="http://bedtools.googlecode.com/files/BEDTools.v2.17.0.tar.gz"
BEDTOOLS_ARCHIVE=`basename "$BEDTOOLS_URL"`
BEDTOOLS_BUILD_DIR="bedtools-2.17.0"

FASTQC_URL="http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip"
FASTQC_ARCHIVE=`basename "$FASTQC_URL"`
FASTQC_BUILD_DIR="FastQC"

HTSEQ_URL="https://pypi.python.org/packages/source/H/HTSeq/HTSeq-0.5.4p5.tar.gz"
HTSEQ_ARCHIVE=`basename "$HTSEQ_URL"`
HTSEQ_BUILD_DIR=`basename "$HTSEQ_ARCHIVE" .tar.gz`

SAMTOOLS_URL="http://downloads.sourceforge.net/project/samtools/samtools/0.1.19/samtools-0.1.19.tar.bz2"
SAMTOOLS_ARCHIVE=`basename "$SAMTOOLS_URL"`
SAMTOOLS_BUILD_DIR=`basename "$SAMTOOLS_ARCHIVE" .tar.bz2`

CUTADAPT_URL="http://cutadapt.googlecode.com/files/cutadapt-0.9.5.tar.gz"
CUTADAPT_ARCHIVE=`basename "$CUTADAPT_URL"`
CUTADAPT_BUILD_DIR=`basename "$CUTADAPT_ARCHIVE" .tar.gz`

PICARD_URL="http://downloads.sourceforge.net/project/picard/picard-tools/1.77/picard-tools-1.77.zip"
PICARD_ARCHIVE=`basename "$PICARD_URL"`
PICARD_BUILD_DIR=`basename "$PICARD_ARCHIVE" .zip`

VCFTOOLS_URL="http://downloads.sourceforge.net/project/vcftools/vcftools_0.1.11.tar.gz"
VCFTOOLS_ARCHIVE=`basename "$VCFTOOLS_URL"`
VCFTOOLS_BUILD_DIR=`basename "$VCFTOOLS_ARCHIVE" .tar.gz`



# Exit Trapping

completeCheck() { 
	EXITCODE=$?

	if [ ! $EXITCODE == 0 ] ; then 
		set +x
		echo ""
		echo "Install Failed."
		echo "---------------------------------------------------------------------------------------------------------"
		echo "Please examine the output above to identify the cause."
		echo "If you're uncertain about how to interpret the error message, please forward it bockol.matthew@mayo.edu"
		echo "and we'll attempt to help you identify the problem."
		echo ""
	fi

	exit $EXITCODE
}

trap completeCheck EXIT




# Usage and command line parsing

usage() { 
	echo ""
	echo "$0 -p /path/to/install"
	echo "  -p the install directory for mirdeep and it's dependencies."
	echo "  -h this usage description"
	echo ""
	exit 0
}

PREFIX=''
SOURCES=''
BUILD=''

while getopts ":p:h:" o; do
	case "${o}" in
		p)
			PREFIX=`readlink -f "${OPTARG}"`
			SOURCES="$PREFIX/sources"
			BUILD="$PREFIX/build"
			;;
		*)
			usage
			;;
	esac
done

set +e
if [ -z "$PREFIX" ] ; then 
	usage
fi
set -e 





# Create the install directory structure 

# attempt the create install path

if [ ! -d $PREFIX ] ; then 
	set -x
	`mkdir "$PREFIX"`
	set +x
fi 

# create the source directory

if [ ! -d "$SOURCES" ] ; then 
	set -x
	`mkdir "$SOURCES"`
	set +x
fi 

# create the build directory

if [ ! -d "$BUILD" ] ; then 
	set -x
	`mkdir "$BUILD"`
	set +x
fi





# Check base dependencies

# checking for WGET
set +e
WGET=`which wget`
set -e
if [ ! -x "$WGET" ]; then
	echo "Unable to find wget in your PATH"
	exit 1
fi

# checking for UNZIP
set +e
UNZIP=`which unzip`
set -e
if [ ! -x "$UNZIP" ]; then
	echo "Unable to find unzip in your PATH"
	exit 1
fi

# checking for GCC
set +e
CC=`which gcc`
set -e
if [ ! -x "$CC" ]; then
	echo "Unable to find GCC in your PATH"
	exit 1
fi

# checking for G++ 
set +e
CPP=`which g++`
set -e
if [ ! -x "$CPP" ]; then
	echo "Unable to find G++ in your PATH"
	exit 1
fi

# checking for make
set +e
MAKE=`which make`
set -e
if [ ! -x "$MAKE" ]; then
	echo "Unable to find make in your PATH"
	exit 1
fi

# checking for java
set +e
JAVA=`which java`
set -e
if [ ! -x "$JAVA" ]; then 
	echo "Unable to find java in your PATH"
	exit 1
fi





# Download Software

if [ ! -e "$SOURCES/$RANDFOLD_ARCHIVE" ] ; then 
	echo -n "Downloading Randfold - "
	$WGET --directory-prefix="$SOURCES" -nc "$RANDFOLD_URL"
fi 

if [ ! -e "$SOURCES/$BOWTIE_ARCHIVE" ] ; then 
	echo -n "Downloading Bowtie - "
	$WGET --directory-prefix="$SOURCES" -nc "$BOWTIE_URL"
fi 

if [ ! -e "$SOURCES/$SQUID_ARCHIVE" ] ; then 
	echo -n "Downloading Squid - "
	$WGET --directory-prefix="$SOURCES" -nc "$SQUID_URL"
	echo ""
fi 

if [ ! -e "$SOURCES/$VIENNA_RNA_ARCHIVE" ] ; then 
	echo -n "Downloading ViennaRNA - "
	$WGET --directory-prefix="$SOURCES" -nc "$VIENNA_RNA_URL"
fi 

if [ ! -e "$SOURCES/$MIRDEEP_ARCHIVE" ] ; then 
	echo -n "Downloading Mirdeep - "
	$WGET --directory-prefix="$SOURCES" -nc "$MIRDEEP_URL"
fi 

if [ ! -e "$SOURCES/$PDFAPI2_ARCHIVE" ] ; then 
	echo -n "Downloading PDF::API2 - "
	$WGET --directory-prefix="$SOURCES" -nc "$PDFAPI2_URL"
fi 

if [ ! -e "$SOURCES/$BEDTOOLS_ARCHIVE" ] ; then 
	echo -n "Downloading BedTools - "
	$WGET --directory-prefix="$SOURCES" -nc "$BEDTOOLS_URL"
fi 

if [ ! -e "$SOURCES/$FASTQC_ARCHIVE" ] ; then 
	echo -n "Downloading FastQC - "
	$WGET --directory-prefix="$SOURCES" -nc "$FASTQC_URL"
fi 

if [ ! -e "$SOURCES/$HTSEQ_ARCHIVE" ] ; then 
	echo -n "Downloading HTSeq - "
	$WGET --no-check-certificate --directory-prefix="$SOURCES" -nc "$HTSEQ_URL"
fi 

if [ ! -e "$SOURCES/$SAMTOOLS_ARCHIVE" ] ; then 
	echo -n "Downloading Samtools - "
	$WGET --directory-prefix="$SOURCES" -nc "$SAMTOOLS_URL"
fi 

if [ ! -e "$SOURCES/$CUTADAPT_ARCHIVE" ] ; then 
	echo -n "Downloading CutAdapt - "
	$WGET --directory-prefix="$SOURCES" -nc "$CUTADAPT_URL"
fi 

if [ ! -e "$SOURCES/$PICARD_ARCHIVE" ] ; then 
	echo -n "Downloading Picard - "
	$WGET --directory-prefix="$SOURCES" -nc "$PICARD_URL"
fi 

if [ ! -e "$SOURCES/$VCFTOOLS_ARCHIVE" ] ; then 
	echo -n "Downloading VCFTools - "
	$WGET --directory-prefix="$SOURCES" -nc "$VCFTOOLS_URL"
fi 


# Unpack Archives

if [ ! -d "$BUILD/$RANDFOLD_BUILD_DIR" ] ; then 
	set -x
	tar xvz --directory "$BUILD" -f "$SOURCES/$RANDFOLD_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$BOWTIE_BUILD_DIR" ] ; then 
	set -x
	"$UNZIP" -q -d "$BUILD" "$SOURCES/$BOWTIE_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$SQUID_BUILD_DIR" ] ; then 
	set -x
	tar xvz --directory "$BUILD" -f "$SOURCES/$SQUID_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$VIENNA_RNA_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$VIENNA_RNA_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$PDFAPI2_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$PDFAPI2_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$MIRDEEP_BUILD_DIR" ] ; then 
	set -x
	"$UNZIP" -q -d "$BUILD" "$SOURCES/$MIRDEEP_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$BEDTOOLS_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$BEDTOOLS_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$FASTQC_BUILD_DIR" ] ; then 
	set -x
	"$UNZIP" -q -d "$BUILD" "$SOURCES/$FASTQC_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$HTSEQ_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$HTSEQ_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$SAMTOOLS_BUILD_DIR" ] ; then 
	set -x
	tar xj --directory "$BUILD" -f "$SOURCES/$SAMTOOLS_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$CUTADAPT_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$CUTADAPT_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$PICARD_BUILD_DIR" ] ; then 
	set -x
	"$UNZIP" -q -d "$BUILD" "$SOURCES/$PICARD_ARCHIVE"
	set +x
fi

if [ ! -d "$BUILD/$VCFTOOLS_BUILD_DIR" ] ; then 
	set -x
	tar xz --directory "$BUILD" -f "$SOURCES/$VCFTOOLS_ARCHIVE"
	set +x
fi


# Apply patches

# RANDFOLD
if [ ! -e "$BUILD/$RANDFOLD_BUILD_DIR/Makefile.original" ] ; then 
	NEW_INCLUDE=`echo "-I. -I${PREFIX}/include -L${PREFIX}/lib" | sed -r "s/\//\\\//g"`
	set -x
	cp "$BUILD/$RANDFOLD_BUILD_DIR/Makefile" "$BUILD/$RANDFOLD_BUILD_DIR/Makefile.original"
	echo "perl -pi -e \"s|INCLUDE=-I.|INCLUDE=${NEW_INCLUDE}|\" \"$BUILD/$RANDFOLD_BUILD_DIR/Makefile\""
	perl -pi -e "s|INCLUDE=-I.|INCLUDE=${NEW_INCLUDE}|" "$BUILD/$RANDFOLD_BUILD_DIR/Makefile"
	set +x
fi


# VIENNA RNA

# sprintf bug in RNAforrester
if [ ! -e "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp.orig" ] ; then 
	echo "#include <stdio.h>" > "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp.new"
	set -x
	cat "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp" >> "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp.new"
	mv "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp" "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp.orig"
	mv "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp.new" "$BUILD/$VIENNA_RNA_BUILD_DIR/RNAforester/src/rnafuncs.cpp"
	set +x
fi

# perl module install prefix bug
if [ ! -e "$BUILD/$VIENNA_RNA_BUILD_DIR/Perl/Makefile.PL.orig" ] ; then 
	set -x
	cp "$BUILD/$VIENNA_RNA_BUILD_DIR/Perl/Makefile.PL" "$BUILD/$VIENNA_RNA_BUILD_DIR/Perl/Makefile.PL.orig"
	perl -pi -e "s|\);|, INSTALL_BASE => \"$PREFIX\" |" "$BUILD/$VIENNA_RNA_BUILD_DIR/Perl/Makefile.PL"
	echo ");" >> "$BUILD/$VIENNA_RNA_BUILD_DIR/Perl/Makefile.PL"
	set +x
fi
 




# Build and Install Software

# SQUID
if [ ! -e "$BUILD/$SQUID_BUILD_DIR/Makefile" ] ; then 
	set -x
	cd "$BUILD/$SQUID_BUILD_DIR"
	"$BUILD/$SQUID_BUILD_DIR/configure" --prefix="$PREFIX"
	set +x
fi

if [ ! -e "$BUILD/$SQUID_BUILD_DIR/seqsplit" ] ; then 
	set -x
	cd "$BUILD/$SQUID_BUILD_DIR"
	make -f "$BUILD/$SQUID_BUILD_DIR/Makefile"
	set +x
fi 

if [ ! -e "$PREFIX/bin/seqsplit" ] ; then 
	set -x
	cd "$BUILD/$SQUID_BUILD_DIR"
	make -f "$BUILD/$SQUID_BUILD_DIR/Makefile" install 
	set +x
fi


# Bowtie
if [ ! -e "$BUILD/$BOWTIE_BUILD_DIR/bowtie" ] ; then 
	echo "Bowtie install failed. Missing \"$BUILD/$BOWTIE_BUILD_DIR/bin/bowtie\"."
fi

if [ ! -e "$PREFIX/bin/bowtie" ] ; then 
	set -x
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie" "$PREFIX/bin"
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie-build" "$PREFIX/bin"
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie-build-debug" "$PREFIX/bin"
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie-debug" "$PREFIX/bin"
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie-inspect" "$PREFIX/bin"
	cp -rav "$BUILD/$BOWTIE_BUILD_DIR/bowtie-inspect-debug" "$PREFIX/bin"
	set +x
fi


# Randfold
if [ ! -e "$BUILD/$RANDFOLD_BUILD_DIR/randfold" ] ; then 
	set -x
	cd "$BUILD/$RANDFOLD_BUILD_DIR"
	make 
	set +x
fi 

if [ ! -e "$PREFIX/bin/randfold" ] ; then 
	set -x
	cd "$BUILD/$RANDFOLD_BUILD_DIR"
	cp "$BUILD/$RANDFOLD_BUILD_DIR/randfold" "$PREFIX/bin"
	set +x
fi 


# PDF::API2
if [ ! -e "$BUILD/$PDFAPI2_BUILD_DIR/Makefile" ] ; then 
	set -x
	cd "$BUILD/$PDFAPI2_BUILD_DIR"
	perl Makefile.PL INSTALL_BASE="$PREFIX"
	set +x
fi 

if [ ! -e "$PREFIX/lib/perl5/PDF/API2.pm" ] ; then 
	set -x
	cd "$BUILD/$PDFAPI2_BUILD_DIR"
	make install
	set +x
fi 


# ViennaRNA
if [ ! -e "$BUILD/$VIENNA_RNA_BUILD_DIR/Makefile" ] ; then 
	set -x
	cd "$BUILD/$VIENNA_RNA_BUILD_DIR"
	./configure --prefix="$PREFIX"
	set +x
fi

if [ ! -e "$BUILD/$VIENNA_RNA_BUILD_DIR/Progs/RNAfold" ] ; then 
	set -x
	cd "$BUILD/$VIENNA_RNA_BUILD_DIR"
	make 
	set +x
fi 


if [ ! -e "$PREFIX/bin/Kinfold" ] ; then 
 echo "kinfold"
fi

if [ ! -e "$PREFIX/bin/RNAforester" ] ; then 
 echo "rnaforester"
fi 

if [ ! -d "$PREFIX/include/ViennaRNA" ] ; then 
 echo "viennarna"
fi

if [ ! -e "$PREFIX/bin/Kinfold" ] || [ ! -e "$PREFIX/bin/RNAforester" ] || [ ! -d "$PREFIX/include/ViennaRNA" ] ; then 
	set -x
	cd "$BUILD/$VIENNA_RNA_BUILD_DIR"
	make install
	set +x
fi


# Mirdeep

if [ ! -e "$PREFIX/bin/mirdeep2bed.pl" ] ; then 
	set -x
	cp -rav "$BUILD/$MIRDEEP_BUILD_DIR/"* "$PREFIX/bin"
	set +x
fi


# BedTools
if [ ! -e "$BUILD/$BEDTOOLS_BUILD_DIR/bin/bedtools" ] ; then 
	set -x
	cd "$BUILD/$BEDTOOLS_BUILD_DIR"
	make
	set +x
fi 

if [ ! -e "$PREFIX/bin/bedtools" ] ; then 
	set -x
	cd "$BUILD/$BEDTOOLS_BUILD_DIR"
	cp -v "$BUILD/$BEDTOOLS_BUILD_DIR/bin/"* "$PREFIX/bin/" 
	set +x
fi

# FASTQC
if [ ! -e "$PREFIX/bin/fastqc" ] ; then 
	set -x
	cd "$BUILD/$FASTQC_BUILD_DIR"
	cp -rav "$BUILD/$FASTQC_BUILD_DIR/"* "$PREFIX/bin/"
	chmod +x "$PREFIX/bin/fastqc"
	set +x
fi 


# HTSeq
if [ ! -e "$PREFIX/bin/htseq-count" ] ; then 
	set -x
	cd "$BUILD/$HTSEQ_BUILD_DIR"
	python setup.py install --user
	cp ~/.local/bin/htseq-count "$PREFIX/bin"
	cp ~/.local/bin/htseq-qa "$PREFIX/bin"
	set +x
fi

if [ ! -e "$PREFIX/bin/htseq-count" ] ; then 
	echo "HTSeq install failed. Missing \"$PREFIX/bin/htseq-count\"."
fi


# Samtools
if [ ! -e "$BUILD/$SAMTOOLS_BUILD_DIR/samtools" ] ; then 
	set -x 
	cd "$BUILD/$SAMTOOLS_BUILD_DIR"
	make 
	set +x
fi

if [ ! -e "$PREFIX/bin/samtools" ] ; then 
	set -x
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/samtools" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/bcftools/bcftools" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/ace2sam" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/bamcheck" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/"*.pl "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/"*.lua "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/maq2sam-long" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/maq2sam-short" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/md5fa" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/md5sum-lite" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/plot-bamcheck" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/varfilter.py" "$PREFIX/bin"
	cp -rav "$BUILD/$SAMTOOLS_BUILD_DIR/misc/wgsim" "$PREFIX/bin"
	set +x
fi


# Cutadapt
if [ ! -e "$PREFIX/bin/cutadapt" ] ; then 
	set -x
	cd "$BUILD/$CUTADAPT_BUILD_DIR"
	python setup.py install --user
	cp ~/.local/bin/cutadapt "$PREFIX/bin"
	set +x
fi

if [ ! -e "$PREFIX/bin/cutadapt" ] ; then 
	echo "CutAdapt install failed. Missing \"$PREFIX/bin/cutadapt\"."
fi


# Picard
if [ ! -e "$BUILD/$PICARD_BUILD_DIR/SamToFastq.jar" ] ; then
	echo "Picard failed to unpack"
	exit 1
fi

if [ ! -e "$PREFIX/bin/SamToFastq.jar" ] ; then 
	set -x 
	cp -rav "$BUILD/$PICARD_BUILD_DIR/"* "$PREFIX/bin/"
	set +x 
fi


# VCFTools
if [ ! -e "$BUILD/$VCFTOOLS_BUILD_DIR/bin/vcf-concat" ] ; then 
	set -x
	cd "$BUILD/$VCFTOOLS_BUILD_DIR"
	make
	set +x
fi

if [ ! -e "$PREFIX/bin/vcf-concat" ] ; then 
	set -x 
	cd "$BUILD/$VCFTOOLS_BUILD_DIR"
	export PREFIX=$PREFIX
	cp -rav bin/* "$PREFIX/bin"
	make install
	set +x
fi	



chmod ugo+x "$PREFIX/bin/"* 

# Rewrite tool_info.txt 
echo "Rewriting $CONFIG_DIRECTORY/tool_info_hg19.txt"
# tools
perl -pi -e "s|SCRIPT_PATH=.*|SCRIPT_PATH=$SCRIPT_DIRECTORY|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|MIRDEEP2_PATH=.*|MIRDEEP2_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|BOWTIE_PATH=.*|BOWTIE_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|RANDFOLD_PATH=.*|RANDFOLD_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|SQUID_PATH=.*|SQUID_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|VIENNA_PATH=.*|VIENNA_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|PDFAPI2_PM_PATH=.*|PDFAPI2_PM_PATH=$PREFIX/lib/perl5|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|PICARD_PATH=.*|PICARD_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|FASTQC_PATH=.*|FASTQC_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|CUTADAPT_PATH=.*|CUTADAPT_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|SAMTOOLS_PATH=.*|SAMTOOLS_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|BEDTOOLS_PATH=.*|BEDTOOLS_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|VCFTOOLS_PATH=.*|VCFTOOLS_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|VCFTOOLS_PERLLIB=.*|VCFTOOLS_PERLLIB=$PREFIX/lib/perl5/site_perl|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|HTSEQ_PATH=.*|HTSEQ_PATH=$PREFIX/bin|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"

HTSEQLIBEGG=`find ~/.local -type d -name HTSeq-0.5.4p5-py2.6-linux-x86_64.egg`
HTSEQLIB=`dirname "$HTSEQLIBEGG"`
perl -pi -e "s|HTSEQ_LIB_PATH=.*|HTSEQ_LIB_PATH=$HTSEQLIB|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"

# references

perl -pi -e "s|REF_GENOME=.*|REF_GENOME=$REF_DIRECTORY/hg19_chr1.fa|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|BOWTIE_REF=.*|BOWTIE_REF=$REF_DIRECTORY/hg19_chr1|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|MIRBASE_HAIRPIN=.*|MIRBASE_HAIRPIN=$REF_DIRECTORY/hairpin.hsa.dna.fa|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|MIRBASE_MATURE=.*|MIRBASE_MATURE=$REF_DIRECTORY/mature.hsa.dna.fa|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|MIRBASE_GFF=.*|MIRBASE_GFF=$REF_DIRECTORY/mirbase.v19.hsa.chr1.gff3|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"
perl -pi -e "s|GENCODE_GTF=.*|GENCODE_GTF=$REF_DIRECTORY/gencode.v18.annotation.chr1.gtf|" "$CONFIG_DIRECTORY/tool_info_hg19.txt"


# run info
perl -pi -e "s|INPUT_DIR=.*|INPUT_DIR=${INSTALL_DIRECTORY}/sample_data|" "$CONFIG_DIRECTORY/run_info.txt"
perl -pi -e "s|OUTPUT_DIR=.*|OUTPUT_DIR=${INSTALL_DIRECTORY}/sample_output|" "$CONFIG_DIRECTORY/run_info.txt"
perl -pi -e "s|TOOL_INFO=.*|TOOL_INFO=${CONFIG_DIRECTORY}/tool_info_hg19.txt|" "$CONFIG_DIRECTORY/run_info.txt"
perl -pi -e "s|SAMPLE_INFO=.*|SAMPLE_INFO=${CONFIG_DIRECTORY}/sample_info.txt|" "$CONFIG_DIRECTORY/run_info.txt"



echo "Checking..."
"$SCRIPT_DIRECTORY/check_install" -t "$CONFIG_DIRECTORY/tool_info_hg19.txt"

echo "Done Install."

echo ""

echo "You may launch the workflow against the included sample data with:"

echo ""

echo "$SCRIPT_DIRECTORY/CAP-miRseq.sh $CONFIG_DIRECTORY/run_info.txt"

echo ""

echo "Upon completion, results will be available in $INSTALL_DIRECTORY/sample_output"

echo ""

echo ""
