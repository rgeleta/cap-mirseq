#!/bin/bash
# merge stats

############################################################################
## Description:
## Final CAP-miRSeq script to create final reports and send completion email
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <output_dir> - Directory where CAP-miRSeq output files are located
## <script path> - Path to CAP-miRSeq script dir
## <run_info.txt> - CAP-miRSeq run_info.txt config file
##
############################################################################
# -----------------------------------------------------------------------------
# set up log message routine
if [ -z $CAP_MIRNA_LOG_SCRIPT ]
then
	echo "XXXXX Set ENV variable CAP_MIRNA_LOG_SCRIPT=/path/to/logging/script.sh"
	exit 1
fi
log_msg5=$CAP_MIRNA_LOG_SCRIPT
my_name=main_document.sh
# write blank line to initialize output
echo
$log_msg5 $my_name 0.00.000 "starting"

# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then
        use_sge=0
else   
        use_sge=1
fi


if [ $# != 3 ];
then
        echo "usage: <output_dir> <script path> <run_info>";
else
	set -x
	output_dir=$1
	script_path=$2
	run_info=$3
	$log_msg5 $my_name 0046 "arg" "\$1 output_dir"  "$1"
	$log_msg5 $my_name 0047 "arg" "\$2 script_path" "$2"
	$log_msg5 $my_name 0048 "arg" "\$3 run_info"    "$3"

	flowcell=$( cat $run_info | grep -w '^FLOWCELL' | cut -d '=' -f2)
	tool=$( cat $run_info | grep -w '^TOOL' | cut -d '=' -f2)
	call_snvs=$( cat $run_info | grep -w '^CALL_SNVS' | cut -d '=' -f2)
	trim_adapter=$( cat $run_info | grep -w '^TRIM_ADAPTER' | cut -d '=' -f2)
	diff_expression=$( cat $run_info | grep -w '^DIFF_EXPRESSION' | cut -d '=' -f2)
	diff_expression_analyses=$( cat $run_info | grep -w '^DIFF_EXPRESSION_ANALYSES' | cut -d '=' -f2)
	snvs=0
	trim=0
	diff=""

	if [ ! -s $output_dir/SampleSummary.xls ]
	then
#		echo "ERROR : ${output_dir}/SampleSummary.xls is empty!"
		$log_msg5 $my_name 0063 "ERROR" "file empty" "${output_dir}/SampleSummary.xls is empty!"
		fi

	if [ $call_snvs == "YES" ]; then
		snvs=1
	fi
	if [ $trim_adapter == "YES" ]; then
		trim=1
	fi
	if [ $diff_expression == "YES" ]; then
		diff=$diff_expression_analyses
	fi

	# generate IGV session
	$log_msg5 $my_name 0077 "processing" "" "generate IGV session"
	$log_msg5 $my_name 0077 "calling" "perl script" "perl $script_path/create_igv.pl -o $output_dir/igv -r $run_info"
	perl $script_path/create_igv.pl -o $output_dir/igv -r $run_info
	cp $script_path/IGV_Setup.doc $output_dir/igv

	# generate html MainDocument
	$log_msg5 $my_name 0083 "processing" "" "generate MainDocument.html"
	$log_msg5 $my_name 0084 "calling" "perl script" "perl $script_path/main_document.pl $run_info $output_dir/MainDocument.html $output_dir/SampleSummary.xls $snvs $trim $diff"
	perl $script_path/main_document.pl $run_info $output_dir/MainDocument.html $output_dir/SampleSummary.xls $snvs $trim $diff
	cp $script_path/CAP-miRSeq_workflow.png $output_dir

	## check all logs files for error or warnings.
	END=`date`


	# the logs being parsed only exist when executed under SGE
	if [ "$use_sge" = "1" ]; then
		cd $output_dir/logs

		files=`ls -lhrt | awk 'NR>1' |awk -F' ' '{print $NF}' | tr "\n" " "`
		cat $files > $output_dir/LOG

		cat $output_dir/LOG | grep -w '^ERROR ' > $output_dir/errorlog
		cat $output_dir/LOG | grep -w '^WARNING ' > $output_dir/warninglog
		rm $output_dir/LOG

		e_size=`ls -l $output_dir/errorlog | awk '{ print $5 }'`
		w_size=`ls -l $output_dir/warninglog | awk '{ print $5 }'`
		if [ $e_size -le 0 ]
		then
			text="SUCCESS"
		else
			text="ERROR"
		fi
		if [ $w_size -le 0 ]
		then
			text1="with no warnings"
		else
			text1="with warnings"
		fi
	fi


        # Update NGS Portal
        if [ "$use_portal" = "1" ]; then
                $script_path/ngs_portal.sh $run_info Complete
        fi

	# send an email notification if mailx exists
	$log_msg5 $my_name 0124 "processing" "mailx" "send email notification if mailx exists"
	mailx=`which mailx`
	if [ "$mailx" != "" ] ; then
		TMPDIR=$output_dir/
		SUB="$tool workflow completion for RunID ${flowcell} "
		MESG=" ${text} ${text1} $tool workflow completed for ${flowcell} on ${END} and ready for tertiary analysis in ${output_dir} "
		## send the completion email
		TO=`id |awk -F '(' '{print $2}' | cut -f1 -d ')'`
		echo -e "$MESG" | mailx -v -s "$SUB" "$TO"
	else
		$log_msg5 $my_name 0134 "info" "mailx" "not installed"
	fi 

	# 	echo "Done "
	# echo `date`
	$log_msg5 $my_name 0139 "done"

fi
$log_msg5 $my_name 9999 "exiting"
