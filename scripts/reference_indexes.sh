#!/bin/sh
# generate reference indexes if needed

############################################################################
## Description:
## A script that will check to see if the necessary reference index file exist
## if they don't then the script will create them
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <tool_info> - CAP-miRSeq tool_info.txt config file
##
############################################################################

if [ $# != 1 ];
then
	echo "usage: reference_indexes.sh <tool_info>";
	exit 1
fi				
	set -x
	echo `date`
	tool_info=$1
	
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	java=$( cat $tool_info | grep -w '^JAVA_PATH' | cut -d '=' -f2)
	samtools=$( cat $tool_info | grep -w '^SAMTOOLS_PATH' | cut -d '=' -f2)
	bowtie=$( cat $tool_info | grep -w '^BOWTIE_PATH' | cut -d '=' -f2)
	bowtie_ref=$( cat $tool_info | grep -w '^BOWTIE_REF' | cut -d '=' -f2)
	picard=$( cat $tool_info | grep -w '^PICARD_PATH' | cut -d '=' -f2)
	createdictionary_jvm_mem=$( cat $tool_info | grep -w '^CREATEDICTIONARY_JVM_MEM' | sed 's/CREATEDICTIONARY_JVM_MEM=//g')

	if [ ! -s $bowtie_ref.fa.fai ]
	then
		$samtools/samtools faidx $bowtie_ref.fa
		if [ ! -s $bowtie_ref.fa.fai ]
		then
			echo "ERROR : ${bowtie_ref}.fa.fai doesn't exist!"
		fi
	fi


	if [ ! -s $bowtie_ref.1.ebwt ]
	then
		$bowtie/bowtie-build $bowtie_ref.fa $bowtie_ref
		if [ ! -s $bowtie_ref.1.ebwt ]
		then
			echo "ERROR : ${bowtie_ref}.1.ebwt doesn't exist!"
		fi
	fi

	if [ ! -s $bowtie_ref.dict ]
	then
		$java/java $createdictionary_jvm_mem -jar $picard/CreateSequenceDictionary.jar R=$bowtie_ref.fa O=$bowtie_ref.dict
		if [ ! -s $bowtie_ref.dict ]
		then
			echo "ERROR : ${bowtie_ref}.dict doesn't exist!"
		fi
	fi

	echo `date`

