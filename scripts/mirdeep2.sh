#!/bin/bash
# Run miRDeep2

############################################################################
## Description:
## Wrapper script to run miRDeep2 for each sample
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input ARF and FA files are
## <output_dir> - Directory where miRDeep2 output should be written
## <sample names(s1:s2)> - Colon seperated list of sample names
## <reads(fa1:fa2)> - Colon seperated list of FASTA files from miRDeep2 mapper.pl
## <alignments(arf1:arf2)> - Colon seperated list of ARF files from miRDeep2 mapper.pl
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################

# -----------------------------------------------------------------------------
# set up log message routine
log_msg5=$HOME/miRNA/log_msg_le5.sh
my_name=mirdeep2.sh
$log_msg5 $my_name 0000 "starting"
$log_msg5 $my_name 0027 "version" "2015-06-08 09:54"

# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then
        use_sge=0
else   
        use_sge=1
fi

if [ $# != 6 -a "$use_sge" = "1" ]; then
	echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <reads(fa1:fa2)> <alignments (arf1:arf2)> <tool_info>";
elif [ $# != 7 -a "$use_sge" = "0" ] ; then
	echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <reads(fa1:fa2)> <alignments (arf1:arf2)> <tool_info> <sample_number>";
else  
	set -x
	input_dir=$1
	output_dir=$2
	samples=$3
	reads=$4
	alignments=$5
	tool_info=$6

	$log_msg5 $my_name 0048 "arg" "\$1 input_dir  " "$1 "
	$log_msg5 $my_name 0049 "arg" "\$2 output_dir " "$2 "
	$log_msg5 $my_name 0050 "arg" "\$3 samples    " "$3 "
	$log_msg5 $my_name 0051 "arg" "\$4 reads      " "$4 "
	$log_msg5 $my_name 0052 "arg" "\$5 alignments " "$5 "
	$log_msg5 $my_name 0053 "arg" "\$6 tool_info  " "$6 "

        # SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
        if [ "$use_sge" = "1" ]; then
                sample_number=$SGE_TASK_ID
        else    
                sample_number=$7
        fi


	$log_msg5 $my_name 0063 "Retrieving\tparameters\tfrom tool_info "
	mirbase_precursor_seqs=$( cat $tool_info | grep -w '^MIRBASE_HAIRPIN' | cut -d '=' -f2)
	   mirbase_mature_seqs=$( cat $tool_info | grep -w '^MIRBASE_MATURE'  | cut -d '=' -f2)
	            ref_genome=$( cat $tool_info | grep -w '^REF_GENOME'      | cut -d '=' -f2)
	         mirdeep2_path=$( cat $tool_info | grep -w '^MIRDEEP2_PATH'   | cut -d '=' -f2)
	           script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH'     | cut -d '=' -f2)
	            squid_path=$( cat $tool_info | grep -w '^SQUID_PATH'      | cut -d '=' -f2)
	           bowtie_path=$( cat $tool_info | grep -w '^BOWTIE_PATH'     | cut -d '=' -f2)
	           vienna_path=$( cat $tool_info | grep -w '^VIENNA_PATH'     | cut -d '=' -f2)
	         randfold_path=$( cat $tool_info | grep -w '^RANDFOLD_PATH'   | cut -d '=' -f2)
	       pdfapi2_pm_path=$( cat $tool_info | grep -w '^PDFAPI2_PM_PATH' | cut -d '=' -f2)
	         close_species=$( cat $tool_info | grep -w '^MIRDEEP2_CLOSE_SPECIES' | cut -d '=' -f2)
	       mirdeep2_params=$( cat $tool_info | grep -w '^MIRDEEP2_PARAMS' | cut -d '=' -f2)

	export PERL5LIB=$pdfapi2_pm_path:$PERL5LIB
	export PATH=$bowtie_path:$mirdeep2_path:$squid_path:$vienna_path:$randfold_path:$PATH

	# Update NGS Portal
        if [ "$use_portal" = "1" ]; then
                $script_path/ngs_portal.sh $run_info ExpressionCount
        fi
	
	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	collapsed_seqs_fa=$( echo $reads | tr ":" "\n" | head -$sample_number | tail -1 )
	mapped_reads_arf=$( echo $alignments | tr ":" "\n" | head -$sample_number | tail -1 )
	
	# this could be done with 'mkdir -p' but done this way to report it
	if [ ! -d "$output_dir/$sample" ]
	then
		mkdir $output_dir/$sample
	else
		$log_msg5 $my_name 0095 'info' 'dir exists' "$output_dir/$sample"
	fi
		
	cd $output_dir/$sample
	$log_msg5 $my_name 0099 "calling" "perl script" "$script_path/miRDeep2.pl $collapsed_seqs_fa $ref_genome $mapped_reads_arf $mirbase_mature_seqs $close_species $mirbase_precursor_seqs $mirdeep2_params -T $tool_info 2> mirdeep2.log"
	$script_path/miRDeep2.pl $collapsed_seqs_fa $ref_genome $mapped_reads_arf $mirbase_mature_seqs $close_species $mirbase_precursor_seqs $mirdeep2_params -T $tool_info 2> mirdeep2.log
	# the following appears to not be working yet
#	$tmp_cmd="$script_path/miRDeep2.pl $collapsed_seqs_fa $ref_genome $mapped_reads_arf $mirbase_mature_seqs $close_species $mirbase_precursor_seqs $mirdeep2_params -T $tool_info 2> mirdeep2.log"
#	$log_msg5 $my_name 0092 "Running\tcommand:\t$tmp_cmd "
#	$tmp_cmd
		
	$log_msg5 $my_name 0105 "info" "dumping" "mirdeep2.log"
	cat mirdeep2.log

	if [ ! -s miRNAs_expressed_all_samples*.csv ]
	then
		$log_msg5 $my_name 0099 "ERROR" "file empty" "miRNAs_expressed_all_samples is empty!"
		$log_msg5 $my_name 0111 "exiting" "returning" "1"
		exit 1
	fi
	
	# remove mirdeep2 temporary files
	$log_msg5 $my_name 0103 "Processing" "remove" "mirdeep2 temporary files"
	rm -Rf ./dir_prepare_signature*
	rm -Rf ./expression_analyses
	rm -Rf ./mirdeep_runs
	rm $collapsed_seqs_fa
	rm $mapped_reads_arf
	
	$log_msg5 $my_name 0110 "done"
fi	
$log_msg5 $my_name 9999 "exiting"
