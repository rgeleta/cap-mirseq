#!/bin/bash

############################################################################
## Description:
## Classify the aligned reads into different categories based on GENCODE annotation.
## Useful QC for checking the fraction of reads associated with miRNA or other RNA
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input BAMs are located
## <output_dir> - Directory where output table and pie charts should be written
## <sample names(s1:s2)> - Colon seperated list of sample names
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################


# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then
        use_sge=0
else    
        use_sge=1
fi

if [ $# != 4 -a "$use_sge" = "1" ]; then
	echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <tool_info>";
elif [ $# != 5 -a "$use_sge" = "0" ] ; then
        echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <tool_info> <sample_number>";
else  
	set -x
	echo `date`
	input_dir=$1
	output_dir=$2
	samples=$3
	tool_info=$4

        # SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
        if [ "$use_sge" = "1" ]; then
                sample_number=$SGE_TASK_ID
        else    
                sample_number=$5
        fi
	
	ref_genome=$( cat $tool_info | grep -w '^REF_GENOME' | cut -d '=' -f2)
	gencode_gtf=$( cat $tool_info | grep -w '^GENCODE_GTF' | cut -d '=' -f2)
	samtools=$( cat $tool_info | grep -w '^SAMTOOLS_PATH' | cut -d '=' -f2)
	picard=$( cat $tool_info | grep -w '^PICARD_PATH' | cut -d '=' -f2)
	java=$( cat $tool_info | grep -w '^JAVA_PATH' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	htseq=$( cat $tool_info | grep -w '^HTSEQ_PATH' | cut -d '=' -f2)
	htseq_lib_path=$( cat $tool_info | grep -w '^HTSEQ_LIB_PATH' | cut -d '=' -f2)
	python_path=$( cat $tool_info | grep -w '^PYTHON_PATH' | cut -d '=' -f2)
	sortsam_params=$( cat $tool_info | grep -w '^SORTSAM_PARAMS' | sed 's/SORTSAM_PARAMS=//g')
	htseq_params=$( cat $tool_info | grep -w '^HTSEQ_PARAMS' | sed 's/HTSEQ_PARAMS=//g')
	sortsam_jvm_mem=$( cat $tool_info | grep -w '^SORTSAM_JVM_MEM' | sed 's/SORTSAM_JVM_MEM=//g')
	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	
	export PATH=$python_path:$PATH
	export PYTHONPATH=$htseq_lib_path:$PYTHONPATH

	# sort bam by read id
	$java/java $sortsam_jvm_mem \
        -jar $picard/SortSam.jar \
        INPUT=$input_dir/$sample.bam \
        OUTPUT=$output_dir/$sample.queryname.bam \
        SORT_ORDER=queryname \
        TMP_DIR=$output_dir/ \
        $sortsam_params

	if [ ! -s $output_dir/$sample.queryname.bam ]
	then
		echo "ERROR : ${output_dir}/${sample}.queryname.bam is empty!"
	fi
	
	# get gencode gene counts from HTSeq
	$samtools/samtools view $output_dir/$sample.queryname.bam | $htseq/htseq-count $htseq_params - $gencode_gtf > $output_dir/$sample.gencode.genecount.txt

	if [ ! -s $output_dir/$sample.gencode.genecount.txt ]
	then
		echo "ERROR : ${output_dir}/${sample}.gencode.genecount.txt is empty!"
	fi

	# prepare gencode file to match HTSeq output
	cat $gencode_gtf | awk -F"\t" '($3 == "gene"){print}' | awk -F"\"" '{print $2"\t"$6"\t"$8"\t"$10}' | sort -k 1 | paste $output_dir/$sample.gencode.genecount.txt - | awk '($1 == $3){print}' > $output_dir/$sample.gencode.genecount.annotation.txt

	
	# create pie chart
	Rscript $script_path/gencode_piechart.R $output_dir/$sample.gencode.genecount.annotation.txt $sample $output_dir

	if [ ! -s $output_dir/${sample}_gencode_piechart.pdf ]
	then
		echo "ERROR : ${output_dir}/${sample}_gencode_piechart.pdf is empty!"
	fi

	rm $output_dir/$sample.queryname.bam
	rm $output_dir/$sample.gencode.genecount.txt
	rm $output_dir/$sample.gencode.genecount.annotation.txt

	echo `date`
fi	

