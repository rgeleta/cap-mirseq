#!/usr/bin/perl


###########################################################################
## Description:
## Script that will create a report of unique mature miRNA counts. For mature
## miRNA that are found on multiple precursors, they will be summed into one value
##
## Author: Jared Evans
## Date: 5/20/13
##
## Parameters:
## <input raw expression xls> - raw miRNA expression excel file as output by CAP-miRSeq
## <output file> - file name of output excel file
##
############################################################################


use strict;
use warnings;
use Data::Dumper;

if (scalar(@ARGV) != 2)	
{
	die ( "USAGE: uniq_mature_mirna.pl [input raw expression xls] [output xls]\n" );
}


open IN, "$ARGV[0]" or die "opening file: $ARGV[0]";
open OUT, ">", "$ARGV[1]" or die "opening file: $ARGV[1]";

# col positions (0 base)
my $sample_start_col = 3;
my $mature_col = 0;
my %uniq_mature = ();

# create header
my $header = <IN>;
chomp $header;
my @row = split("\t",$header);
my @head = ();
push(@head,$row[$mature_col]);
for(my $i = $sample_start_col; $i < scalar(@row); $i++){
	push(@head,$row[$i]);
}
while(<IN>){
	my $line = $_;
	chomp $line;
	@row = split("\t",$line);
	my $mature = $row[$mature_col];
	my @counts = ();
	for(my $i = $sample_start_col; $i < scalar(@row); $i++){
		push(@counts,$row[$i]);
	}
	for(my $i = 0; $i < scalar(@counts); $i++){
		if(exists $uniq_mature{$mature}){
			$uniq_mature{$mature}[$i]+=$counts[$i];
		}else{
			$uniq_mature{$mature}[$i]=$counts[$i];
		}
	}
}

print OUT join("\t",@head)."\n";
foreach my $m (sort keys %uniq_mature){
	# round counts
	for(my $i = 0; $i < scalar(@{$uniq_mature{$m}}); $i++ ){
		 #$uniq_mature{$m}[$i] = sprintf("%.0f",$uniq_mature{$m}[$i]);
		 $uniq_mature{$m}[$i] = int($uniq_mature{$m}[$i] + 0.5);
	}
	print OUT $m."\t".join("\t",@{$uniq_mature{$m}})."\n";
	#print OUT $m."\t".join("\t",sprintf("%.0f", @{$uniq_mature{$m}}))."\n";
}

close IN;
close OUT;


