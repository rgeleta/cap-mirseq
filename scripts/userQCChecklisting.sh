#!/bin/bash
## Raymond Moore
## miRNA 1.1
## 10.8.13
shopt -s nocasematch
CWD=$(pwd)


read -p "$CWD is a WORK_DIR? " OPT

if [[ !($OPT == "y" || $OPT == "") ]]
then
	echo -e "Incorrect Directory: Quit!\n"
	exit 1
fi

LINESINERRORLOG=`wc -l errorlog | cut -f1 -d' '`
if [[ 0 < $LINESINERRORLOG ]]
then
	cat errorlog;
	echo -e "\n\e[0;31mThere are ERRORS!\e[0m\n\n"
fi

## Look at bams, confirm all samples are aligned.
SAMPNAMES=$(ls bams/ | grep ".bam$" | sed 's/^/\t/')
SAMPNUMBERS=$(ls bams/ | grep ".bam$" | cut -f1 -d'.' | sort -u | wc -l)
echo "I have $SAMPNUMBERS Samples:"
echo -e "$SAMPNAMES\n"

read -p "Do you expect $SAMPNUMBERS dirs in bams/ ? " OPT
if [[ !($OPT == "y" || $OPT == "") ]]
then
	ls bams/ | sed 's/^/\t/';
fi

### Check cutadapted seq files, for non-zero
if [[ -d fastqs/ ]]
then
	echo -e "Has fastq directory!"
	du -hs fastqs/*.cutadapt.fastq | sed 's/^/\t/'
	
	read -p "Are these sizes ok (non-zero)? " OPT
	if [[ !($OPT == "y" || $OPT == "") ]]; then ls -lhS fastqs/ | sed 's/^/\t/'; fi
fi

### Look for empty directories in FastQC dirs
echo -e "Look for empty dirs in \e[1;32m fastqc_pretrim/ \e[0m"
find qc/fastqc_pretrim -empty -type d

echo -e "Look for empty dirs in \e[1;32mfastqc_posttrim/ \e[0m"
find qc/fastqc_posttrim -empty -type d

read -p "Did I find any empty FastQC directories? " OPT
if [[ $OPT == "y" ]]; then echo -e "\n\e[0;31mRequires Manual Fix!\e[0m\n"; exit 1; fi

### TODO
### check all pre & post fastqc ran successfully
### look for peaks around 22bp, in length distribution
### ensure all files in place.


### Match number of samples in mirdeep/ & ensure non-empty.
SAMPNUM=$(ls mirdeep2/| sort -u | wc -l)
read -p "Do you expect $SAMPNUM dirs in mirdeep2/ ? " OPT
if [[ !($OPT == "y" || $OPT == "") ]]
then
	ls mirdeep2/ | sed 's/^/\t/';
fi

echo -e "Look for empty dirs in \e[1;32mmirdeep2/ \e[0m"
find mirdeep2 -empty -type d

read -p "Did I find any empty MirDeep2 directories? " OPT
if [[ $OPT == "y" ]]; then echo -e "\n\e[0;31mRequires Manual Fix!\e[0m\n"; exit 1; fi


## check lines in files in expression/
wc -l expression/*.xls
read -p "Are the number of lines in these files ok? (~2000 mature)" OPT 
if [[ !($OPT == "y" || $OPT == "") ]] ; then exit 1; fi


########
### Look into Delievery Directory more
########
DELIV01=`ls *html`
echo -e "\t\e[1;32m > $DELIV01 < \e[0m"
read -p "Do I have my HTML Report?" OPT 
if [[ !($OPT == "y" || $OPT == "") ]] ; then exit 1; fi

DELIV02=`ls igv/igv*`
echo -e "\t\e[1;32m > $DELIV02 < \e[0m"
read -p "Do I have my IGV Session?" OPT 
if [[ !($OPT == "y" || $OPT == "") ]] ; then exit 1; fi

