#!/bin/bash

############################################################################
## Description:
## The wrapper script to run the CAP-miRSeq workflow
##
## Author: Jared Evans (evans.jared@mayo.edu)
## Date: 5/22/14
## Updated: 2015-06-10 Robert Geleta (www.rgeleta.com)
##
## Parameters:
## <run_info file> - The CAP-miRSeq run_info.txt config file
##
############################################################################
# Subs
# -----------------------------------------------------------------------------
log_step_msg5 () {
	tmp_msg=''
	if [ ! $# -lt 3 ]
	then
		tmp_msg=$3
		if [ ! $# -lt 4 ]
		then 
			tmp_msg="$tmp_msg\t$4" 
			if [ ! $# -lt 5 ]
			then
				tmp_msg="$tmp_msg\t$5" 
			fi
		fi
	fi
	$log_msg5 $my_name "$1/$2" "$tmp_msg"
}
# -----------------------------------------------------------------------------
run_step() {
	local step_name=run_step
	log_step_msg5 $step_name 000 "entered" 
	log_step_msg5 $step_name 010 "arg" "\$1" "$1"
	log_step_msg5 $step_name 020 "arg" "\$2" "$2"
	log_step_msg5 $step_name 030 "var" "restart_at" "$restart_at"
	
	this_step_restart=$1
	tmp_cmd=$2
	if [ ! $this_step_restart -lt $restart_at ]
	then
		log_step_msg5 $step_name 100 "calling" "step" "$tmp_cmd"
		$tmp_cmd
	else
		log_step_msg5 $step_name 200 "skipping" "step" "$1 $2"
	fi 
	
	log_step_msg5 $step_name 999 "exiting"
}
# -----------------------------------------------------------------------------
step_03_trim_adapter() {
	local step_name=03_trim_adapter
	## Trim Adapter
	log_step_msg5 $step_name 2.03.000 "Processing" "Trim Adapter" "starting"
	if [ $trim_adapter == "YES" ]
	then
		log_step_msg5 $step_name 2.03.100 "var" "trim_adapter" "YES"
		CUTADAPT_CMD="$script_path/cutadapt.sh $input_dir $output_dir/fastqs/ $samples $fastq_list $tool_info"
		if [ "$use_sge" = "1" ]; then
			trim_args=$args
			if [ ! -z $jobid_ref_idx ]
			then
				trim_args="${trim_args} -hold_jid ${jobid_ref_idx}"
			fi
			mem=$( cat $tool_info | grep -w '^CUTADAPT_MEM=' | sed 's/CUTADAPT_MEM=//g')
			log_step_msg5 $step_name 2.03.200 "calling" "qsub" "$CUTADAPT"
			CUTADAPT=$(qsub $trim_args -t 1-$sample_count -N $job_name.CUTADAPT $mem $CUTADAPT_CMD)
			jobid_cutadapt=$(echo $CUTADAPT | cut -d ' ' -f3 | cut -d '.' -f1)
			all_job_ids="${all_job_ids}${jobid_cutadapt},"
		else
			for count in `seq 1 $sample_count`
			do
				log_step_msg5 $step_name 2.03.301/$count "info" "cutadapt" "Running cutadapt on sample $count "
				log_step_msg5 $step_name 2.03.302/$count "CALLING" "script" "$CUTADAPT_CMD $count "
				CUTADAPT=$($CUTADAPT_CMD $count )
			done
		fi
	else
		log_step_msg5 $step_name 2.03.501 "var" "trim_adapter" "is not YES"
		trimmed_fastq_list=$fastq_list
		i=1
		for fastq in $(echo $fastq_list | tr ":" " ")
		do
			log_step_msg5 $step_name 2.03.521/$i "info" "linking" "fastq=$fastq"
			ln -s $input_dir/$fastq $output_dir/fastqs/
		done
	fi
	log_step_msg5 $step_name 2.03.999 "Processing" "Trim Adapter" "ended"
}

# ---------------------------------------------------------------
step_04_fastqc() {
	local step_name=04_fastqc
	## Run Fastqc
	log_step_msg5 $step_name 2.04.000 "Processing" "Fastqc" "started"
	PRE_FASTQC_CMD="$script_path/fastqc.sh $input_dir $output_dir/qc/fastqc_pretrim $samples $fastq_list $tool_info"
	POST_FASTQC_CMD="$script_path/fastqc.sh $output_dir/fastqs $output_dir/qc/fastqc_posttrim $samples $trimmed_fastq_list $tool_info"
	if [ "$use_sge" = "1" ]; then
		log_step_msg5 $step_name 2.04.sge.000 "Processing" "fastqz" "using SGE"
		fastqc_args=$args
		mem=$( cat $tool_info | grep -w '^FASTQC_MEM=' | sed 's/FASTQC_MEM=//g')
		if [ $trim_adapter == "YES" ]
		then
			log_step_msg5 $step_name 2.04.sge.110 "Processing" "fastqc" "using SGE with trim=YES"
			FASTQC=$(qsub $args -t 1-$sample_count -N $job_name.FASTQC $mem $PRE_FASTQC_CMD)
			jobid_fastqc=$(echo $FASTQC | cut -d ' ' -f3 | cut -d '.' -f1)
			all_job_ids="${all_job_ids}${jobid_fastqc},"
			fastqc_args="${fastqc_args} -hold_jid $jobid_cutadapt"
		fi
		log_step_msg5 $step_name 2.04.sge.200 "calling" "qsub" "fastqc "
		FASTQC=$(qsub $fastqc_args -t 1-$sample_count -N $job_name.FASTQC $mem $POST_FASTQC_CMD)
		jobid_fastqc=$(echo $FASTQC | cut -d ' ' -f3 | cut -d '.' -f1)
		all_job_ids="${all_job_ids}${jobid_fastqc},"
	else
		for count in `seq 1 $sample_count`
		do
			log_step_msg5 $step_name 2.04.loc.110/$count "Processing" "fastqc" "on sample $count"
			if [ $trim_adapter == "YES" ]
			then
				log_step_msg5 $step_name 2.04.loc.112/$count "CALLING" "script" "$PRE_FASTQC_CMD $count "
				PRE_FASTQC=$($PRE_FASTQC_CMD $count )
			fi
			log_step_msg5 $step_name 2.04.loc.114/$count "CALLING" "script" "$POST_FASTQC_CMD $count "
			POST_FASTQC=$($POST_FASTQC_CMD $count )
		done
	fi
	log_step_msg5 $step_name 2.04.999 "Processing" "Fastqc" "ended"
}


# ---------------------------------------------------------------
step_05_mapper() {
	local step_name=05_mapper
	## Run mirdeep2 mapper
	log_step_msg5 $step_name 2.05.000 "Processing" "mirdeep2 mapper" "starting"
	MAPPER_CMD="$script_path/mapper.sh $output_dir/fastqs/ $output_dir/mirdeep2/ $samples $trimmed_fastq_list $tool_info"
	if [ "$use_sge" = "1" ]; then 	
		log_step_msg5 $step_name 2.05.sge.000 "Processing" "mirdeep2 mapper" "using SGE"
		mem=$( cat $tool_info | grep -w '^MIRDEEP2_MAPPER_MEM=' | sed 's/MIRDEEP2_MAPPER_MEM=//g')
		mapper_args=$args
		if [ $trim_adapter == "YES" ]; then
			mapper_args="${args} -hold_jid ${jobid_cutadapt}"
		fi
		log_step_msg5 $step_name 1512 "calling\tqsub"
		MAPPER=$(qsub $mapper_args -N $job_name.MAPPER -t 1-$sample_count -pe threaded 4 $mem $MAPPER_CMD)
		jobid_mapper=$(echo $MAPPER | cut -d ' ' -f3 | cut -d '.' -f1)
		all_job_ids="${all_job_ids}${jobid_mapper},"
	else
		for count in `seq 1 $sample_count`
		do	
			log_step_msg5 $step_name 2.05.loc.101/$count "CALLING" "script" "$MAPPER_CMD $count "
			MAPPER=$($MAPPER_CMD $count)
		done
	fi
	log_step_msg5 $step_name 2.05.000 "Processing" "mirdeep2 mapper" "ended"
}
# ---------------------------------------------------------------
step_06_bams() {
	local step_name=06_bams
	## Create BAMs
	log_step_msg5 $step_name 2.06.000 "Processing" "BAMs" "starting"
	BAMS_CMD="$script_path/bams.sh $output_dir/fastqs/ $output_dir/bams/ $samples $trimmed_fastq_list $tool_info"
	if [ "$use_sge" = "1" ]; then 
		mem=$( cat $tool_info | grep -w '^BAMS_MEM=' | sed 's/BAMS_MEM=//g')
		bams_args=$args
		if [ $trim_adapter == "YES" ]; then
			bams_args="${args} -hold_jid ${jobid_cutadapt}"
		fi
		BAMS=$(qsub $bams_args -N $job_name.BAMS -t 1-$sample_count -pe threaded 4 $mem $BAMS_CMD)
		jobid_bams=$(echo $BAMS | cut -d ' ' -f3 | cut -d '.' -f1)
		all_job_ids="${all_job_ids}${jobid_bams},"
	else
		for count in `seq 1 $sample_count`
		do	
			log_step_msg5 $step_name 2.06.loc.101/$count "CALLING" "script" "$BAMS_CMD $count "
			BAMS=$($BAMS_CMD $count)
		done
	fi
	log_step_msg5 $step_name 2.06.999 "Processing" "BAMs" "ended"
}
# ---------------------------------------------------------------
step_07_mirdeep2() {
	local step_name=07_mirdeep2
	## Run miRDeep2
	log_step_msg5 $step_name 2.07.000 "Processing" "miRDeep2" "starting"
	MIRDEEP2_CMD="$script_path/mirdeep2.sh $output_dir/mirdeep2/ $output_dir/mirdeep2/ $samples $reads_list $alignment_list $tool_info"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^MIRDEEP2_MEM=' | sed 's/MIRDEEP2_MEM=//g')
		MIRDEEP2=$(qsub $args -N $job_name.MIRDEEP2 -t 1-$sample_count $mem -hold_jid $jobid_mapper $MIRDEEP2_CMD)
		jobid_mirdeep2=$(echo $MIRDEEP2 | cut -d ' ' -f3 | cut -d '.' -f1)
		all_job_ids="${all_job_ids}${jobid_mirdeep2},"
	else
		for count in `seq 1 $sample_count`
		do	
			log_step_msg5 $step_name 2.07.loc.101/$count "CALLING" "script" "$MIRDEEP2_CMD $count "
			MIRDEEP2=$($MIRDEEP2_CMD $count)
		done
	fi
	log_step_msg5 $step_name 2.07.999 "Processing" "miRDeep2" "ended"
}
# ---------------------------------------------------------------
step_08_snvs() {
	local step_name=08_snvs
	## Call SNVs
	log_step_msg5 $step_name 2.08.000 "Processing" "SNVs" "starting"
	if [ $call_snvs == "YES" ]; then
		VARIANTS_CMD="$script_path/variants.sh $output_dir/bams $output_dir/variants $run_info"
		if [ "$use_sge" = "1" ]; then
			mem=$( cat $tool_info | grep -w '^VARIANTS_MEM=' | sed 's/VARIANTS_MEM=//g')
			VARIANTS=$(qsub $args -hold_jid $jobid_bams $mem -N $job_name.VARIANTS $VARIANTS_CMD )
			jobid_variants=$(echo $VARIANTS | cut -d ' ' -f3)
			all_job_ids="${all_job_ids}${jobid_variants},"
		else
			log_step_msg5 $step_name 2.08.loc.101 "CALLING" "script" "$VARIANTS_CMD "
			VARIANTS=$($VARIANTS_CMD)
		fi
	fi
	log_step_msg5 $step_name 2.08.999 "Processing" "SNVs" "ended"
}
# ---------------------------------------------------------------
step_09_gencode() {
	local step_name=09_gencode
	## Gencode Classification
	log_step_msg5 $step_name 2.09.000 "Processing" "Gencode Classification" "starting"
	GENCODE_CMD="$script_path/gencode_classification.sh $output_dir/bams $output_dir/qc/other_rna $samples $tool_info"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^GENCODE_CLASSIFICATION_MEM=' | sed 's/GENCODE_CLASSIFICATION_MEM=//g')
		GENCODE=$(qsub $args -hold_jid $jobid_bams $mem -t 1-$sample_count -N $job_name.GENCODE $GENCODE_CMD)
		jobid_gencode=$(echo $GENCODE | cut -d ' ' -f3 | cut -d '.' -f1)
		all_job_ids="${all_job_ids}${jobid_gencode},"
	else
		for count in `seq 1 $sample_count`
		do
			log_step_msg5 $step_name 2.09.loc.101/$count "CALLING" "script" "$GENCODE_CMD "
			GENCODE=$($GENCODE_CMD $count)
		done
	fi
	log_step_msg5 $step_name 2.09.999 "Processing" "Gencode Classification" "ended"
}
# ---------------------------------------------------------------
step_10_exp_rpt() {
	local step_name=10_exp_rpt
	## Generate expression reports
	log_step_msg5 $step_name 2.10.000 "Processing" "expression report gen" "starting"
	EXPRESSION_REPORTS_CMD="$script_path/expression_reports.sh $output_dir/mirdeep2/ $output_dir/expression/ $samples $script_path"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^EXPRESSION_REPORTS_MEM=' | sed 's/EXPRESSION_REPORTS_MEM=//g')
		EXPRESSION_REPORTS=$(qsub $args -N $job_name.EXPRESSION_REPORTS -hold_jid $jobid_mirdeep2 $mem $EXPRESSION_REPORTS_CMD)
		jobid_expression_reports=$(echo $EXPRESSION_REPORTS | cut -d ' ' -f3)
		all_job_ids="${all_job_ids}${jobid_expression_reports},"
	else
		log_step_msg5 $step_name 2.10.201 "CALLING" "script" "$EXPRESSION_REPORTS_CMD "
		EXPRESSION_REPORTS=$($EXPRESSION_REPORTS_CMD)
	fi
	log_step_msg5 $step_name 2.10.999 "Processing" "expression report gen" "ended"
}
# ---------------------------------------------------------------
step_11_diff_exp() {
	local step_name=11_diff_exp
	## Differential Expression
	log_step_msg5 $step_name 2.11.000 "Processing" "Differential Expression" "starting"
	DIFF_EXPRS_CMD="$script_path/differential_expression.sh $output_dir/expression/mature_miRNA_expression.xls $output_dir/differential_expression $run_info"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^DIFF_EXPRESSION_MEM=' | sed 's/DIFF_EXPRESSION_MEM=//g')
		DIFF_EXPRS=$(qsub $args -N $job_name.DIFF_EXPRESSION -hold_jid $jobid_expression_reports $mem $DIFF_EXPRS_CMD)
		jobid_diff_exprs=$(echo $DIFF_EXPRS | cut -d ' ' -f3)
		all_job_ids="${all_job_ids}${jobid_diff_exprs},"
	else
		log_step_msg5 $step_name 2.11.201 "CALLING" "script" "$DIFF_EXPRS_CMD "
		DIFF_EXPRS=$($DIFF_EXPRS_CMD)
	fi
	log_step_msg5 $step_name 2.11.999 "Processing" "Differential Expression" "ended"
}
# ---------------------------------------------------------------
step_12_per_sample_stats() {
	local step_name=12_per_sample_stats
	## Parse and summarize per-sample stats
	log_step_msg5 $step_name 3.01.000 "Processing" "per sample stats" "starting"
	SAMPLE_SUMMARY_CMD="$script_path/sample_summary.sh $output_dir $samples $trim_adapter"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^SAMPLE_SUMMARY_MEM=' | sed 's/SAMPLE_SUMMARY_MEM=//g')
		SAMPLE_SUMMARY=$(qsub $args -N $job_name.SAMPLE_SUMMARY -hold_jid $all_job_ids $mem $SAMPLE_SUMMARY_CMD)
		jobid_sample_summary=$(echo $SAMPLE_SUMMARY | cut -d ' ' -f3)
		all_job_ids="${all_job_ids}${jobid_sample_summary},"
	else
		log_step_msg5 $step_name 3.01.201 "CALLING" "script" "$SAMPLE_SUMMARY_CMD "
		SAMPLE_SUMMARY=$($SAMPLE_SUMMARY_CMD)
	fi
	log_step_msg5 $step_name 3.01.999 "Processing" "per sample stats" "ended"
}
# ---------------------------------------------------------------
step_20_gen_main_doc() {
	local step_name=20_gen_main_doc
	## Generate Main Document
	log_step_msg5 $step_name 3.02.000 "Processing" "main doc gen" "starting"
	MAIN_DOC_CMD="$script_path/main_document.sh $output_dir $script_path $run_info"
	if [ "$use_sge" = "1" ]; then
		mem=$( cat $tool_info | grep -w '^MAIN_DOC_MEM=' | sed 's/MAIN_DOC_MEM=//g')
		MAIN_DOC=$(qsub $args -N $job_name.MAIN_DOC -hold_jid $all_job_ids $mem $MAIN_DOC_CMD)
	else
		log_step_msg5 $step_name 3.02.201 "CALLING" "script" "$MAIN_DOC_CMD "
		MAIN_DOC=$($MAIN_DOC_CMD)
	fi
	log_step_msg5 $step_name 3.02.999 "Processing" "main doc gen" "ended"
}
# #############################################################################
# set shell options
# set -e : exit immediately if a simple command fails
set -e
# -----------------------------------------------------------------------------
# set up log message routine
if [ -z $CAP_MIRNA_LOG_SCRIPT ]
then
	echo "XXXXX Set ENV variable CAP_MIRNA_LOG_SCRIPT=/path/to/logging/script.sh"
	exit 1
fi
log_msg5=$CAP_MIRNA_LOG_SCRIPT
my_name=CAP-miRseq.sh
# write blank line to initialize output
echo
$log_msg5 $my_name 0.00.000 "starting"
# -----------------------------------------------------------------------------
# TODO add code to check for restart ENV variable
if [ -z $CAP_MIRNA_RESTART ]
then
	restart_at=0
	$log_msg5 $my_name 0.00.010 "restart" "no code set, starting at beginning"
else
	restart_at=$CAP_MIRNA_RESTART
	$log_msg5 $my_name 0.00.020 "restart" "restarting at $restart_at"
fi
# -----------------------------------------------------------------------------
# set shell options
# set -u : treat unset variables as an error (as in "option explicit" or "use strict")
set -u
# -----------------------------------------------------------------------------
# start processing
if [ $# != 1 ];
then
	$log_msg5 $my_name 0.01.000 "Usage:" "" "./CAP-miRseq.sh <run_info file>";
else
	set -x
	run_info=$1

	if [ ! -s $run_info ]
	then
		echo -e "ERROR : run_info=$run_info does not exist\n";
		exit 1;
	fi

	
	## get parameters
	$log_msg5 $my_name 1.01.100 "Processing" "parm_file" "run_info=$run_info"
	# fix format (if required)
	dos2unix $run_info

	   tool_info=$( cat $run_info | grep -w '^TOOL_INFO'    | cut -d '=' -f2)
	        tool=$( cat $run_info | grep -w '^TOOL'         | cut -d '=' -f2)
	     version=$( cat $run_info | grep -w '^VERSION'      | cut -d '=' -f2)
	    flowcell=$( cat $run_info | grep -w '^FLOWCELL'     | cut -d '=' -f2)
	 sample_info=$( cat $run_info | grep -w '^SAMPLE_INFO'  | cut -d '=' -f2)
	       email=$( cat $run_info | grep -w '^EMAIL'        | cut -d '=' -f2)
	     samples=$( cat $run_info | grep -w '^SAMPLENAMES'  | cut -d '=' -f2)
	 genomebuild=$( cat $run_info | grep -w '^GENOMEBUILD'  | cut -d '=' -f2)
	      center=$( cat $run_info | grep -w '^CENTER'       | cut -d '=' -f2)
	    platform=$( cat $run_info | grep -w '^PLATFORM'     | cut -d '=' -f2)
	   input_dir=$( cat $run_info | grep -w '^INPUT_DIR'    | cut -d '=' -f2)
	  output_dir=$( cat $run_info | grep -w '^OUTPUT_DIR'   | cut -d '=' -f2)
	trim_adapter=$( cat $run_info | grep -w '^TRIM_ADAPTER' | cut -d '=' -f2 | tr "[a-z]" "[A-Z]")
	   call_snvs=$( cat $run_info | grep -w '^CALL_SNVS'    | cut -d '=' -f2 | tr "[a-z]" "[A-Z]")
	     use_sge=$( cat $run_info | grep -w '^USE_SGE'      | cut -d '=' -f2)
	  use_portal=$( cat $run_info | grep -w '^USE_PORTAL'   | cut -d '=' -f2)	
	
	$log_msg5 $my_name 1.01.200 "Processing" "parm file" "tool_info=$tool_info"
	      queue=$( cat $tool_info | grep -w '^QUEUE'       | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	 bowtie_ref=$( cat $tool_info | grep -w '^BOWTIE_REF'  | cut -d '=' -f2)
	 
	$log_msg5 $my_name 1.01.300 "Processing" "parm file" "sample_info=$sample_info"
	dos2unix $sample_info


	## check the environment
	$log_msg5 $my_name 1.02.000 "Processing" "Environment Check"
	tmp_cmd="$script_path/env_check/env_check -t $tool_info"
	$log_msg5 $my_name 1.02.100 "CALLING" "script" "$tmp_cmd "
	$tmp_cmd

	## confirm whether to skip the portal update / use Sun Grid Engine
	$log_msg5 $my_name 1.03.000 "Processing" "" "portal update/SGE"
	set +u
	if [ ! $use_portal ]; then 
		use_portal="1"; 
	fi

	if [ ! $use_sge ]; then 
		use_sge="1"; 
	fi
	set -u 

	if [ ! "$use_portal" = "0" ]; then 
		use_portal="1" ; 
	fi

	if [ ! "$use_sge" = "0" ]; then 
		use_sge="1" ; 
	fi

	## these are used in subsequent shell scripts
	$log_msg5 $my_name 1.04.000 "info" "" "Exporting variables to be used in subsequent scripts"
	export tool_info run_info sample_info use_sge use_portal

	## check sample_config sample definition
	$log_msg5 $my_name 1.05.000 "Processing" "" "check sample_config sample definitions"
	tmp_cmd="$script_path/CAP-miRSeq-check-parms-sample.sh"
	$log_msg5 $my_name 1.05.100 "CALLING" "script" "$tmp_cmd"
	$tmp_cmd

	## check tool_info_config reference file definition
	$log_msg5 $my_name 1.05.000 "Processing" "" "check tool_info config ref file definitions"
	tmp_cmd="$script_path/CAP-miRSeq-check-parms-refs.sh"
	$log_msg5 $my_name 1.05.100 "CALLING" "script" "$tmp_cmd"
	$tmp_cmd

	## create output dirs
#	$log_msg5 $my_name 1.06.000 "setup" "" "Creating output directories"
#	if [ -d "$output_dir" ]
#	then
#		$log_msg5 $my_name 01.06.100 "ERROR" "" "output dir exists, delete it and restart"
#		exit 1
#	fi
	mkdir -p $output_dir
	mkdir -p $output_dir/logs
	mkdir -p $output_dir/bams
	mkdir -p $output_dir/igv
	mkdir -p $output_dir/mirdeep2
	mkdir -p $output_dir/expression
	mkdir -p $output_dir/variants
	mkdir -p $output_dir/qc
	mkdir -p $output_dir/qc/fastqc_pretrim
	mkdir -p $output_dir/qc/fastqc_posttrim
	mkdir -p $output_dir/qc/other_rna
	mkdir -p $output_dir/differential_expression
	mkdir -p $output_dir/fastqs
	mkdir -p $output_dir/config

	$log_msg5 $my_name 1.06.200 "setup" "" "copying config files to output dir"
	cp $run_info $output_dir/config
	cp $tool_info $output_dir/config
	cp $sample_info $output_dir/config
	# @TODO why are we doing this (changing what config files used here)?
	if [ -f $output_dir/config/$(basename $run_info) ]
	then
		run_info=$output_dir/config/$(basename $run_info)
		tool_info=$output_dir/config/$(basename $tool_info)
		sample_info=$output_dir/config/$(basename $sample_info)
	fi

	## Update NGS Portal
	$log_msg5 $my_name 1.07.000 "info" "" "Update NGS Portal"
	if [ "$use_portal" = "1" ]; then 
		tmp_cmd=$script_path/ngs_portal.sh $run_info Start
		$log_msg5 $my_name 1.07.100 "CALLING" "script" "$tmp_cmd "
		$tmp_cmd
	else 
		$log_msg5 $my_name 1.07.200 "info" "" "Skipping Portal Update"
	fi


	sample_count=$(echo $samples | tr ":" "\n" | wc -l)
	$log_msg5 $my_name 1.08.000 "var" "sample_count" "$sample_count "

	# ---------------------------------------------------------------
	## fastqs
	$log_msg5 $my_name 1.09.000 "setup" "fastqs" "starting"
	i=1
	for sample in $(echo $samples | tr ":" " ")
	do
		$log_msg5 $my_name 1.10.100/$i "info" "fastq sample" "$i $sample "
		fastqArray[$i]=$(grep -w "^${sample}" $sample_info | cut -d"=" -f2)
		trimmedFastqArray[$i]=$sample.cutadapt.fastq
		readsArray[$i]=$sample.reads.fa
		alignmentArray[$i]=$sample.reads_vs_genome.arf
		let i=i+1
	done

	fastq_list=$(echo ${fastqArray[@]} | tr " " ":")
	$log_msg5 $my_name 1.10.901 "var" "fastq_list" "$fastq_list "

	trimmed_fastq_list=$(echo ${trimmedFastqArray[@]} | tr " " ":")
	$log_msg5 $my_name 1.10.902 "var" "trimmed_fastq_list" "$trimmed_fastq_list "

	reads_list=$(echo ${readsArray[@]} | tr " " ":")
	$log_msg5 $my_name 1.10.903 "var" "reads_list" "$reads_list"

	alignment_list=$(echo ${alignmentArray[@]} | tr " " ":")
	$log_msg5 $my_name 1.10.904 "var" "alignment_list" "$alignment_list"
	$log_msg5 $my_name 1.10.999 "Processing" "fastqs" "ended"


	# ---------------------------------------------------------------
	## hold job ids var
	if [ "$use_sge" = "1" ]; then
		$log_msg5 $my_name 1168 "info" "" "Saving job ids for SGE"
		all_job_ids=""
		job_name="$tool.$version.$flowcell"
		args="-V -wd $output_dir/logs -q $queue -m a -M $email"
	fi 

	jobid_ref_idx=""
	
	# ---------------------------------------------------------------
	## Generate reference index files if needed
	$log_msg5 $my_name 1.12.000 "Processing" "bowtie" "starting"
	$log_msg5 $my_name 1.12.010 "var" "bowtie_ref" "$bowtie_ref"
	if [ ! -s $bowtie_ref.fa.fai -o ! -s $bowtie_ref.1.ebwt -o ! -s $bowtie_ref.dict ]
	then
		$log_msg5 $my_name 1.12.100 "WARNING" "bowtie" "missing one or more \$bowtie_ref files"
		REF_IDX_CMD="$script_path/reference_indexes.sh $tool_info"
		if [ "$use_sge" = "1" ]; then
			mem=$( cat $tool_info | grep -w '^REFERENCE_INDEXES_MEM=' | sed 's/REFERENCE_INDEXES_MEM=//g')
			REF_IDX=$(qsub $args $mem -N $job_name.REFERENCE_INDEXES $REF_IDX_CMD )
			jobid_ref_idx=$(echo $REF_IDX | cut -d ' ' -f3)
			all_job_ids="${all_job_ids}${jobid_ref_idx},"
		else
			$log_msg5 $my_name 1.12.200 "CALLING" "script" "$REF_IDX_CMD "
			REF_IDX=$($REF_IDX_CMD)
		fi
	else
		$log_msg5 $my_name 1.12.200 "info" "bowtie" "reference files found"
	fi
	$log_msg5 $my_name 1.12.999 "Processing" "bowtie" "ended"

	# run_step: usage: run_step <restart value> <step function name>
	run_step 03 step_03_trim_adapter
	run_step 04 step_04_fastqc
	run_step 05 step_05_mapper
	run_step 06 step_06_bams
	run_step 07 step_07_mirdeep2
	run_step 08 step_08_snvs
	run_step 09 step_09_gencode
	run_step 10 step_10_exp_rpt
	run_step 11 step_11_diff_exp
	run_step 12 step_12_per_sample_stats
	run_step 20 step_20_gen_main_doc

	# ---------------------------------------------------------------
	$log_msg5 $my_name 9.01.000 "done"
		
fi

$log_msg5 $my_name 9.99.999 "exiting"
