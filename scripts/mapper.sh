#!/bin/bash

############################################################################
## Description:
## wrapper script to run miRDeep2's mapper.pl script
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input,trimmed fastqs are located
## <output_dir> - Directory where output alignment files should be written
## <sample names(s1:s2)> - Colon seperated list of sample names
## <sample fastqs(fq1:fq2)> - Colon seperated list of input fastq files
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################

# -----------------------------------------------------------------------------
# set up log message routine
log_msg5=$HOME/miRNA/log_msg_le5.sh
my_name=mapper.sh
# initialize logfile
echo
$log_msg5 $my_name 0000 "starting"
$log_msg5 $my_name 0025 "version" "2015-05-24-1232"

# -----------------------------------------------------------------------------
# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then
        use_sge=0
else   
        use_sge=1
fi


if [ $# != 5 -a "$use_sge" = "1" ]; then
	echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <sample fastqs(fq1:fq2)> <tool_info>";
elif [ $# != 6 -a "$use_sge" = "0" ] ; then 
	echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <sample fastqs(fq1:fq2)> <tool_info> <sample_number>";
else 
	set -x
	input_dir=$1
	output_dir=$2
	samples=$3
	fastqs=$4
	tool_info=$5
	$log_msg5 $my_name 0046 "arg" "\$1 input_dir  " "$1 "
	$log_msg5 $my_name 0047 "arg" "\$2 output_dir " "$2 "
	$log_msg5 $my_name 0048 "arg" "\$3 samples    " "$3 "
	$log_msg5 $my_name 0049 "arg" "\$4 fastqs     " "$4 "
	$log_msg5 $my_name 0050 "arg" "\$5 tool_info  " "$5 "

        # SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
        if [ "$use_sge" = "1" ]; then
                sample_number=$SGE_TASK_ID
        else   
                sample_number=$6
        fi
	
	bowtie_ref_genome=$( cat $tool_info | grep -w '^BOWTIE_REF' | cut -d '=' -f2)
	mirdeep2_path=$( cat $tool_info | grep -w '^MIRDEEP2_PATH' | cut -d '=' -f2)
	bowtie_path=$( cat $tool_info | grep -w '^BOWTIE_PATH' | cut -d '=' -f2)
	mapper_params=$( cat $tool_info | grep -w '^MAPPER_PARAMS' | cut -d '=' -f2)

	export PATH=$bowtie_path:$mirdeep2_path:$PATH

	# Update NGS Portal
        if [ "$use_portal" = "1" ]; then
                $script_path/ngs_portal.sh $run_info Alignment
        fi

	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	fastq=$( echo $fastqs | tr ":" "\n" | head -$sample_number | tail -1 )

	output_dir=$output_dir/$sample
	mkdir -p $output_dir

	extension=$(echo $input_dir/$fastq | sed 's/.*\.//')
	if [ $extension == "gz" ]
	then
		zcat $input_dir/$fastq > $output_dir/$fastq.fastq
		fastq=$fastq.fastq
	else
		ln -s $input_dir/$fastq $output_dir/
	fi

	$log_msg5 $my_name 1000 "Processing" "mapper" "starting"
	cd $output_dir
	# cores limited to 1 to avoid a bug in the default mapper.pl. It needs a patch to chomp($cores) if the host has > 1
	$mirdeep2_path/mapper.pl $fastq $mapper_params -p $bowtie_ref_genome -s $output_dir/$sample.reads.fa -t $output_dir/$sample.reads_vs_genome.arf -o 1
	$log_msg5 $my_name 1999 "Processing" "mapper" "ended"

	if [ ! -s $output_dir/$sample.reads.fa ]
	then
		$log_msg5 $my_name 0087 "ERROR" "empty file" "${output_dir}/${sample}.reads.fa is empty!"
	fi
	
	if [ ! -s $output_dir/$sample.reads_vs_genome.arf ]
	then
		$log_msg5 $my_name 0093 "ERROR" "empty file" "${output_dir}/${sample}.reads_vs_genome.arf is empty!"
	fi
	
	rm -Rf ${output_dir}/dir_mapper_seq_${fastq}_*
	rm $output_dir/$fastq

	$log_msg5 $my_name 0099 "done"
fi	
$log_msg5 $my_name 9999 "exiting"
