use strict;
use warnings;
use Getopt::Std;

###########################################################################
## Description:
## a script to generate an IGV session that can be used to visualize the 
## NGS reads in the IGV genome browser
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## -o [output dir] - output directory where the IGV session should be written
## -r [run info file] - The CAP-miRSeq run_info.txt config file
##
############################################################################




our ($opt_o,$opt_r);
print "RAW paramters: @ARGV\n";
getopt('or');
if ( (!defined $opt_o) && (!defined $opt_r) ) {
	die ("Usage: $0 \n\t -o [ output folder] \n\t -r [ run info file ]\n");
}
else {
	# parse parameters from confi files
	my $output = $opt_o;   # output folder
	my $run_info = $opt_r;
	my $remove_first_dir = 1;
	my @line=split(/=/,`perl -ne "/^SAMPLENAMES/ && print" $run_info`);
	my $samples=$line[$#line];chomp $samples;
	@line=split(/=/,`perl -ne "/^DELIVERY_FOLDER/ && print" $run_info`);
	my $delivery_folder=$line[$#line];chomp $delivery_folder;
	@line=split(/=/,`perl -ne "/^TOOL_INFO/ && print" $run_info`);
	my $tool_info=$line[$#line];chomp $tool_info;
	@line=split(/=/,`perl -ne "/^IGVSESSION_SERVER/ && print" $tool_info`);
	my $server=$line[$#line];chomp $server if defined $server;
	@line=split(/=/,`perl -ne "/^GENOME_BUILD/ && print" $run_info`);
	my $genome=$line[$#line];chomp $genome;
	my $dest = $output . "/igv_session.xml";

	# check to see if the path should point to an FTP server defined in the tool_info.txt
	if(defined $server && $remove_first_dir){
		$delivery_folder =~ s/\/.*?\/(.*)$/$1/;
		$delivery_folder = $server."/".$delivery_folder;
	}

	# write xml igv session
	open FH , ">$dest" or die "can not open $dest : $! \n";
	print FH "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
		<Global genome=\"$genome\" locus=\"All\" version=\"4\">
	    	<Resources> ";
	my @sampleNames=split(/:/,$samples);
	for(my $i = 0 ; $i <= $#sampleNames; $i++)	{
		print FH "\n<Resource path=\"$delivery_folder/bams/$sampleNames[$i].bam\" />";
	}

	print FH "		</Resources>
			</Global>";

}
close FH;
