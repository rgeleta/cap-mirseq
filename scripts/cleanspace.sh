#!/bin/bash

############################################################################
## Description:
## script that can be used after CAP-miRSeq finished running to clean-up
## any temporary files not needed. Only run this script after running the
## transfer2delivery.sh script.
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <run_info.txt> - The CAP-miRSeq run_info.txt config file
##
############################################################################


if [ $# != 1 ];
then
        echo "usage: cleanspace.sh <run_info.txt>";
else
set -x
echo `date`
run_info=$1
output_dir=$( cat $run_info | grep -w '^OUTPUT_DIR' | cut -d '=' -f2)

# make sure we don't start deleting things if the important deliverables are still around
IGV_BAMS=$(ls $output_dir/IGV_BAM/*.igv-sorted.bam | wc -l)
REPORTS=$(ls $output_dir/*.xls | wc -l)
IGV_SESSION=$(ls $output_dir/*.xml | wc -l)
MIRDEEP2=$(ls $output_dir/mirdeep2/*/result_* | wc -l)
let total=$IGV_BAMS+$REPORTS+$IGV_SESSION+$MIRDEEP2
if [ $total -gt 0 ]
then
	echo "ERROR: Transfer deliverables out of this space before cleaning it!!"
	exit 1;
fi

# start deleting
rm -R $output_dir/alignment
rm -R $output_dir/cutadapt
rm -R $output_dir/IGV_BAM
rm -R $output_dir/mirdeep2
rm -R $output_dir/tmp


echo `date`
fi
