#!/bin/bash

############################################################################
## Description:
## a script to run bowtie to create bam files used for IGV and other RNA QC
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input fastqs are located
## <output_dir> - Directory where bam files should be written to
## <sample names(s1:s2)> - Colon seperated list of sample names
## <sample fastqs(fq1:fq2)> - Colon seperated list of fastq files for each sample
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################

# -----------------------------------------------------------------------------
# set up log message routine
if [ -z $CAP_MIRNA_LOG_SCRIPT ]
then 
	echo Set ENV variable CAP_MIRNA_LOG_SCRIPT=/path/to/logging/script.sh
	exit 1
fi
log_msg5=$CAP_MIRNA_LOG_SCRIPT
my_name=bams.sh
# initialize msglog
echo
$log_msg5 $my_name 0000 "starting"
$log_msg5 $my_name 0025 "version" "2015-06-09-1537"

# -----------------------------------------------------------------------------
# determine whether this script is being called through Sun Grid Engine
$log_msg5 $my_name 0027 "checking" "SGE" "Sun Grid Engine"
if [ -z $SGE_TASK_ID ]; then
        use_sge=0
else    
        use_sge=1
fi

# -----------------------------------------------------------------------------
if [ $# != 5 -a "$use_sge" = "1" ]; then
	$log_msg5 $my_name 0038 "usage" "" "<input dir> <output_dir> <sample names(s1:s2)> <sample fastqs(fq1:fq2)> <tool_info>";
elif [ $# != 6 -a "$use_sge" = "0" ] ; then
    $log_msg5 $my_name 0040 "usage" "" "<input dir> <output_dir> <sample names(s1:s2)> <sample fastqs(fq1:fq2)> <tool_info> <sample_number>";
else  
	set -x
	input_dir=$1
	output_dir=$2
	samples=$3
	fastqs=$4
	tool_info=$5
	
	$log_msg5 $my_name 0046 "arg" "\$1 input_dir   " "$1 "
	$log_msg5 $my_name 0047 "arg" "\$2 output_dir  " "$2 "
	$log_msg5 $my_name 0048 "arg" "\$3 samples     " "$3 "
	$log_msg5 $my_name 0049 "arg" "\$4 fastqs      " "$4 "
	$log_msg5 $my_name 0050 "arg" "\$5 tool_info   " "$5 "

        # SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
        if [ "$use_sge" = "1" ]; then
                sample_number=$SGE_TASK_ID
        else    
                sample_number=$6
        fi
	
	## extract parameters from config files
	$log_msg5 $my_name 0055 "processing" "config" "extract parameters from config files "
	       ref_genome=$( cat $tool_info | grep -w '^REF_GENOME' | cut -d '=' -f2)
	bowtie_ref_genome=$( cat $tool_info | grep -w '^BOWTIE_REF' | cut -d '=' -f2)
	      bowtie_path=$( cat $tool_info | grep -w '^BOWTIE_PATH' | cut -d '=' -f2)
	         samtools=$( cat $tool_info | grep -w '^SAMTOOLS_PATH' | cut -d '=' -f2)
	           picard=$( cat $tool_info | grep -w '^PICARD_PATH' | cut -d '=' -f2)
	             java=$( cat $tool_info | grep -w '^JAVA_PATH' | cut -d '=' -f2)
	      script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
#	gatk_jar=$( cat $tool_info | grep -w '^GATK_JAR' | cut -d '=' -f2)
	addorreplacereadgroups_params=$( cat $tool_info | grep -w '^ADDORREPLACEREADGROUPS_PARAMS' | sed 's/ADDORREPLACEREADGROUPS_PARAMS=//g')
	addorreplacereadgroups_jvm_mem=$( cat $tool_info | grep -w '^ADDORREPLACEREADGROUPS_JVM_MEM' | sed 's/ADDORREPLACEREADGROUPS_JVM_MEM=//g')
#	printreads_jvm_mem=$( cat $tool_info | grep -w '^PRINTREADS_JVM_MEM' | sed 's/PRINTREADS_JVM_MEM=//g')
	bowtie_params=$( cat $tool_info | grep -w '^BOWTIE_PARAMS' | sed 's/BOWTIE_PARAMS=//g')
#	printreads_params=$( cat $tool_info | grep -w '^PRINTREADS_PARAMS' | sed 's/PRINTREADS_PARAMS=//g')
	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	fastq=$( echo $fastqs | tr ":" "\n" | head -$sample_number | tail -1 )
	
	## check if fastq file is gzipped
	$log_msg5 $my_name 0073 "checking" "fastq" "if fastq file is gzipped "
	extension=$(echo $input_dir/$fastq | sed 's/.*\.//')
	if [ $extension == "gz" ]
	then
		$log_msg5 $my_name 0077 "extracting" "fastq" "extracting fastq gz file "
		zcat $input_dir/$fastq > $output_dir/$fastq.fastq
		input_dir=$output_dir
		fastq=$fastq.fastq
	fi
	
	## check if base quality scores are encoded using Sanger or Illumina scores
	$log_msg5 $my_name 0094 "checking" "" "if base quality scores are encoded using Sanga or Illumina scores "
	$log_msg5 $my_name 0095 "CALLING" "perl script" "perl $script_path/checkFastqQualityScores.pl $input_dir/$fastq 1000"
	ILL2SANGER=$(perl $script_path/checkFastqQualityScores.pl $input_dir/$fastq 1000)

	if [ $ILL2SANGER -gt 65 ]
        then
		$log_msg5 $my_name 0100 "info" "Qual scores" "are Illumina "
		quals="--phred64-quals" # Illumina Qual scores
	else
		$log_msg5 $my_name 0103 "info" "Qual scores" "are Sanger "
		quals="--phred33-quals" # Sanger Qual scores
	fi
	
	# run bowtie with same parameters used by miRDeep2 mapper.pl
	$log_msg5 $my_name 0105 "Processing" "bowtie" "run bowtie with same parameters used by miRDeep2 mapper.pl "
	$log_msg5 $my_name 0109 "CALLING" "binary" "$bowtie_path/bowtie $quals $bowtie_params --sam-RG ID:$sample --sam-RG SM:$sample $bowtie_ref_genome $input_dir/$fastq $output_dir/$sample.aligned.sam 2> $output_dir/$sample.bowtie.log"
	$bowtie_path/bowtie $quals $bowtie_params --sam-RG ID:$sample --sam-RG SM:$sample $bowtie_ref_genome $input_dir/$fastq $output_dir/$sample.aligned.sam 2> $output_dir/$sample.bowtie.log

	cat $output_dir/$sample.bowtie.log

	if [ ! -s $output_dir/$sample.aligned.sam ]
	then
		$log_msg5 $my_name 0103 "ERROR" "empty file" "${output_dir}/${sample}.aligned.sam is empty!"
	fi

	if [ $extension == "gz" ]
	then
		rm $output_dir/$fastq
	fi

	# convert the bowtie 255 mapping quality score to 60 so GATK can be use for variant calling
	$log_msg5 $my_name 0112 "Processing" "bowtie" "convert bowtie 255 mapping quality score for GATK "
	$samtools/samtools view -SH $output_dir/$sample.aligned.sam > $output_dir/$sample.aligned.mq.sam
	$samtools/samtools view -S $output_dir/$sample.aligned.sam | awk -F"\t" 'OFS="\t"{$5 = 60; print $0}' >> $output_dir/$sample.aligned.mq.sam
	
	rm $output_dir/$sample.aligned.sam

	# sort bam and add per-read read groups that bowtie can't do
	$log_msg5 $my_name 0119 "processing" "bam" "sort bam and add per read groups that bowtie cannot do "
	$java/java $addorreplacereadgroups_jvm_mem \
        -jar $picard/AddOrReplaceReadGroups.jar \
        INPUT=$output_dir/$sample.aligned.mq.sam \
        OUTPUT=$output_dir/$sample.bam \
        SORT_ORDER=coordinate \
        TMP_DIR=$output_dir/ \
	RGID=$sample RGPU=$sample RGSM=$sample \
        $addorreplacereadgroups_params

	if [ ! -s $output_dir/$sample.bam ]
	then
		$log_msg5 $my_name 0131 "ERROR" "empty_file" "${output_dir}/${sample}.sorted.bam is empty!"
	fi
	
	rm $output_dir/$sample.aligned.mq.sam
	$samtools/samtools index $output_dir/$sample.bam
	
	

	if [ ! -s $output_dir/$sample.bam.bai ]
	then
		$log_msg5 $my_name 0141 "ERROR" "empty_file" "${output_dir}/${sample}.bam.bai is empty!"
	fi

	$log_msg5 $my_name 0144 "done"
fi	

$log_msg5 $my_name 9999 "exiting"
