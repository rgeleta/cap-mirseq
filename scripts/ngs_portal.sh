#!/bin/bash
# update the NGS portal

############################################################################
## Description:
## Script to update Mayo's internal NGS portal software. Will not work and isn't
## necessary for users outside of Mayo Clinic
##
############################################################################

if [ $# -lt 2 ]
then
    echo -e "Usage : script to update NGS portal\nUsage:<runinfo> <stage of the workflow> <\"complete\" if samples finished (optional)>"
else
	set -x
	echo `date`	
	run_info=$1
	stage=$2
	stage_status=$3

	tool_info=$( cat $run_info | grep -w '^TOOL_INFO' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2 )
	java=$( cat $tool_info | grep -w '^JAVA_PATH' | cut -d '=' -f2)
	run_num=$( cat $run_info | grep -w '^FLOWCELL' | cut -d '=' -f2)
	flowcell=$(echo $run_num | awk -F'_' '{if(substr($NF,0,1) == 0){print "0"$NF}else{print $NF}}' | sed 's/.\(.*\)/\1/')
	version=$( cat $run_info | grep -w '^VERSION' | cut -d '=' -f2)
	tool=$(cat $run_info | grep -w '^TOOL' | cut -d '=' -f2)
	portal_project_name=$(cat $run_info | grep -w '^PORTAL_PROJECT_NAME' | cut -d '=' -f2)

		
	if [ $stage_status == "complete" ]
	then
		$status_param="-c 1"
	fi

	# tepmorary for testing:
	# PORTAL_JAR=$script_path
	PORTAL_JAR=/home/m082166/AddSecTest

	#$java/java -Xmx256M -jar $script_path/AddSecondaryAnalysis.jar -p $script_path/AddSecondaryAnalysis.properties -S http://rcftomprod1 -f $flowcell -r $run_num -s $stage -a $tool $status_param --analysisName $portal_project_name 
	$java/java -Xmx256M -jar $PORTAL_JAR/AddSecondaryAnalysis.jar -p $PORTAL_JAR/AddSecondaryAnalysis.properties -S http://rcftomprod1 -f $flowcell -r $run_num -s $stage -a $tool $status_param --analysisName $portal_project_name 

	echo `date`
fi


