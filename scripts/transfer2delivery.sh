#!/bin/bash

############################################################################
## Description:
## Script to be run after CAP-miRSeq finishes. Only the final results will be
## transfered to delivery location, leaving unnessary intermediate files
##
## Parameters:
## <tool_info> - CAP-miRSeq run_info.txt config file
##
############################################################################


# transfer deliverables and IGV bams to /data2/delivery/.... 
RSYNC_URL="http://dnode0.mayo.edu/cgi-cim/rsync-agtc-subdir.pl"
#HSRWEB_URL="http://hsrwww.mayo.edu/bsi/projects/bic"
HSRWEB_URL="http://bucky/data_delivery/bic"
RCF_URL="ftp://rcfisinl1-212/delivery"

if [ $# != 1 ];
then
        echo "usage: transfer2delivery.sh <run_info.txt>";
		exit 0;
fi 
echo `date`
run_info=$1

## <path to delivery (/data2/delivery/...../secondary/)>
delivery_dir=$( cat $run_info | grep -w '^DELIVERY_FOLDER' | cut -d '=' -f2)
echo -e "\t\e[1;32m $delivery_dir \e[0m"
read -p "Send this RCF directory?" OPT
if [[ !($OPT == "y" || $OPT == "") ]]; then	echo -e "\e[1;31m Please fix DELIVERY_FOLDER in run_info.txt and rerun this script. \e[0m"; exit 1; fi

output_dir=$( cat $run_info | grep -w '^OUTPUT_DIR' | cut -d '=' -f2)
samples=$( cat $run_info | grep -w '^SAMPLENAMES' | cut -d '=' -f2)
PI=$( cat $run_info | grep -w '^PI' | cut -d '=' -f2)
FLOWCELL=$( cat $run_info | grep -w '^FLOWCELL' | cut -d '=' -f2)


read -p "Move files to RCF?" OPT
if [[ ($OPT == "y" || $OPT == "") ]] 
then
	### Show Exicutions
	set -x
	mkdir -p $delivery_dir/mirdeep2
	for sample in $(echo $samples | tr ":" " ");
	do
		mkdir $delivery_dir/mirdeep2/$sample
		pdfs=$(ls $output_dir/mirdeep2/$sample/ | grep pdfs)
		mkdir $delivery_dir/mirdeep2/$sample/$pdfs
		mv $output_dir/mirdeep2/$sample/$pdfs/* $delivery_dir/mirdeep2/$sample/$pdfs/
		mv $output_dir/mirdeep2/$sample/result_* $delivery_dir/mirdeep2/$sample/
		mv $output_dir/mirdeep2/$sample/expression_*.html $delivery_dir/mirdeep2/$sample/
		mv $output_dir/mirdeep2/$sample/miRNAs_expressed_all_samples* $delivery_dir/mirdeep2/$sample/
	done

	mkdir $delivery_dir/config
	mkdir $delivery_dir/expression
	mkdir $delivery_dir/qc
	mkdir $delivery_dir/igv
	mkdir $delivery_dir/variants
	mkdir $delivery_dir/differential_expression
	cp $output_dir/config/* $delivery_dir/config
	mv $output_dir/variants/* $delivery_dir/variants
	mv $output_dir/expression/* $delivery_dir/expression
	mv $output_dir/differential_expression/* $delivery_dir/differential_expression
	cp -R $output_dir/qc/* $delivery_dir/qc
	mv $output_dir/MainDocument.html $delivery_dir
	mv $output_dir/CAP-miRSeq_workflow.png $delivery_dir
	mv $output_dir/SampleSummary.xls $delivery_dir
	mv $output_dir/igv/* $delivery_dir/igv

	cd $delivery_dir
	zip -r ALL_FILES ./*

	mkdir $delivery_dir/bams
	mv $output_dir/bams/*.bam $delivery_dir/bams/
	mv $output_dir/bams/*.bam.bai $delivery_dir/bams/
	### Stop showing all exicutions
	set -
	echo -e "\n"
	echo -e "Please Rsync: \e[1;36m $RSYNC_URL \e[0m"
	echo -e "\n"
fi


read -p "Move files to HSR?" OPT
if [[ ($OPT == "y" || $OPT == "") ]] 
then
	DESTDIR=$(date +"%Y_%m_%d")
	read -p "The HSR directory MUST already exist, Please enter the s##.keyword name here: " HSR_DIR
	if [[ $HSR_DIR == "" ]]; then	echo "INVALID!\n"; exit 1; fi
	
	HSRDELIVER="apps/data_delivery/bsi/bic/${PI}/${HSR_DIR}/${DESTDIR}"
	echo -e "\t\e[1;32m ${HSRDELIVER} \e[0m"
	read -p "Send this HSR directory?" OPT 
	if [[ !($OPT == "y" || $OPT == "") ]]; then echo "QUIT OUT!\n"; exit 1; fi
		
	## Get User Credentials
	MLAN_ID=$(whoami)
	echo "$MLAN_ID please provide your Mayo password:"
	read -s PSWD
	printf "username=$MLAN_ID\npassword=$PSWD" > ~/ACESSKEY	
		
	printf "\x1b[5mTar Files...\x1b[25m"
	cd $delivery_dir
	ls | grep -E 'expression|qc|igv|mirdeep2|variants|html|xls|png' > list.txt
	tar -cf mytarfile.tar -T list.txt 
	echo -e "\n\n\n"
	
	### Send to HRS web
	smbclient "//hsrntfs/projects" \
	-W MFAD	-A ~/ACESSKEY \
	-c "cd apps/data_delivery/bsi/bic/${PI}/${HSR_DIR}/; mkdir $DESTDIR; cd $DESTDIR;" \
	-Tx mytarfile.tar

	rm ~/ACESSKEY
	rm list.txt
	rm mytarfile.tar
fi



# Update NGS Portal
#$script_path/ngs_portal.sh $run_info Delivered complete
read -p "Ready to Update NGS Portal?" OPT
if [[ ($OPT == "y" || $OPT == "") ]] 
then
	PORTAL_PROJECT_NAME=`grep -w '^PORTAL_PROJECT_NAME' $run_info | cut -d '=' -f2`
	tool_info=$( cat $run_info | grep -w '^TOOL_INFO' | cut -d '=' -f2)
	JAVA_PATH=`grep -w '^JAVA_PATH' $tool_info | cut -d '=' -f2`
	NGS_PORTAL_PATH=`grep -w '^NGS_PORTAL_PATH' $tool_info | cut -d '=' -f2`

	## -c => means this is complete
	## -s => state keyword
	$JAVA_PATH/java -Xmx256M -jar $NGS_PORTAL_PATH/AddSecondaryAnalysis.jar \
	-p $NGS_PORTAL_PATH"/"AddSecondaryAnalysis.properties -S http://rcftomprod1 \
	-r $FLOWCELL -f $FLOWCELL --analysisName $PORTAL_PROJECT_NAME -s Delivered -a miRNA -c

fi



read -p "Ready to DELETE everything in working dir?" OPT
if [[ ($OPT == "y" || $OPT == "") ]] 
then
	echo -e "\e[1;31m  THIS CANNOT BE UNDONE!\e[0m"
	read -p "Are you sure?" OPT1
	if [[ ($OPT1 == "y" || $OPT1 == "") ]]; then ls; fi
	# start deleting
	
fi


echo -e "\n\nFlowcell: $FLOWCELL"
if [[ $HSR_DIR != "" ]]; then echo -e "The Deliverable Results:\n  \e[1;36m$HSRWEB_URL/$PI/$HSR_DIR/$DESTDIR \e[0m \n";  fi
RCFDEST=$( echo $delivery_dir | sed 's/^.*\delivery\///' )
echo -e "The Workflow Analysis Output (Alignment & Large Files):\n  \e[1;36m$RCF_URL/$RCFDEST \e[0m \n\n"

