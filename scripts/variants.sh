#!/bin/bash

############################################################################
## Description:
## Call variants using GATK UnifiedGenotyper and add miRNA and seed annotations
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input BAMs are located
## <output_dir> - Directory where output VCF and excel file should be written
## <tool_info> - CAP-miRSeq run_info.txt config file
##
############################################################################

if [ $# != 3 ];
then
	echo "usage: variants.sh <input bam dir> <output_dir> <run_info>";
	exit 1
fi				
	set -x
	echo `date`
	input_dir=$1
	output_dir=$2
	run_info=$3
	
	tool_info=$( cat $run_info | grep -w '^TOOL_INFO' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	mirbase_gff=$( cat $tool_info | grep -w '^MIRBASE_GFF' | cut -d '=' -f2)
	ref_genome=$( cat $tool_info | grep -w '^REF_GENOME' | cut -d '=' -f2)
	samtools_path=$( cat $tool_info | grep -w '^SAMTOOLS_PATH' | cut -d '=' -f2)
	bedtools_path=$( cat $tool_info | grep -w '^BEDTOOLS_PATH' | cut -d '=' -f2)
	vcftools_path=$( cat $tool_info | grep -w '^VCFTOOLS_PATH' | cut -d '=' -f2)
	vcftools_perllib=$( cat $tool_info | grep -w '^VCFTOOLS_PERLLIB' | cut -d '=' -f2)
	gatk_jar=$( cat $tool_info | grep -w '^GATK_JAR' | cut -d '=' -f2)
	java=$( cat $tool_info | grep -w '^JAVA_PATH' | cut -d '=' -f2)
	unifiedgenotyper_params=$( cat $tool_info | grep -w '^UNIFIEDGENOTYPER_PARAMS' | cut -d '=' -f2)
	ref_genome=$( cat $tool_info | grep -w '^REF_GENOME' | cut -d '=' -f2)
	unifiedgenotyper_jvm_mem=$( cat $tool_info | grep -w '^UNIFIEDGENOTYPER_JVM_MEM' | cut -d '=' -f2)
	samples=$( cat $run_info | grep -w '^SAMPLENAMES' | cut -d '=' -f2)

	export PERL5LIB=$vcftools_perllib:$PERL5LIB
	export PATH=$bedtools_path:$PATH

	# create precursor and mature bed files from gff3
	grep -v "^#" $mirbase_gff | grep miRNA_primary_transcript | awk -F"\t" '{split($9,a,";"); split(a[3],b,"="); mirna=b[2]; print $1"\t"$4-1"\t"$5"\t"mirna"\t"($5-($4-1))"\t"$7}' | sortBed > $output_dir/mirbase_precursor.tmp.bed
	grep -v "^#" $mirbase_gff | grep -v miRNA_primary_transcript | awk -F"\t" '{split($9,a,";"); split(a[3],b,"="); mirna=b[2]; print $1"\t"$4-1"\t"$5"\t"mirna"\t"($5-($4-1))"\t"$7}' | sortBed > $output_dir/mirbase_mature.tmp.bed
	awk -F"\t" '{if($6 == "+"){print $1"\t"$2+1"\t"$2+8}else{print $1"\t"$3-9"\t"$3-2}}' $output_dir/mirbase_mature.tmp.bed | $bedtools_path/sortBed | $bedtools_path/mergeBed > $output_dir/seed_region.tmp.bed

	for sample in $(echo $samples | tr ":" " ")
	do
		input_bams="${input_bams} -I ${input_dir}/${sample}.bam"
	done

	# call variants accross all samples with UnifiedGenotyper, only in the mature miRNA regions
	$java/java $unifiedgenotyper_jvm_mem \
	-jar $gatk_jar -T UnifiedGenotyper \
	-R $ref_genome $input_bams \
	-L $output_dir/mirbase_precursor.tmp.bed \
	-o $output_dir/mirna_variants.vcf \
	$unifiedgenotyper_params

	if [ ! -s $output_dir/mirna_variants.vcf ]
	then
		echo "ERROR : ${output_dir}/mirna_variants.vcf is empty!"
	fi

	# create annotation report
	$vcftools_path/vcf-query --use-old-method -f '%CHROM\t%POS\t%REF\t%ALT[\t%GT\t%AD\t%DP\t%GQ]\n' ${output_dir}/mirna_variants.vcf > ${output_dir}/mirna_variants.vcf.txt
	awk -F"\t" '{print $1"\t"$2-1"\t"$2}' ${output_dir}/mirna_variants.vcf.txt > ${output_dir}/mirna_variants.vcf.txt.bed
	$bedtools_path/intersectBed -a ${output_dir}/mirna_variants.vcf.txt.bed -b $output_dir/mirbase_mature.tmp.bed -loj | cut -f1,2,3,7 | $bedtools_path/mergeBed -nms -d -1 > ${output_dir}/mirna_variants.vcf.txt.bed.mature.bed
	$bedtools_path/intersectBed -a ${output_dir}/mirna_variants.vcf.txt.bed -b $output_dir/mirbase_precursor.tmp.bed -loj | cut -f1,2,3,7 | $bedtools_path/mergeBed -nms -d -1 > ${output_dir}/mirna_variants.vcf.txt.bed.precursor.bed
	$bedtools_path/intersectBed -a ${output_dir}/mirna_variants.vcf.txt.bed -b $output_dir/seed_region.tmp.bed -c > ${output_dir}/mirna_variants.vcf.txt.bed.seed.bed


	# write header
	echo -ne "Chr\tPos\tRef\tAlt\tPrecursor_miRNA\tMature_miRNA\tSeed_Region" > $output_dir/miRNA_variants.xls
	for sample in $(echo $samples | tr ":" " ")
	do
		echo -ne "\t${sample}.Genotype\t${sample}.Allelic_Depth\t${sample}.Total_Read_Depth\t${sample}.Genotype_Quality" >> $output_dir/miRNA_variants.xls
	done
	echo -ne "\n" >> $output_dir/miRNA_variants.xls

	# merge different annotations
	paste ${output_dir}/mirna_variants.vcf.txt ${output_dir}/mirna_variants.vcf.txt.bed.precursor.bed | awk -F"\t" 'BEGIN{ORS=""}{if($1 == $(NF-3) && $2 == $(NF-1)){print $1"\t"$2"\t"$3"\t"$4"\t"$(NF); for(i=5;i<=(NF-4);i++){print "\t"$i}; print "\n";}else{print "ERROR! annotation columns out of order!" | "cat 1>&2"}}' > ${output_dir}/mirna_variants.vcf.txt.pre
	paste ${output_dir}/mirna_variants.vcf.txt.pre ${output_dir}/mirna_variants.vcf.txt.bed.mature.bed | awk -F"\t" 'BEGIN{ORS=""}{if($1 == $(NF-3) && $2 == $(NF-1)){print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$(NF); for(i=6;i<=(NF-4);i++){print "\t"$i}; print "\n";}else{print "ERROR! annotation columns out of order!" | "cat 1>&2"}}' > ${output_dir}/mirna_variants.vcf.txt.pre.mat
	paste ${output_dir}/mirna_variants.vcf.txt.pre.mat ${output_dir}/mirna_variants.vcf.txt.bed.seed.bed | awk -F"\t" 'BEGIN{ORS=""}{if($1 == $(NF-3) && $2 == $(NF-1)){print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$(NF); for(i=7;i<=(NF-4);i++){print "\t"$i}; print "\n";}else{print "ERROR! annotation columns out of order!" | "cat 1>&2"}}' > ${output_dir}/mirna_variants.vcf.txt.pre.mat.seed

	cat ${output_dir}/mirna_variants.vcf.txt.pre.mat.seed | sed 's/,/;/g' >> $output_dir/miRNA_variants.xls

	if [ ! -s $output_dir/miRNA_variants.xls ]
	then
		echo "ERROR : ${output_dir}/mirna_variants.xls is empty!"
	fi

	rm $output_dir/mirbase_precursor.tmp.bed
	rm $output_dir/mirbase_mature.tmp.bed
	rm $output_dir/seed_region.tmp.bed
	rm $output_dir/mirna_variants.vcf.txt.pre
	rm $output_dir/mirna_variants.vcf.txt.pre.mat
	rm $output_dir/mirna_variants.vcf.txt.pre.mat.seed
	rm $output_dir/mirna_variants.vcf.txt.bed.precursor.bed
	rm $output_dir/mirna_variants.vcf.txt.bed.mature.bed
	rm $output_dir/mirna_variants.vcf.txt.bed.seed.bed
	rm $output_dir/mirna_variants.vcf.txt
	rm $output_dir/mirna_variants.vcf.txt.bed

	echo `date`

