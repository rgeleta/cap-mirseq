#!/bin/bash

############################################################################
## Description:
## run Cutadapt to trim adapter sequences from miRNA reads
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input fastqs are located
## <output_dir> - Directory where trimmed output fastqs should be written
## <sample names(s1:s2)> - Colon seperated list of sample names
## <sample fastqs(fq1:fq2)> - Colon seperated list of fastq files for each sample
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################
# -----------------------------------------------------------------------------
# set up log message routine
write_log_msg5=$HOME/miRNA/log_msg_le5.sh
my_name=cutadapt.sh
$write_log_msg5 $my_name 0000 "starting"
$write_log_msg5 $my_name 0025 "version" "2015-05-24-2307"

# -----------------------------------------------------------------------------
# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then 
	use_sge=0
else 
	use_sge=1
fi
$write_log_msg5 $my_name 0034 "var" "use_sge" "$use_sge"
# -----------------------------------------------------------------------------
# set shell options
# done after SGE check to avoid script failure if variable is not set
# set -e : exit immediately if a simple command fails
set -e
# set -u : treat unset variables as an error (as in "option explicit" or "use strict")
set -u 
# -----------------------------------------------------------------------------

if [ $# != 5 -a "$use_sge" = "1" ]; then 
	$write_log_msg5 $my_name 0042 "INFO" "usage" "<input dir> <output dir> <sample names(sn1:sn2)> <input fastq names (s1:s2)> <tool_info>";
	exit 1
elif [ $# != 6 -a "$use_sge" = "0" ] ; then 
	$write_log_msg5 $my_name 0044 "INFO" "usage" "<input dir> <output dir> <sample names(sn1:sn2)> <input fastq names (s1:s2)> <tool_info> <sample_number>";
	exit 1
else
	set -x
	input_dir=$1
	output_dir=$2
	samples=$3
	fastqs=$4
	tool_info=$5
	
	$write_log_msg5 $my_name 0042 "arg" "count           " "$#"
	$write_log_msg5 $my_name 0053 "arg" "\$1 input_dir   " "$1 "
	$write_log_msg5 $my_name 0054 "arg" "\$2 output_dir  " "$2 "
	$write_log_msg5 $my_name 0055 "arg" "\$3 samples     " "$3 "
	$write_log_msg5 $my_name 0056 "arg" "\$4 fastqs      " "$4 "
	$write_log_msg5 $my_name 0057 "arg" "\$5 tool_info   " "$5 "
	if [ ! $# -lt 6 ] ; then
		$write_log_msg5 $my_name 0058 "arg" "\$6 run_nbr(opt)" "$6 "
	fi

	# SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
	if [ "$use_sge" = "1" ]; then
		sample_number=$SGE_TASK_ID
	else
		sample_number=$6
	fi

	cutadapt_path=$( cat $tool_info | grep -w '^CUTADAPT_PATH' | cut -d '=' -f2)
#	fastqc_path=$( cat $tool_info | grep -w '^FASTQC_PATH' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	cutadapt_params=$( cat $tool_info | grep -w '^CUTADAPT_PARAMS' | cut -d '=' -f2)

	# Update NGS Portal
#	if [ "$use_portal" = "1" ]; then
#        	$script_path/ngs_portal.sh $run_info AdapterTrimming	
#	fi

	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	fastq=$( echo $fastqs | tr ":" "\n" | head -$sample_number | tail -1 )
	
	# check if input fastqs are gzipped and what encoding they are (SANGER/ILLUMINA)
	$write_log_msg5 $my_name 0080 "info" "fastq" "check if input fastqs are gzipped and encoding type "
	extension=$(echo $input_dir/$fastq | sed 's/.*\.//')
	if [ "$extension" == "gz" ];
	then
		$write_log_msg5 $my_name 0084 "info" "fastq" "extension is gz "
		zcat $input_dir/$fastq | head -4000 > $output_dir/$fastq.tmp
		ILL2SANGER=$(perl $script_path/checkFastqQualityScores.pl $output_dir/$fastq.tmp 1000)
		rm $output_dir/$fastq.tmp
	else
		ILL2SANGER=$(perl $script_path/checkFastqQualityScores.pl $input_dir/$fastq 1000)
	fi

	if [ "$ILL2SANGER" -gt 65 ];
        then
		qual_trim=51 # Illumina Qual scores
	else
		qual_trim=20 # Sanger Qual scores
	fi
	
	#run cutadapt
	$write_log_msg5 $my_name 0101 "Running" "cutadapt"
	tmp_cmd="$cutadapt_path/cutadapt $cutadapt_params -q $qual_trim $input_dir/$fastq -o $output_dir/$sample.cutadapt.fastq --too-short-output=$output_dir/${sample}.tooshort.fastq > $output_dir/$sample.cutadapt.log"
	$write_log_msg5 $my_name 0101 "CALLING" "command" "$tmp_cmd "
	         $cutadapt_path/cutadapt $cutadapt_params -q $qual_trim $input_dir/$fastq -o $output_dir/$sample.cutadapt.fastq --too-short-output=$output_dir/${sample}.tooshort.fastq > $output_dir/$sample.cutadapt.log
	# $tmp_cmd
	
	cat $output_dir/$sample.cutadapt.log

	# check to see if expected output file exist
	$write_log_msg5 $my_name 0107 "info" "checking" "to see if expected output file exists "
	if [ ! -s $output_dir/$sample.cutadapt.fastq ]
	then
		$write_log_msg5 $my_name 0110 "ERROR" "empty file" "${output_dir}/${sample}.cutadapt.fastq is empty!"
	fi
	
	if [ ! -s $output_dir/$sample.tooshort.fastq ]
	then
		$write_log_msg5 $my_name 0115 "WARNING" "empty file" "${output_dir}/${sample}.tooshort.fastq is empty!"
	fi
	

	$write_log_msg5 $my_name 0119 "done"
fi	

$write_log_msg5 $my_name 9999 "exiting"
