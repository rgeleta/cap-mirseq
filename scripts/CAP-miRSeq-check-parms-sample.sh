#!/bin/bash

############################################################################
## Description:
## Check validity of sample parameters
##
## @author Robert Geleta (www.rgeleta.com)
## Date: 5/22/15
##
############################################################################

# bash script debugging (set -x = on, set +x = off)
# set -x

############################################################################
# Define Subroutines
# -----------------------------------------------------------------------------
bump_return_code() {
	# for one or more errors_count, set script return to 1
	let error_count=error_count+1
	script_return=1
}
############################################################################

# initialize script return value (0=ok)
script_return=0
error_count=0
# -----------------------------------------------------------------------------
# set shell execution options
# set -e : exit immediately if a simple command fails
set -e
# set -u : treat unset variables as an error (as in "option explicit" or "use strict")
set -u

# -----------------------------------------------------------------------------
# set up log message routine
if [ -z $CAP_MIRNA_LOG_SCRIPT ]
then
	echo "XXXXX Set ENV variable CAP_MIRNA_LOG_SCRIPT=/path/to/logging/script.sh"
	exit 1
fi
log_msg5=$CAP_MIRNA_LOG_SCRIPT
my_name=cap-mirseq-check-parms-sample.sh
# output a blank line to start
echo
$log_msg5 $my_name 0000 "starting" 
# -----------------------------------------------------------------------------
# show values of variables set by calling script

$log_msg5 $my_name 0033 "var" "run_info" $run_info
$log_msg5 $my_name 0034 "var" "sample_info" $sample_info
$log_msg5 $my_name 0035 "var" "tool_info" "$tool_info"

# -----------------------------------------------------------------------------
# CAP-miRseq.sh did not export import_dir, so I will check it here
$log_msg5 $my_name 0100 "checking" 'file/key' "file=run_info, key=INPUT_DIR"
input_dir=$( cat $run_info | grep -w '^INPUT_DIR'    | cut -d '=' -f2 )
if [ "$input_dir" = "" ]
then
	$log_msg5 $my_name 0110 "ERROR" 'missing key' "INPUT_DIR key missing in run_info"
	bump_return_code
else
	$log_msg5 $my_name 0120 'var' 'INPUT_DIR' "$input_dir"
	if [ ! -d $input_dir ]
	then
		$log_msg5 $my_name 0120 "ERROR" "dir not found" "INPUT_DIR"
		bump_return_code
	else
		$log_msg5 $my_name 0130 "info" 'input_dir' "directory exists"
	fi
fi

# -----------------------------------------------------------------------------
# check to see run_info.txt key SAMPLENAMES is there (otherwise what's the point?)
$log_msg5 $my_name 0200 "checking" 'file/key' "file=run_info key=SAMPLENAMES"

# grep option -w : look for only whole words
# cut option -d : delimiter - what to cut on
# cut option -f2 : split

samplename_key=$( cat $run_info | grep -w '^SAMPLENAMES' | cut -d '=' -f1 )

if [ ! "$samplename_key" = "SAMPLENAMES" ]
then
  $log_msg5 $my_name 0210 "ERROR" "run_info" "file $run_info missing SAMPLENAMES key"
  $log_msg5 $my_name 0211 "ERROR" "" "correct and resubmit"
  bump_return_code
  exit $script_return
fi

# -----------------------------------------------------------------------------
# check to see if SAMPLENAMES key has data
$log_msg5 $my_name 0300 "checking" "file exists" "file=run_info key=SAMPLENAMES file exists"

samples=$( cat $run_info | grep -w '^SAMPLENAMES'  | cut -d '=' -f2 )

if [ "$samples" = "" ]
then
	$log_msg5 $my_name 0310 "ERROR" "empty key" "file=run_info key=SAMPLENAMES has no data"
	$log_msg5 $my_name 0311 "ERROR" "" "correct and resubmit"
	bump_return_code
	exit $script_return
fi

$log_msg5 $my_name 0399 "var" "SAMPLENAMES" "$samples"

# -----------------------------------------------------------------------------
# info - how many samples are we dealing with 
sample_count=$(echo $samples | tr ":" "\n" | wc -l )
$log_msg5 $my_name 0400 "var" "sample count" "$sample_count "

# -----------------------------------------------------------------------------
# check sample parameters
i=1
for sample_name in $(echo $samples | tr ":" " ")
do
	$log_msg5 $my_name 0500/$i "checking" "sample_name" "$sample_name "
	
	$log_msg5 $my_name 0510/$i "checking" "file" "$sample_info"
	sample_info_key=$(grep -w "^${sample_name}" $sample_info | cut -d "=" -f1 )
	if [ ! "$sample_info_key" = $sample_name ]
	then
		$log_msg5 $my_name 0521/$i "ERROR" "key not found"  "sample_name in sample_info file $sample_name "
		bump_return_code
		# let $sample_return=$sample_return+1
	else
		sample_info_file=$( grep -w "^${sample_name}" $sample_info | cut -d "=" -f2 )
		$log_msg5 $my_name 0530 'var' 'sample_info_file' "$sample_info_file"
		if [ ! -e $input_dir/$sample_info_file ]
		then
			$log_msg5 $my_name 0531/$i "ERROR" "file not found" "sample_file: $sample_info_file"
			$log_msg5 $my_name 0532/$i "var" "sample_name" "$sample_name"
			$log_msg5 $my_name 0533/$i "var" "filename" "$sample_info_file"
			bump_return_code
		else
			$log_msg5 $my_name 0599/$i "info" "sample file found" "$sample_name"
		fi	
	fi

	let i=i+1
done
# -----------------------------------------------------------------------------
$log_msg5 $my_name 9002 "var" "error_count" "$error_count"
$log_msg5 $my_name 9001 "var" "script_return" "$script_return"
$log_msg5 $my_name 9999 "exiting" 
if [ ! "$error_count" = "0" ]
then
	exit $script_return
fi

