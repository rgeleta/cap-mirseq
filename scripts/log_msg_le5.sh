#!/bin/sh
# log_msg3 - write log messages with 3 parameters

# #####################################################################
# Description:
#   Writes formatted log messages from shell scripts to stdout
#
#   Message is formatted with 
#   ... a standard prefix (to make message stand out in log)
#   ... a date+time stamp 
#   ... up to three parameters (not edited) 
#
# Parameters: (note these are conventions, not absolute requirements)
#   @param string $1 (required) calling script/program - name
#   @param string $2 (required) calling script/program - script area locator number
#   @param string $3 (optional) message type (info ERROR var processing .. whatever, your choice)
#   @param string $4 (optional) msg_short_info (keep data in this field short)
#   @param string $5 (optional) msg_long_info (put longer information here, e.g. pathnames)
#
# @author    Robert Geleta www.rgeleta.com
# @copyright 2015 Robert Geleta 
# @license   cc BY-SA 3.0 US
# 
# #####################################################################
# Notes: 
#
#   the marker "XXXXX" is for message visibility in the log and a basis for grepping messages
#   feel free to change it to suit your preferences
#
#   fields are separated by tabs to facilitate importing and viewing with a spreadsheet program
#   try this:
#      cat script.log | grep '^XXXXX' > script.log.tsv
# #####################################################################
# now go to work
# initialize text field
msg_text=''

# build msg_text based on number of arguments passed
if [ ! $# -lt 3 ] ; then
	msg_text=$msg_text"$3 \t"

	if [ ! $# -lt 4 ] ; then
		msg_text=$msg_text"$4 \t"

		if [ ! $# -lt 5 ] ; then
			msg_text=$msg_text"$5 \t"
		fi
	fi
fi

echo 'XXXXX'"\t"$(date '+%Y-%m-%d %H:%M:%S')"\t"'p='$1"\t"l=$2"\t"$msg_text
#
# end
