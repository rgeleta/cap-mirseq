use strict;
use warnings;
use File::Basename;

###########################################################################
## Description:
## Script to create an excel sheet of Novel miRNA predicted by miRDeep2. All
## samples will be merged onto 1 excel sheet to make it easy to see similar
## predicted miRNA across samples.
##
## Author: Aditya Bhagwate bhagwate.adityavijay@mayo.edu
## Date: 5/22/14
##
## Parameters:
## <BED files list> - A txt file listing the path to each sample's novel BED file from miRDeep2
## <output_dir> - Directory where output excel file should be written
##
############################################################################

if (@ARGV != 2) {
    print "usage : <novel_mirna.pl> <BED file list> <Output directory> \n";
    exit 1;
}

else {

my %h;
my $sample = ''; ## list of samples
my @starts;
my @ends;
my %chrs;

## Read files and extract regions with scores and read counts 
## *****
my $path = $ARGV[1]; ## Output directory
open (F, $ARGV[0]) or die 'Cannot open $ARGV[0] : $!'; ## List of results.bed files
while (my $f = <F> ) {
    chomp $f;
    my $s = '';
    my @parts = split('/', $f);
    if( @parts > 1 ) {
	$s = $parts[@parts - 2];
    } else {
	$s = '/' ;
    }
    $sample .= "$s\t";
    my $dir = dirname($f);
    open (BED, $f) or die 'Cannot open $f : $!';
    opendir(DIR,$dir) or die 'Cannot open directory $dir : $!';
    my @csvfiles = grep(/^result.*\.csv$/,readdir(DIR));
    my $csvfile = $csvfiles[0];
    my $str = "${dir}/${csvfile}";
    
    while (my $l = <BED>) {
	chomp $l;
	if ($l =~ /novel/) {
	    my @a = split('\t', $l);
	    my $chromosome = $a[0];
	    my $start = $a[1];
	    my $end = $a[2];
	    push @starts, $start;
	    push @ends, $end;
	    my $reg = "${start}"."-"."${end}";
	    $chrs{$start} = $chromosome;
	    $chrs{$end} = $chromosome;
	    my @stat = split('\t',`grep $start $str`);
	    my $chr = (split('_',$stat[0]))[0];
	    my $score = $stat[1];
	    my $read_count = $stat[4];
	    $h{$s}{$reg}{"chr"} = $chr;
	    $h{$s}{$reg}{"score"} = $score;
	    $h{$s}{$reg}{"read_count"} = $read_count;
	}
    }
    close BED;
}
close F;
## ****** 

## Create merged intervals for overlapping regions
## ******
my @ranges;
while ( @starts && @ends ) {
    my $s = shift @starts;
    my $e = shift @ends;
    push @ranges, [ $s, $e ];
    
}

my @merged_ranges;
push @merged_ranges, shift @ranges;

my %merged_chr_info;
foreach my $range (@ranges) {
    my $overlap = 0;
    foreach my $m_range (@merged_ranges) {
        if ( ranges_overlap($range,$m_range) ) {
            $overlap = 1;
            $m_range = merge_range($range,$m_range);
        }
    }
    if ( !$overlap ) {
        push @merged_ranges, $range;
    }
}

foreach my $merged (@merged_ranges){
    my $m_start = $merged->[0];
    my $m_end = $merged->[1];
    my $m_str = "$m_start"."-"."$m_end";
    if ($chrs{$m_start} eq $chrs{$m_end}) {
	${merged_chr_info}{$m_str} = $chrs{$m_start};
    }
    else {
    print "WARNING : Overlapping regions between chromosomes in novel_mirna.pl !\n"
    }
}
## *******

## Generate files containing read counts and scores for each sample
## *******
foreach my $samp (sort keys %h) { #Iterate over each sample
    
    open (OUT, ">$path/$samp.novel.tmp") or die 'Cannot open file for writing : $!';
    my %sampletemp;
    foreach my $merged_region (@merged_ranges){ # Iterate over each merged interval
	my $chrom = '';
      
	my $mergestart = $merged_region->[0];
	my $mergeend = $merged_region->[1];
	my $merged_str = "${mergestart}"."-"."${mergeend}";
	if (! exists($sampletemp{$merged_str})){
	    $sampletemp{$merged_str} = undef;
	    
	}
	foreach my $region (keys %{$h{$samp}}){ # Iterate over each region in the sample
	    	    
	    my $regionstart = (split('-', $region))[0];
	    my $regionend = (split('-', $region))[1];
	    
        

	    if ($mergestart <= $regionstart && $regionend <= $mergeend) { #Check if region falls within merged intervals
		
		$sampletemp{$merged_str}{$region}{'read_count'} = $h{$samp}{$region}{"read_count"};
		$sampletemp{$merged_str}{$region}{'score'} = $h{$samp}{$region}{"score"};
	    }   	    
	}      
    }
    print OUT "chr"."\t"."start"."\t"."stop"."\t"."${samp}_novel_miRNA"."\t"."${samp}_ReadCount"."\t"."${samp}_QualityScore"."\n";
    foreach my $tmp (sort keys %sampletemp){
	my $m_start = (split('-', $tmp))[0];
	my $m_stop = (split('-', $tmp))[1];
	my $chrom = $merged_chr_info{$tmp};
	if (defined $sampletemp{$tmp}) {
	    my $reg_str = '';
	    my $readcnt = '';
	    my $quals = '';
	    my $count = 0;
	    foreach my $region (keys %{$sampletemp{$tmp}}){
		$count += 1;
		if ($count <= 1){
		    $reg_str = $reg_str."$chrom".":"."$region";
		    $readcnt = $readcnt."$sampletemp{$tmp}{$region}{'read_count'}";
		    $quals = $quals."$sampletemp{$tmp}{$region}{'score'}";
		}
		else {
		    $reg_str = $reg_str.", "."$chrom".":"."$region";
		    $readcnt = $readcnt.", "."$sampletemp{$tmp}{$region}{'read_count'}";
		    $quals = $quals.", "."$sampletemp{$tmp}{$region}{'score'}";
		}
	    }
	    print OUT "$chrom"."\t"."$m_start"."\t"."$m_stop"."\t"."$reg_str"."\t"."$readcnt"."\t"."$quals"."\n";
		
	    

	}
       else {
	   print OUT "$chrom"."\t"."$m_start"."\t"."$m_stop"."\t"."-"."\t"."-"."\t"."-"."\n";
       }
	
	    
    }
    close OUT;
}
## ******

## FUNCTIONS

## check if ranges overlap
sub ranges_overlap {
    my $r1 = shift;
    my $r2 = shift;
    my $len_r1 = $r1->[1] - $r1->[0];
    my $len_r2 = $r2->[1] - $r2->[0];

    #if ($len_r1 - $len_r2 > 0){
	#my $small = $len_r2;
    #}
    #else {
	#my $small = $len_r1;
    #}
    ## call an overlap only if the overlap is 80% or more (default)
    if ( $r1->[0] <= $r2->[1] && $r2->[0] <= $r1->[1] ) {
	my $start_r2 = $r2->[0];
	my $stop_r1 = $r1->[1];
	my $len_overlap = $stop_r1 - $start_r2;

	my $percent_overlap_r1 = ($len_overlap/$len_r1)*100;
	my $percent_overlap_r2 = ($len_overlap/$len_r2)*100;

	if ($percent_overlap_r1 >= 80 || $percent_overlap_r2 >= 80){
	    return ( $r1->[0] <= $r2->[1] && $r2->[0] <= $r1->[1] );
	}
    }
}

## merge overlapping ranges
sub merge_range {
    my $r1 = shift;
    my $r2 = shift;
    use List::Util qw/ min max/;

    my $merged = [ min($r1->[0],$r2->[0]), max($r1->[1],$r2->[1]) ];
    return $merged;
}
}
