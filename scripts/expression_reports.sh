#!/bin/bash

############################################################################
## Description:
## a script to create raw, normalized, and novel miRNA expression reports
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where per-sample miRDeep2 results are located
## <output_dir> - Directory where merged excel sheets should be written
## <sample names(s1:s2)> - Colon seperated list of sample names
## <script path> - Path to where the CAP-miRSeq scripts are located
##
############################################################################

# -----------------------------------------------------------------------------
# set up log message routine
log_msg5=$HOME/miRNA/log_msg_le5.sh
my_name=expression_reports.sh
# initialize msglog
echo
$log_msg5 $my_name 0000 "starting"
$log_msg5 $my_name 0025 "version" "2015-06-09-1537"
# -----------------------------------------------------------------------------


if [ $# != 4 ];
then
#       echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <script path>";
        echo "usage: <input dir> <output_dir> <sample names(s1:s2)> <script path>";
else
set -x
input_dir=$1
output_dir=$2
samples=$3
script_path=$4
$log_msg5 $my_name 0039 "arg" "\$1 input_dir"   "$1"
$log_msg5 $my_name 0040 "arg" "\$2 output_dir"  "$2"
$log_msg5 $my_name 0041 "arg" "\$3 samples"     "$3"
$log_msg5 $my_name 0042 "arg" "\$4 script_path" "$4"

#mkdir $output_dir/tmp
first_sample=$(echo $samples | cut -d":" -f1)

$log_msg5 $my_name 0047 "info" "" "creating header line"
echo -e "Mature miRNA\tPrecursor\tmiRBase link" > $output_dir/miRNA_expression_raw.xls
sed 1d $input_dir/$first_sample/miRNAs_expressed_all_samples*.csv | cut -f1,3 | awk '{print $1"\t"$2"\t=Hyperlink(\"http://www.mirbase.org/cgi-bin/query.pl?terms="$2"\",\"miRBase\")"}' >> $output_dir/miRNA_expression_raw.xls
cat $output_dir/miRNA_expression_raw.xls > $output_dir/miRNA_expression_norm.xls
touch $output_dir/beds.txt

$log_msg5 $my_name 0053 "info" "" "looping through samples"
# loop through each sample and extract the expression counts from the miRDeep2 output
i=1
for sample in $(echo $samples | tr ":" " ")
do
	$log_msg5 $my_name 0058/$i "processing" "sample" "$sample"
	
	file=$input_dir/$sample/miRNAs_expressed_all_samples*.csv
	echo $input_dir/$sample/result*.bed >> $output_dir/beds.txt 
    
	# raw report
	$log_msg5 $my_name 0064/$i "processing" "raw report"
	echo $sample > $output_dir/$sample.expressed.raw.tmp
	sed 1d $file | cut -f5 >> $output_dir/$sample.expressed.raw.tmp
	paste $output_dir/miRNA_expression_raw.xls $output_dir/$sample.expressed.raw.tmp > $output_dir/miRNA_expression_raw.xls.tmp
	mv $output_dir/miRNA_expression_raw.xls.tmp $output_dir/miRNA_expression_raw.xls
	rm $output_dir/$sample.expressed.raw.tmp
	
	# normalized report
	$log_msg5 $my_name 0072/$i "processing" "normalized report"
	echo $sample > $output_dir/$sample.expressed.norm.tmp
	sed 1d $file | cut -f6 >> $output_dir/$sample.expressed.norm.tmp
	paste $output_dir/miRNA_expression_norm.xls $output_dir/$sample.expressed.norm.tmp > $output_dir/miRNA_expression_norm.xls.tmp
	mv $output_dir/miRNA_expression_norm.xls.tmp $output_dir/miRNA_expression_norm.xls
	rm $output_dir/$sample.expressed.norm.tmp
	

	# precursor tmp report
#	grep nowrap $input_dir/$sample/expression_*.html | awk -F"\t" '{print $1}' | grep pdfs | awk -F"</a>" '{print $1}' | cut -d">" -f4 > $output_dir/tmp/$sample.precursor.reads.txt.tmp
#	grep nowrap $input_dir/$sample/expression_*.html | awk -F"\t" '{print $4}' | grep nowrap | cut -d">" -f2 | cut -d"<" -f1 | paste $output_dir/tmp/$sample.precursor.reads.txt.tmp - > $output_dir/tmp/$sample.precursor.reads.txt
#	rm $output_dir/tmp/$sample.precursor.reads.txt.tmp

	let i=i+1
done


# sometimes there are duplicate rows in the mirdeep2 expression reports. why? who knows..
#uniq $output_dir/miRNA_expression_raw.xls > $output_dir/miRNA_expression_raw.xls.tmp
#uniq $output_dir/miRNA_expression_norm.xls > $output_dir/miRNA_expression_norm.xls.tmp
#mv $output_dir/miRNA_expression_raw.xls.tmp $output_dir/miRNA_expression_raw.xls
#mv $output_dir/miRNA_expression_norm.xls.tmp $output_dir/miRNA_expression_norm.xls

# unique mature miRNA report
$log_msg5 $my_name 0096 "processing" "report" "unique mature miRNA report"
$script_path/uniq_mature_mirna.pl $output_dir/miRNA_expression_raw.xls $output_dir/mature_miRNA_expression.xls

# generate novel miRNA report
$log_msg5 $my_name 0100 "processing" "report" "novel miRNA report"
perl $script_path/novel_mirna.pl $output_dir/beds.txt $output_dir
file=$(ls $output_dir/*.novel.tmp | sort -n | head -1)
$log_msg5 $my_name 0103 "var" "file" "$file"
if [ ! "$file" == "" ]
then
	cat $file | cut -f1-3 > $output_dir/report.tmp.xls
	i=1
	for f in $output_dir/*.novel.tmp
	do
		$log_msg5 $my_name 0108/$i "processing" "file" "$f" 
		$(cat $f | cut -f4-6 | paste $output_dir/report.tmp.xls - > $output_dir/report.tmp); mv $output_dir/report.tmp $output_dir/report.tmp.xls
		let i=i+1
	done
	head -n1 $output_dir/report.tmp.xls > $output_dir/novel_miRNA.xls
	sed 1d $output_dir/report.tmp.xls | perl -p -e 's/^chr([1-9])\b/chr0$1/g'| sort -k1,1 -k2,2n | perl -p -e 's/^chr0([1-9])\b/chr$1/g' >> $output_dir/novel_miRNA.xls
	rm $output_dir/*.novel.tmp
	rm $output_dir/report.tmp.xls
	rm $output_dir/beds.txt
else
	$log_msg5 $my_name 0108 "ERROR" "file not found" "no files $output_dir/\*.novel.tmp"
fi




# check if expected output files exist
if [ ! -s $output_dir/miRNA_expression_raw.xls ]
then
	$log_msg5 $my_name 0130 "ERROR" "file is empty" "${output_dir}/miRNA_expression_raw.xls is empty!"
fi
	
if [ ! -s $output_dir/miRNA_expression_norm.xls ]
then
	$log_msg5 $my_name 0136 "ERROR" "file is empty" "${output_dir}/miRNA_expression_norm.xls is empty!"
fi

if [ ! -s $output_dir/mature_miRNA_expression.xls ]
then
	$log_msg5 $my_name 0142 "ERROR" "file is empty" "${output_dir}/mature_miRNA_expression.xls is empty!"
fi

if [ ! -s $output_dir/novel_miRNA.xls ]
then
    $log_msg5 $my_name 0148 "ERROR" "file is empty" "${output_dir}/novel_miRNA.xls is empty!"
fi

$log_msg5 $my_name 0127 "done"
fi
$log_msg5 $my_name 9999 "exiting"

