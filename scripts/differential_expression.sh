#!/bin/bash

############################################################################
## Description:
## a wrapper script to perform differential expression using edgeR
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input file> - The Mature_miRNA_expression.xls file output from CAP-miRSeq
## <output_dir> - Directory where differential expression results should be written
## <run_info.txt> - CAP-miRSeq run_info.txt config file
##
############################################################################


if [ $# != 3 ];
then
	echo "usage: differential_expression.sh <Mature_miRNA_expression.xls> <output_dir> <run_info>";
	exit 1
fi				
	echo `date`
	input_file=$1
	output_dir=$2
	run_info=$3
	
	sample_info=$( cat $run_info | grep -w '^SAMPLE_INFO' | cut -d '=' -f2)
	tool_info=$( cat $run_info | grep -w '^TOOL_INFO' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)
	diff_expression=$( cat $run_info | grep -w '^DIFF_EXPRESSION' | cut -d '=' -f2)
	diff_expression_analyses=$( cat $run_info | grep -w '^DIFF_EXPRESSION_ANALYSES' | cut -d '=' -f2)

	if [ ! -e $output_dir ]; then mkdir -p $output_dir; fi
	
	set -x

	# plot expression QC boxplots
	Rscript $script_path/expression_QC_boxplots.R $input_file $output_dir

	if [ ! -s $output_dir/expression_boxplots.pdf ]
	then
		echo "ERROR : ${output_dir}/expression_boxplots.pdf is empty!"
		exit 1;
	fi

	# differential expression
	if [ $diff_expression == "YES" ]
	then
		for analysis in $(echo $diff_expression_analyses | tr ":" " ")
		do
			samples=$(cat $sample_info | grep -w "^${analysis}:SAMPLES" | cut -d '=' -f2)
			groups=$(cat $sample_info | grep -w "^${analysis}:GROUPS" | cut -d '=' -f2)
			pairs=$(cat $sample_info | grep -w "^${analysis}:PAIRS" | cut -d '=' -f2)
			Rscript $script_path/differential_expression.R $input_file $output_dir $analysis $samples $groups $pairs
			if [ ! -s $output_dir/$analysis.differential_expression.xls ]
			then
				echo "ERROR : ${output_dir}/${analysis}.differential_expression.xls is empty!"
				exit 1;
			fi
		done
	fi
	
	echo `date`

