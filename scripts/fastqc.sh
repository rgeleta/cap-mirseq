#!/bin/bash

############################################################################
## Description:
## Run FastQC on input fastqs
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <input dir> - Directory where input fastqs are located
## <output_dir> - Directory where FastQC files should be written to
## <sample names(s1:s2)> - Colon seperated list of sample names
## <sample fastqs(fq1:fq2)> - Colon seperated list of fastq files for each sample
## <tool_info> - CAP-miRSeq tool_info.txt config file
## <sample_number> - Optional number indicating which sample to use
##
############################################################################
# set shell options
# set -e : exit immediately if a simple command fails
set -e
# --------------------------------------------------------------------
# set up log message routine
if [ -z $CAP_MIRNA_LOG_SCRIPT ]
then
	echo Set ENV variable CAP_MIRNA_LOG_SCRIPT=/path/to/logging/script.sh
	exit 1
fi
log_msg5=$CAP_MIRNA_LOG_SCRIPT
my_name=fastqc.sh
$log_msg5 $my_name 000 "starting"
# ------------------------------------------------------------------------


# determine whether this script is being called through Sun Grid Engine
if [ -z $SGE_TASK_ID ]; then 
	use_sge=0
else 
	use_sge=1
fi

set -e
set -u 

if [ $# != 5 -a "$use_sge" = "1" ]; then 
	echo "usage: <input dir> <output dir> <sample names(sn1:sn2)> <input fastq names (s1:s2)> <tool_info>";
elif [ $# != 6 -a "$use_sge" = "0" ] ; then 
	echo "usage: <input dir> <output dir> <sample names(sn1:sn2)> <input fastq names (s1:s2)> <tool_info> <sample_number>";
else 
	set -x
	echo `date`
	input_dir=$1
	output_dir=$2
	samples=$3
	fastqs=$4
	tool_info=$5
	
	$log_msg5 $my_name 058 "arg" "\$1 input_dir"   "$1"
	$log_msg5 $my_name 059 "arg" "\$2 output_dir"  "$2"
	$log_msg5 $my_name 060 "arg" "\$3 samples"     "$3"
	$log_msg5 $my_name 061 "arg" "\$4 fastqs"      "$4"
	$log_msg5 $my_name 062 "arg" "\$5 tool_info"   "$5"

	# SGE passes this as part of an array job, but when run standalone the value needs to be passed on the command line
	if [ "$use_sge" = "1" ]; then
		sample_number=$SGE_TASK_ID
	else
		sample_number=$6
	fi

	fastqc_path=$( cat $tool_info | grep -w '^FASTQC_PATH' | cut -d '=' -f2)
	script_path=$( cat $tool_info | grep -w '^SCRIPT_PATH' | cut -d '=' -f2)

	sample=$( echo $samples | tr ":" "\n" | head -$sample_number | tail -1 )
	fastq=$( echo $fastqs | tr ":" "\n" | head -$sample_number | tail -1 )
	
	mkdir -p $output_dir/$sample
	
	#run fastqc
	$log_msg5 $my_name 081 "CALLING" "program" "$fastqc_path/fastqc -o $output_dir/$sample $input_dir/$fastq"
	$fastqc_path/fastqc -o $output_dir/$sample $input_dir/$fastq
	
	$log_msg5 $my_name 900 "done"
fi	
$log_msg5 $my_name 999 "exiting"
