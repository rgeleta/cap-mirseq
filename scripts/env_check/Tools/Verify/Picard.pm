package Tools::Verify::Picard;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Picard'; 
	$self->{filename} = 'SortSam.jar'; 
	$self->{executable} = TRUE ; 
	$self->{execute_via} = "java" ;
	$self->{execute_via_options} = "-jar" ;

	$self->{min_version} = "1.77"; 
	@{$self->{run_dependencies}} = qw/Java/;
} 

sub getDescription { return "Picard.  This should be the path to the directory containing the Picard JAR files."; }

sub getConfigLabel { return "PICARD_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://downloads.sourceforge.net/project/picard/picard-tools/1.92/picard-tools-1.92.zip"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath();
	my $command = $self->getFirstMatchingFilePath($self->getExecuteVia()) . " " . $self->getExecuteViaOptions() . " $filename -version "; 
	
	# TRUE in the execute call determines that the version check can return a non-zero exit code, as picard does here.
	my $output = $self->execute($command, TRUE);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# Version: 1.92(1464)
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^Version:/){ 
			$version = $thisLine ; 
			$version =~ s/^Version: (\d+.\d+).*/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
