package Tools::Verify::RandFold;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'RandFold'; 
	$self->{filename} = 'randfold'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0"; 
} 

sub getDescription { return "RandFold.  This should be the path to the directory containing the randfold binary."; }

sub getConfigLabel { return "RANDFOLD_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://bioinformatics.psb.ugent.be/supplementary_data/erbon/nov2003/downloads/randfold.tar.gz"; }

sub getVersion { 
	my $self = shift ; 
	return undef ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
