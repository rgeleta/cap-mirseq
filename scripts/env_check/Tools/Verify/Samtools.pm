package Tools::Verify::Samtools;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Samtools'; 
	$self->{filename} = 'samtools'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.1.18"; 
} 

sub getDescription { return "Samtools.  This should be the path to the directory containing the samtools binary."; }

sub getConfigLabel { return "SAMTOOLS_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://downloads.sourceforge.net/project/samtools/samtools/0.1.19/samtools-0.1.19.tar.bz2"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename 2>&1";
	my $output = $self->execute($command, TRUE);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^Version: (\d+.\d+.\d+).+/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
