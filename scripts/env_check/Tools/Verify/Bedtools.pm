package Tools::Verify::Bedtools;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Bedtools'; 
	$self->{filename} = 'bedtools'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "2.17.0"; 
} 

sub getDescription { return "Bedtools. This should be the path to the bedtools binary."; }

sub getConfigLabel { return "BEDTOOLS_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "https://bedtools.googlecode.com/files/BEDTools.v2.17.0.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename --version";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^bedtools v(\d+.\d+.\d+)$/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
