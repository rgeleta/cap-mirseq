package Tools::Verify::HTSeqLib;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 

use File::Basename;
use Cwd qw/getcwd abs_path/ ;

sub init {
	my $self = shift ; 

	$self->{name} = 'HTSEQ Lib'; 
	$self->{directory} = 'dist-packages'; 
	$self->{executable} = FALSE ; 
	@{$self->{run_dependencies}} = qw/Python/;
} 

sub getDescription { return "HTSeq Python Lib. This should be the python site-packages or dist-packages directory containing HTSeq library created by the HTSeq install."; }

sub getConfigLabel { return "HTSEQ_LIB_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "https://pypi.python.org/packages/source/H/HTSeq/HTSeq-0.5.3p9.tar.gz"; }


sub getPath { 

        my $script = $0 ;
        my $script_path = dirname(abs_path($script));

	my $pypath_bin = $script_path . "/Tools/paths.py" ;

	my @paths = split(/\n/, `python $pypath_bin`); 

	foreach my $thisPath (@paths){ 
		chomp($thisPath); 
		if(-e $thisPath . "/HTSeq"){ return $thisPath ; }
	}

	return undef ; 
}



sub functionTest { 
	my $self = shift ; 

	my $path = $self->getPath(); 
	if(-f $path){ $path = dirname($path); } 

	my $dir = $path . "/HTSeq"; 
	my $file = $dir . "/_HTSeq.so"; 

	if(!-d $dir){ return "HTSEQ_LIB_PATH missing directory \"$dir\""; }

	if(!-f $file){ return "HTSEQ_LIB_PATH missing file \"file\""; }

	return TRUE ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
