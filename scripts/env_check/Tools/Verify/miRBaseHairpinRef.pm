package Tools::Verify::miRBaseHairpinRef;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'miRBase Hairpin Reference'; 
	$self->{filename} = '/full/path/to/miRBase/Hairpin/reference/fasta'; 
	$self->{executable} = FALSE ; 
} 

sub getConfigLabel { return "MIRBASE_HAIRPIN"; }

sub getConfigType { return "file"; }

sub getDescription { 
	my $self = shift ; 

	my $desc = "miRBase Hairpin Reference Fasta File"; 

	return $desc ; 
}

sub getSource { return "somewhere on your filesystem"; }

sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
