package Tools::Verify::HTSeq;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 

use File::Basename;

sub init {
	my $self = shift ; 

	$self->{name} = 'HTSEQ'; 
	$self->{filename} = 'htseq-count'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.5.3"; 
	@{$self->{run_dependencies}} = qw/Python/;
} 

sub getDescription { return "HTSeq Count.  This should be the path to the htseq-count binary."; }

sub getConfigLabel { return "HTSEQ_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "https://pypi.python.org/packages/source/H/HTSeq/HTSeq-0.5.3p9.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -h";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# Public License v3. Part of the 'HTSeq' framework, version 0.5.3p9
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^.*version (\d+.\d+.\d+).+/$1/ ; 
		}
	}

	return $version ; 
}

sub functionTest { 
	my $self = shift ; 

	my $path = dirname($self->getPath()); 

	# the htseq-qa script has dependencies additional to htseq-count, so test it directly as well.
	my $htseqqa = $path . "/htseq-qa"; 
	if(!-e $htseqqa){ return "htseq-qa does not exist at $htseqqa"; }
	if(!-r $htseqqa){ return "htseq-qa is not readable at $htseqqa"; }
	if(!-x $htseqqa){ return "htseq-qa is not executable at $htseqqa"; }

	my $command = "$htseqqa -h";
	my $output = $self->execute($command); 

	if(!defined($output)){ return "htseq-qa did not execute properly. Please make sure the HTSeq Python library is included in your PYTHONPATH (export PYTHON_PATH=some_path/python2.7/site-packages/) and retry." ; }

	return TRUE ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
