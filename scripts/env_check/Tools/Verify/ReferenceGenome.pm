package Tools::Verify::ReferenceGenome;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Genome Reference'; 
	$self->{directory} = '/full/path/to/reference/fasta/file'; 
	$self->{executable} = FALSE ; 
} 

sub getConfigLabel { return "REF_GENOME"; }

sub getConfigType { return "file"; }

sub getDescription { 
	my $self = shift ; 

	my $desc = "Base Reference Fasta File.  This should be the path to the reference fasta file."; 

	return $desc ; 
}

sub getSource { return "somewhere on your filesystem"; }

# confirm tha tthe fastq index file is present
sub functionTest {
        my $self = shift ;

        my $path = $self->getPath();

	# check for index file

	my $index_path = $path . ".fai"; 
	
	if(!-e $index_path){
		return "Missing index file at \"$index_path\". This can be generated with samtools faidx $path"; 
	}

	return TRUE ; 
}


sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
