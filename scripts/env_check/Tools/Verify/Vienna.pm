package Tools::Verify::Vienna;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'ViennaRNA 1.8.5'; 
	$self->{filename} = 'RNAfold'; 
	$self->{executable} = TRUE ; 
} 

sub getDescription { return "Vienna Package.  This should be the directory containing the ViennaRNA binaries."; }

sub getSource { return "http://www.tbi.univie.ac.at/~ivo/RNA/ViennaRNA-1.8.5.tar.gz"; }

sub getConfigLabel { return "VIENNA_PATH"; }

sub getConfigType { return "dir"; }

sub getVersion { 
	my $self = shift ; 
	return 0 ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
