package Tools::Verify::PDFAPI2;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'PDFAPI2'; 
	$self->{filename} = 'PDF/API2.pm'; 
	$self->{executable} = FALSE ; 
	@{$self->{run_dependencies}} = qw/Perl/;
} 

sub getDescription { return "PDFAPI2 Perl Module.  This should be the path to the Perl library directory to which the PDF::API2 module was installed. It should point to the containing folder, not the PDF directory itself."; }

sub getConfigLabel { return "PDFAPI2_PM_PATH"; }

sub getConfigType { return "dir"; }

# change the returned path in some fashion
sub getPathModification { 
	my $self = shift ; 
	my $path = shift ; 

	$path =~ s/\/PDF\/API2.pm$//; 
	return $path ; 
}


sub getSource { return "http://search.cpan.org/CPAN/authors/id/S/SS/SSIMMS/PDF-API2-2.020.tar.gz"; }

sub getVersion { 
	my $self = shift ; 
	return undef ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
