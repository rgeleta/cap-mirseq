package Tools::Verify::Perl;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Perl 5.10.x'; 
	$self->{filename} = 'perl'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "5.10.0"; 
} 

sub getDescription { return "Perl.  This should be the path to the directory containing the perl binary."; }

sub getSource { return "http://www.cpan.org/src/5.0/perl-5.18.1.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -v 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 
	
	# This is perl 5, version 14, subversion 2 (v5.14.2) built for x86_64-linux-gnu-thread-multi
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^This is perl/){ 
			$version = $thisLine ; 
			$version =~ s/^This is perl.* \(v(\d+.\d+.\d+)\).*/$1/ ; 
			$version =~ s/^This is perl.* v(\d+.\d+.\d+).*/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
