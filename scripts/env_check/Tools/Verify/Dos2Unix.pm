package Tools::Verify::Dos2Unix;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Dos2Unix'; 
	$self->{filename} = 'dos2unix'; 
	$self->{executable} = TRUE ; 
} 

sub getDescription { return "Dos2Unix text format conversion software. Generally available as a system package. \"sudo apt-get install dos2unix\" or \"sudo yum install dos2unix\""; }

sub getSource { return "available through your Linux distribution's package repository"; }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
