package Tools::Verify::CutAdapt;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 

use File::Basename;

sub init {
	my $self = shift ; 

	$self->{name} = 'CutAdapt'; 
	$self->{filename} = 'cutadapt'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.9.5"; 
	@{$self->{run_dependencies}} = qw/Python/;
} 

sub getDescription { return "CutAdapt. The path to the cutadapt binary."; }

sub getConfigLabel { return "CUTADAPT_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://cutadapt.googlecode.com/files/cutadapt-1.3.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename --version";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# 0.9.5
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^(\d+.\d+.\d+).*$/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
