package Tools::Verify::BowtieRefs;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 
use File::Basename ;
use Data::Dumper ; 

sub init {
	my $self = shift ; 

	$self->{name} = 'Bowtie Reference Files'; 
	$self->{directory} = '/path/to/bowtie/references'; 
	$self->{executable} = FALSE ; 
} 

sub getConfigLabel { return "BOWTIE_REF"; }

sub getConfigType { return "wildcard"; }

# change the returned path in some fashion
sub getPathModification {
        my $self = shift ;
        my $path = shift ;

	if(-f $path){ 
		$path =~ s/.fa$//;
	}

	return $path ; 
}

sub getDescription { 
	my $self = shift ; 
	return "Reference and index files required for Bowtie 1.0 ( a fasta file with associated *.ebwt files )"; 
}

sub getSource { return "somewhere on your filesystem"; }

# confirm tha the bowtie index files is present
sub functionTest {
        my $self = shift ;

        my $path = $self->getPath();

	$path =~ s/.fa$//; 

	my @missing ; 

	foreach my $n(1 ... 4){ 
		my $index = $path . "." . $n . ".ebwt" ; 
		if(!-e $index){ push (@missing, $index); }
	}

	foreach my $n(1 ... 2){ 
		my $index = $path . ".rev." . $n . ".ebwt" ; 
		if(!-e $index){ push (@missing, $index); }
	}

	if(@missing < 1){ return TRUE ; }

	push(@missing, "..."); 

	return "Missing the following Bowtie reference files:\n\t\t" . join("\n\t\t", @missing); 


}

sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
