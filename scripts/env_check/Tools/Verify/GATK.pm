package Tools::Verify::GATK;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'GATK'; 
	$self->{filename} = 'GenomeAnalysisTK.jar'; 
	$self->{executable} = TRUE ; 
	$self->{execute_via} = "java" ;
	$self->{execute_via_options} = "-jar" ;

	$self->{min_version} = "1.6"; 
	@{$self->{run_dependencies}} = qw/Java/;
} 

sub getDescription { return "GATK. This should be the path to the GenomeAnalysisTK.jar file."; }

sub getConfigLabel { return "GATK_JAR"; }

sub getConfigType { return "file"; }

sub getSource { return "https://www.dropbox.com/sh/1misjysdehceq4h/Uc7jZNQY3v/GenomeAnalysisTK-1.6-13-g91f02df.tar.bz2"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath();

	my $command = $self->getFirstMatchingFilePath($self->getExecuteVia()) . " " . $self->getExecuteViaOptions() . " $filename -h"; 
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# The Genome Analysis Toolkit (GATK) v1.6-9-g47df7bb, Compiled 2012/06/03 12:26:59
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^The Genome Analysis Toolkit \(GATK\) v/){ 
			$version = $thisLine ; 
			$version =~ s/^The Genome Analysis Toolkit \(GATK\) v(\d+.\d+)-\d.*/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
