package Tools::Verify::Python;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Python 2.6+'; 
	$self->{filename} = 'python'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "2.6.0"; 
} 

sub getDescription { return "Python. This should be the path to the directory containing the python binary."; }

sub getConfigLabel { return "PYTHON_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://www.python.org/ftp/python/2.7.3/Python-2.7.3.tgz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -V 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 
	
	# Python 2.7.3
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^Python/){ 
			$version = $thisLine ; 
			#$version =~ s/^Python (\d+.\d+.\d+)/$1/ ; 
			$version =~ s/^.*(\d+.\d+.\d+).*/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
