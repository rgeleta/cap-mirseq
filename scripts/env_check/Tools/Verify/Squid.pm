package Tools::Verify::Squid;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Squid'; 
	$self->{filename} = 'seqstat'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "1.9"; 
} 

sub getDescription { return "Squid.  This should be the path to the directory containing the squid binaries. We check for seqstat in particular."; }

sub getConfigLabel { return "SQUID_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "ftp://selab.janelia.org/pub/software/squid/squid-1.9g.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -h";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# SQUID 1.9g (Oct 2002)
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^SQUID/){ 
			$version = $thisLine ; 
			$version =~ s/^SQUID (\d+.\d+).*$/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
