package Tools::Verify::CAPmiRseq;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'CAP-miRseq Pipeline'; 
	$self->{filename} = 'CAP-miRseq.sh'; 
	$self->{executable} = TRUE ; 
} 

sub getConfigLabel { return "SCRIPT_PATH"; }

sub getConfigType { return "dir"; }

sub getDescription { return "The CAP-miRseq pipeline's CAP-miRseq.sh workflow script."; }

sub getSource { return "http://bioinformaticstools.mayo.edu/research/cap-mirnaseq"; }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
