package Tools::Verify::VCFTools;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 

use File::Basename;

sub init {
	my $self = shift ; 

	$self->{name} = 'VCFTools'; 
	$self->{filename} = 'vcftools'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.1.11";
	@{$self->{run_dependencies}} = qw/Perl VCFToolsLib/;
} 

sub getDescription { return "VCFTools.  This should be the path to the directory containing the vcftools binary."; }

sub getConfigLabel { return "VCFTOOLS_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://downloads.sourceforge.net/project/vcftools/vcftools_0.1.11.tar.gz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -h";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# VCFtools (v0.1.11)
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^VCFtools \(v(\d+.\d+.\d+)\).+/$1/ ; 
		}
	}

	return $version ; 
}


sub download { 
}

sub build { 
}

sub install { 
}

sub functionTest { 
        my $self = shift ;

        my $path = dirname($self->getPath());

        # the vcftools script has dependencies on VCFTools perl module. This checks that it's in the path
        my $vcftools = $path . "/vcftools";
        if(!-e $vcftools){ return "vcftools does not exist at $vcftools"; }
        if(!-r $vcftools){ return "vcftools is not readable at $vcftools"; }
        if(!-x $vcftools){ return "vcftools is not executable at $vcftools"; }

        my $command = "$vcftools -h";
        my $output = $self->execute($command);

        if(!defined($output)){ return "vcftools did not execute properly. Confirm that the PERL5LIB path includes the path to Vcf.pm and retry." ; return FALSE ; }

        return TRUE ;

}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;



