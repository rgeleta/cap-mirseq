package Tools::Verify::Bowtie;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Bowtie'; 
	$self->{filename} = 'bowtie'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.12.7"; 
	@{$self->{run_dependencies}} = qw/BowtieRefs/;
} 

sub getDescription { return "Bowtie.  This should be the path to the bowtie binary."; }

sub getConfigLabel { return "BOWTIE_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://downloads.sourceforge.net/project/bowtie-bio/bowtie/0.12.9/bowtie-0.12.9-linux-x86_64.zip"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename --version 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /bowtie version/){ 
			$version = $thisLine ; 
			$version =~ s/^.*bowtie version (\d+.\d+.\d+)$/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
