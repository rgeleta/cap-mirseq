package Tools::Verify::FastQC;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'FastQC'; 
	$self->{filename} = 'fastqc'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.10.1"; 
	@{$self->{run_dependencies}} = qw/Java/;
} 

sub getDescription { return "FastQC. This should be the path to the fastqc binary."; }

sub getConfigLabel { return "FASTQC_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -v 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /FastQC/){ 
			$version = $thisLine ; 
			$version =~ s/^FastQC v(\d+.\d+.\d+)/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
