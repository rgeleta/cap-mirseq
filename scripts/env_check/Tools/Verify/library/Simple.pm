package Tools::Verify::Simple;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Simple'; 
	$self->{filename} = 'simple'; 
	$self->{executable} = FALSE ; 
	$self->{min_version} = "1"; 
} 


sub getDescription { return "Samtools"; }

sub getConfigLabel { return "SIMPLE"; }

sub getConfigType { return "dir"; }
#sub getConfigType { return "file"; }

sub getSource { return ""; }

sub getVersion { 
	my $self = shift ; 
	return 1 ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
