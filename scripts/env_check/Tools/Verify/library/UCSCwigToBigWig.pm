package Tools::Verify::UCSCwigToBigWig ;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'BLAT wigToBigWig'; 
	$self->{filename} = 'wigToBigWig'; 
	$self->{executable} = TRUE ; 
} 

sub getDescription { return "UCSC wigToBigWig"; }

sub getConfigLabel { return "UCSC_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/wigToBigWig"; }

sub getVersion { 
	my $self = shift ; 
	return "0.0"; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
