package Tools::Verify::FileSet;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'File Set Example'; 
	$self->{directory} = '/tmp'; 
	@{$self->{fileset}} = qw/1.txt 2.txt 3.txt/; 
	$self->{executable} = FALSE ; 
} 

sub getDescription { return "FileSet Example"; }

sub getSource { return "somewhere on your filesystem"; }

sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
