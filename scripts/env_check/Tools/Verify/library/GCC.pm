package Tools::Verify::GCC;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'GNU C Compiler'; 
	$self->{filename} = 'gcc'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "4.4.5"; 
} 

sub getDescription { return "GCC Compiler Toolchain"; }

sub getSource { return "some system package"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $output = `$filename -v 2>&1`;

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /gcc version/){ 
			$version = $thisLine ; 
			$version =~ s/^gcc version (\d+.\d+.\d+).+/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
