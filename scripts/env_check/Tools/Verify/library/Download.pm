package Tools::Verify::Download;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 
use File::Basename ; 

sub init {
	my $self = shift ; 

	$self->{name} = 'Download Example'; 
	$self->{filename} = 'foo'; 
	$self->{executable} = TRUE ; 
} 

sub getDescription { return "Download Example"; }

sub getConfigLabel { return "EXAMPLE_PATH"; }

sub getConfigType { return "file"; }

sub getSource { return "http://downloads.sourceforge.net/project/bio-bwa/bwa-0.7.5a.tar.bz2"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -version 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /java version/){ 
			$version = $thisLine ; 
			$version =~ s/^java version \"(\d+.\d+.\d+).+\"/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
	my $self = shift ; 

	my $url = $self->getSource(); 

	my $prefix = Tools::Verify::getPrefix(); 

	print "prefix: $prefix\n"; 

	my $filename = basename($url); 

	my $wget = `which wget` ; 
	my $curl = `which curl` ; 

	chomp($wget); 
	chomp($curl); 

	print "$wget\n"; 
	print "$curl\n"; 

	if(-e $wget && -x $wget){ 
		my $result = `$wget $url -o $prefix/$filename`; 	
	} elsif( -e $curl && -x $curl){ 
		my $result = `$curl -L $url -o $prefix/$filename`; 	
	}



}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
