package Tools::Verify::Java;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Java Runtime Environment'; 
	$self->{filename} = 'java'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "1.7.0"; 
} 

sub getDescription { return "Java"; }

sub getSource { return "http://java.oracle.com"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $output = `$filename -version 2>&1`;

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /java version/){ 
			$version = $thisLine ; 
			$version =~ s/^java version \"(\d+.\d+.\d+).+\"/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
