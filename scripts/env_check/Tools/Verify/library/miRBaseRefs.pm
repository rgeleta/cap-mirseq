package Tools::Verify::miRBaseRefs;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'miRBase Reference Files'; 
	$self->{directory} = '/full/path/to/miRBase references'; 
	@{$self->{fileset}} = 
	qw/
		hairpin.dre.dna.fa
		hairpin.fa
		hairpin.hsa.dna.fa
		hairpin.mmu.dna.fa
		hairpin.rno.dna.fa
		hsa.gff3
		mature.dre.dna.fa
		mature.fa
		mature.hsa.dna.fa
		mature.mmu.dna.fa
		mature.rno.dna.fa
		organisms.txt
	/; 
	$self->{executable} = FALSE ; 
} 

sub getDescription { 
	my $self = shift ; 

	my $desc = "Reference Files required for miRBase:\n"; 

	foreach my $file ( @{$self->{fileset}} ){ 
		$desc .= "\t$file\n"; 
	}

	return $desc ; 
}

sub getConfigLabel { return "MIRBASE_REFS"; }

sub getConfigType { return "file"; }

sub getSource { return "somewhere on your filesystem"; }

sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
