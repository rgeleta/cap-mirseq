package Tools::Verify::Circos;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Circos'; 
	$self->{filename} = 'circos'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "0.64"; 
} 

sub getDescription { return "Circos"; }

sub getConfigLabel { return "CIRCOS_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://circos.ca/distribution/circos-0.64.tgz"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -version 2>&1";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# circos | v 0.64 | 2 May 2013
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /circos/){ 
			$version = $thisLine ; 
			$version =~ s/^circos \| v (\d+.\d+).+/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
