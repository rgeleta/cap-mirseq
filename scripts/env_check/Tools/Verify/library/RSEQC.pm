package Tools::Verify::RSEQC;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 

use File::Basename;

sub init {
	my $self = shift ; 

	$self->{name} = 'RSEQC'; 
	$self->{filename} = 'inner_distance.py'; 
	$self->{executable} = TRUE ; 
	$self->{min_version} = "2.3.7"; 
	@{$self->{run_dependencies}} = qw/Python/;
} 

sub getDescription { return "RSEQC with Mayo custom patches."; }

sub getConfigLabel { return "RSEQC_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return ""; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 

	my $command = "$filename -h";
	my $output = $self->execute($command);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 

	# inner_distance.py 2.3.7
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /Version/){ 
			$version = $thisLine ; 
			$version =~ s/^inner_distance (\d+.\d+.\d+).+/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
