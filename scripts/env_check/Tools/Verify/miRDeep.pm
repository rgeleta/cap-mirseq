package Tools::Verify::miRDeep;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'miRDeep2'; 
	$self->{filename} = 'miRDeep2.pl'; 
	$self->{executable} = TRUE ; 
	$self->{execute_via} = "perl" ; 
	$self->{min_version} = "0.10.1"; 
	@{$self->{run_dependencies}} = qw/Perl miRBaseGFF  miRBaseHairpinRef  miRBaseMatureRef/;
} 

sub getDescription { return "miRDeep2.  This should be the path to the directory containing the miRDeep2.pl script."; }

sub getConfigLabel { return "MIRDEEP2_PATH"; }

sub getConfigType { return "dir"; }

sub getSource { return "https://www.mdc-berlin.de/38350089/en/research/research_teams/systems_biology_of_gene_regulatory_elements/projects/miRDeep/mirdeep2_0_0_5.zip"; }

sub getVersion { 
	my $self = shift ; 

	my $filename = $self->getPath(); 


	my $command = $self->getFirstMatchingFilePath($self->getExecuteVia()) . " " . $self->getExecuteViaOptions() . " $filename -h";

	my $output = $self->execute($command, TRUE);

	my $version = undef ; 

	my @lines = split(/\n/, $output); 
	
	# # miRDeep2.0.0.5
	foreach my $thisLine (@lines){ 
		chomp($thisLine); 
		if($thisLine =~ /^# miRDeep/){ 
			$version = $thisLine ; 
			$version =~ s/^# miRDeep(\d+.\d+.\d+\.\d+).*/$1/ ; 
		}
	}

	return $version ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
