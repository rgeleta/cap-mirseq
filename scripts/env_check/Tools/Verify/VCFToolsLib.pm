package Tools::Verify::VCFToolsLib;

@ISA = qw(Tools::Verify); 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'VCFTools Perl Library'; 
	$self->{filename} = 'Vcf.pm'; 
	$self->{executable} = FALSE ; 
	@{$self->{run_dependencies}} = qw/Perl/;
} 

sub getDescription { return "VCFTools Perl Library.  This should be the path to the perl library directory to which the VCFTools perl libraries have been installed. This is often the lib/perl5/site_perl/ directory within the VCFTools install directory."; }

sub getConfigLabel { return "VCFTOOLS_PERLLIB"; }

sub getConfigType { return "dir"; }

sub getSource { return "http://downloads.sourceforge.net/project/vcftools/vcftools_0.1.11.tar.gz"; }

sub getVersion { 
	my $self = shift ; 
	return undef ; 
}

sub download { 
}

sub build { 
}

sub install { 
}

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
