package Tools::Verify::GencodeGTF;

@ISA = qw(Tools::Verify); 

our $state = undef ; 

use Tools::Verify ; 
use strict ; 
use warnings ; 


sub init {
	my $self = shift ; 

	$self->{name} = 'Gencode GTF file'; 
	$self->{filename} = '/full/path/to/gencode/gtf'; 
	$self->{executable} = FALSE ; 
} 

sub getConfigLabel { return "GENCODE_GTF"; }

sub getConfigType { return "file"; }

sub getDescription { 
	my $self = shift ; 

	my $desc = "GENCODE General Transfer Format (GTF) file. "; 
	$desc .= "These files are available for download from http://www.gencodegenes.org/data.html"; 

	return $desc ; 
}

sub getSource { return "somewhere on your filesystem"; }

sub download { }

sub build { }

sub install { }

sub new { 
	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {} ; 
	bless $self, $class; 
	$self->{package} = __PACKAGE__ ; 
	$self->init(); 
	return $self; 
}

1;
