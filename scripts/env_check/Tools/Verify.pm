package Tools::Verify;

use strict ;
use warnings ; 
use File::Basename ; 
use File::Copy ;
use File::Temp qw/tempfile/ ;
use version ;
use Exporter 'import'; 
use Data::Dumper ; 
use Date::Format ;

use constant TRUE => 1 ; 
use constant FALSE => 0 ; 
our @EXPORT = qw(TRUE FALSE);

# This module runs modules in Tools/Verify/, helping the user to identify resources required to execute a workflow. 

# Modules are loaded from the filesystem and instantiated.
# Each is checked for dependencies on other modules.
# Toolinfo files are parsed if provided and used as a basis for checking 
# Toolinfo provided values are checked first.
# Once a module resource is found, it's tested for readability, version, etc
# Modules can provide additional tests.
# Once all the resources are identified and validated, the toolinfo file is updated.
# a PATH value is supplied to the user for modules not represented in the toolinfo file.

# basic class methods
#	new 
#	init()
#		loads modules from Tools/Verify/*.pm  
#
#	listModules()
#		displays the set of modules found in the Tools/Verify/ directory
#
#
# Module check logic and UI 
#	checkModules()
#		iterates over found modules, checks for intra-module dependencies, checks the modules themselves
#
#	missingModuleDependencies()
#		checks whether all the dependencies for a particular module have been defined
#
#	completeModuleDependencies()
#		checks whether all the dependencies for a particular module have been met
#
#	checkModule()
#		tests a specific module
#
#	retryCheckModule()
#		if the module failed, give the user a chance to re-state the path and try again
#
#	askUser()
#		user prompt
#
#	getPathFromUser()
#		collect a new path from the user
#
#
# Methods for looking up where to find module resources
#	getPath()
#		Collects a putative path from the module, toolinfo_file, or asks the user
#
#	mergePossiblePaths()
#		collect paths from the environment and supplied by the user
#
#	getFirstMatchingPath()
#		searches for the path
#
#	getFirstMatchingFilePath()
#		looks for files or directories that match the provided filename
#
#	getFirstMatchingWildcardPath()
#		looks for files or directories that match the provided starting substring
#
#	getPathEnv()
#		the newPathEnv variable contains all the paths necessary to find binaries that do not have a corresponding configLabel in the toolinfo
#
#
# Methods for testing module
#	versionOK()
#		compares version value to min/max/specific version, both supplied by the module
#
#	getVersionError()
#		return the type of version error
#
#	filesetComplete()
#		checks that all the files required by the module are present in the specified path
#
#	functionTest()
#		stub to provide a method modules can define for extended tests
#
#	execute()
#		executes a command, saves exit code, stdout, stderr
#
#	mustExecute()
#		assumes no execution test if not specified
#
#	getExecuteVia()
#		how to execute a command ( via Java, Perl, Python, Base, etc )
#
#	getExecuteViaOptions()
#		command line options for executing a command
#
#
# Toolinfo file manipulation
#
#	parseToolInfo()
#		reads the toolinfo file into a hash
#
#	updateToolInfo()
#		rewrites the specified toolinfo file with module supplied paths
#
#
# Module get/set methods 
#
#	getName()
#		module name
#
#	getFilename()
#		file the module is looking for
#
#	getDirectory()
#		directory the module is looking for
#
#	getPackage()
#		module package name ( Tools::Verify::ModuleName )
#
#	getDescription()
#		tool description
#
#	getPathModification()
#		gives the module an opportunity to customize the path
#
#	getConfigLabel()
#		the label use in toolinfo files
#
#	getConfigType()
#		file, dir, wildcard ( the type of entry expected in the toolinfo file)
#
#	getVersion()
#		if no version is supplied by the module the version isn't considered
#
#	getSearchPath()
#		locations to search for binaries
#
#	setSearchPath()
#		user supplied search path
#
#	getPrefix()
#		install directory path
#
#	setPrefix()
#		user supplied install prefix
#
#	getToolinfo()
#		workflow configuration file path
#
#	setToolinfo()
#		user supplied toolinfo file path
#
#	setDebug()
#		debug mode set
#
#	getDebug()
#		debug mode get
#
#	enableDownload 
#       downloadOK
#	enableInstall 
#       installOK
#	runDependencies 
#	buildDependencies 
#

my @modules ; 			# array of perl modules for each tool / file to test
my %tool_status ; 		# hash containing details about each module check
my %initial_toolinfo_values ; 	# values from parsed from the toolinfo file
my @newPathEnv = ();		# keep track of paths the user must add to their environment
my @searchPath = ();		# search path specified on the command line
my $debug ;			# debug level 

#sub log_msg {
#	print "\n" . "XXXXX" . "\t" . "YYYY-MM-DD-HHMM" . "\t" . "Verify.pm" . "::" .  $_[0] . "\t" . $_[1] . "\n" ;
#	print "\n" . "XXXXX" . "\t" . time2str("%Y-%m-%d %X", time) . "\t" . "Verify.pm()" . "::" .  $_[0] . "\t " . $_[1] . "\n" ;
#}

sub log_msg4 {

#	if (! $self->getDebug() ) 
#	{ return ; }

	my $msg_text = '' ;
	my $num_args = @_ ;
	# additional space after tab char is to force spreadsheet cell to text
	if  ( $num_args > 1 ) 
	{	$msg_text = "l=" . $_[1] ;
		if  ( $num_args > 2 )
		{	$msg_text .= "\t " . $_[2] ;
			if  ( $num_args > 3 ) 
			{	$msg_text .= "\t " . $_[3] ;
				if  ( $num_args > 4 )
				{	
					$msg_text .= "\t " . $_[4]
				}
			}
		}
	}
	print "\n" . "XXXXX" . "\t" . time2str("%Y-%m-%d %X", time) . "\t" . "Verify.pm" . "::" .  $_[0] . "\t" . $msg_text . "\n" ;
}

sub new { 

	my $this = shift ; 
	my $class = ref($this) || $this ; 
	my $self = {};
	bless $self, $class;
	return $self ; 

}

# loads modules from Tools/Verify/*.pm 
sub init { 
	my $self = shift ; 
	my $libPath = shift ; 

	$self->{package} = __PACKAGE__ ; 

	my @modulePaths = glob($libPath . "/Tools/Verify/*.pm"); 

	foreach my $thisModule ( @modulePaths ){
		my $module = basename($thisModule); 
		$module =~ s/.pm$//; 

		$module = "Tools::Verify::$module"; 

		eval "require $module"; 

		if($@){
			warn "Unable to load package $module: " . $@ . "\n"; 
		} else { 
			push(@modules,$module); 
		}
	}

	# some semblance of order ...
	@modules = sort(@modules);

}

# displays the set of modules found in the Tools/Verify/ directory
sub listModules { 
	my $self = shift ; 

	foreach my $thisModule (sort(@modules)){
		my $module = $thisModule->new(); 
		print "\t" . $module->getDescription() . "\n"; 
	}
}

#### 
#### Module check logic and UI
####

# iterates over found modules, checks for intra-module dependencies, checks the modules themselves
sub checkModules {

	my $my_name = "checkModules" ;
#	log_msg($my_name, "000" . "\t" . "entered") ;
	log_msg4($my_name, "000", "entered" ) ;

	my $self = shift ; 

#	log_msg($my_name, "010" . "\t" . "var" . "\t" . "self" . "\t" . "\t" . "[" . $self . "]" ) ;
	log_msg4($my_name, "010", "var", "self", "[" . $self . "]" ) ;
	# performs a check of ALL modules N times where N is the number of modules
	# this is done as a simple way to check that all module dependencies have
	# had a chance to be satisfied (because all modules have been reviewed) before giving up.
	for(1 ... @modules){

		# check each module
		foreach my $thisModule (@modules){

			log_msg4($my_name, "100", "checking", "module", "[" . $thisModule . "]" ) ;

			my $module = $thisModule->new(); 

			# if the module has already been marked complete, skip the recheck
			if($tool_status{$module->getPackage}{complete}){ next ; }

			# if the module was incomplete and the user elected to skip it, do not recheck
			if($tool_status{$module->getPackage}{skip}){ next ; }

			# make certain that all the required modules are defined
			if($module->missingModuleDependencies('run')){ exit 1 ; }

			# make certain that all the require modules are complete, otherwise, continue in an attempt to satisfy them
			if(!$module->completeModuleDependencies('run')){ next ; }

			# confirm the the module's file exists, is executable, readable, and has the correct version
			my $result = $module->checkModule(); 

			# retry with user input if the correct file is not found on the system
			if($result){ 
				print "OK\n"; 

				# save the modules path result to the array used to store the required $PATH env variable
				# if it's not otherwise specified in the toolinfo file. This way we can inform users
				# about the path they need to include before running the workflow.
				if(!defined($module->getConfigLabel())){ 
					if(-f $module->getPath()){ 
						push(@newPathEnv, dirname($module->getPath()));
					} else { 
						push(@newPathEnv, $module->getPath());
					}
				}
			} else {
				# if the check failed, the user is given an opportunity to re-specify the path and try again 
				$result = $module->retryCheckModule(); 
			}

			$tool_status{$module->getPackage}{complete} = $result ; 
		}
	}


	# display a summary of unmet dependencies, conditions to rectify them
	my $checkPassed = TRUE ; 
	foreach my $thisModule (@modules){
		my $module = $thisModule->new(); 

		my $moduleFailed = FALSE ; 
		if(!$tool_status{$module->getPackage()}{complete}){ 
			$moduleFailed = TRUE ; 
		}

		if($tool_status{$module->getPackage()}{complete} && $tool_status{$module->getPackage()}{complete} == 0){
			$moduleFailed = TRUE ; 
		}

		if($moduleFailed == TRUE){ 
			print "*** WARNING: Unable to find " . $module->getName() . "\n"; 
			my $complete = $module->completeModuleDependencies('run','summary');
			$checkPassed = FALSE ; 
		}
	}

	# the tool_status hash contains all the information gathered for each module
	if($self->getDebug()){ 
		print Dumper \%tool_status ; 
	} 

	# update the toolinfo file if specified by the user
	$self->updateToolInfo(); 
	
	log_msg4($my_name, "900", "exiting", "returning", "[" . $checkPassed . ']' ) ;

	return $checkPassed ; 
}

# checks whether all the dependencies for a particular module have been defined through other modules
# in the set.  This does not check whether the items being checked for by those modules are satisfied
# only that they are defined. 
sub missingModuleDependencies { 
	my $self = shift ; 
	my $type = shift ; 

	# collect dependencies
	my @dependencies ; 
	
	if(!$type || ( $type ne 'run' && $type ne 'build')){ 
		my($package, $filename, $line) = caller;
		print "Run dependency type not specified in $package file $filename line $line\n"; 
		exit 1 ; 
	}

	if($type eq 'run'){ @dependencies = $self->runDependencies(); }
	if($type eq 'build'){ @dependencies = $self->buildDependencies(); }

	# check for dependency requirements that are undefined (missing Tools::Verify::* modules)
	my @missingModules = (); 

	foreach my $thisDepStub (@dependencies){

		my $thisDep = "Tools::Verify::" . $thisDepStub ; 

		# make sure the dependency has a correspnding module
		my $found = FALSE ; 

		foreach my $thisModule (@modules){
			if($thisModule eq $thisDep){ $found = TRUE ; }
		}	

		if($found == FALSE){ 
			push(@missingModules, $thisDep); 
		}
	}

	if($#missingModules > -1){
		print "Missing required modules for " . $self->getPackage() . ":\n"; 
		foreach my $missing (@missingModules){ print "   $missing\n"; }
		print "This is a defect in the the env_check script. These modules should be included in the Tools/Verify directory.\n"; 
		return @missingModules ; 
	}

	return undef ; 
}

# checks whether a module's dependencies have been met. It does this before attempting to
# check the module itself. 
sub completeModuleDependencies { 
	my $self = shift ; 
	my $type = shift ; 
	my $summary = shift ; 


	# collect dependencies
	my @dependencies ; 
	
	if(!$type || ( $type ne 'run' && $type ne 'build')){ 
		my($package, $filename, $line) = caller;
		print "Run dependency type not specified in $package file $filename line $line\n"; 
		exit 1 ; 
	}

	if($type eq 'run'){ @dependencies = $self->runDependencies(); }
	if($type eq 'build'){ @dependencies = $self->buildDependencies(); }

	# check for dependency requirements that are undefined (missing Tools::Verify::* modules)
	my @missingModules = (); 
	my $complete = TRUE ; 

	foreach my $thisDepStub (@dependencies){

		my $thisDep = "Tools::Verify::" . $thisDepStub ; 

		# make sure the dependency has a correspnding module

		if(!$tool_status{$thisDep}{complete}){ 
			push(@missingModules, $thisDep);  
			$complete = FALSE ; 
		}
	}

	if($summary && !$complete){ 

		# save state
		$tool_status{$self->getPackage}{complete} = 0 ; 

		# collect error detail
		my $error ; 
		$error = "Module " . $self->getPackage() . " is unavailable because of the following unmet dependencies:\n"; 
		foreach my $missing (@missingModules){
			#$error .= "   $missing\n"; 

			my $module = $missing->new(); 
			$error .= "   " . $module->getDescription() . "\n"; 
		}
		
		$tool_status{$self->getPackage}{error} = $error ; 

		print $error . "\n"; 

	}	

	return $complete ; 
}

# tests a specific module
sub checkModule {
	my $self = shift ; 

	my $my_name = "checkModule" ;
	log_msg4($my_name, "000", "entered", "", "[" . $self->getName . "]" ) ;
	print "Checking for " . $self->getName() . " -- "; 

	# look for the item specified by the module
	my $path = $self->getPath();
	log_msg4($my_name, "100", "checking", "package path", "[" . $path . "]" ) ;
	$tool_status{$self->getPackage()}{path} = $path ; 

	# prepare a place for notes about the module's status
	log_msg4($my_name, "120", "processing", "", "preparing a place for notes" ) ;
	@{$tool_status{$self->getPackage()}{notes}} = (); 
	@{$tool_status{$self->getPackage()}{errors}} = (); 

	# check if a path has been defined
	if($path eq ""){
		push(@{$tool_status{$self->getPackage()}{errors}}, "no path specified"); 
		return FALSE ; 
	}
 
	# Collecting the type to idenfity file wildcards.
	# These are essentially wild-card patterns that will not match an actual file
	# They form the base path for a set of files (i.e. a bowtie reference translates to a series of ebwt files)
	my $type = $self->getConfigType(); 
	my $skip_file_check = FALSE ; 
	if(defined($type) && $type eq "wildcard"){ $skip_file_check = TRUE ; }

	# basic file checks
	if(!$skip_file_check){

		# check if the path exists 
		if(! -e $path ){
			push(@{$tool_status{$self->getPackage()}{errors}}, "Path \"$path\" does not exist"); 
			return FALSE ; 
		}

		# check if the path is readable 
		if(! -r $path ){
			push(@{$tool_status{$self->getPackage()}{errors}}, "Path \"$path\" is not readable"); 
			return FALSE ; 
		}
	}


	# if the path must be executable, check that is is.
	log_msg4($my_name, "490", "checking", "path", "if path must be executable" ) ;
	if($self->mustExecute()){

		# confirm that the path points to an actual file
		if(!-f $path){
			push(@{$tool_status{$self->getPackage()}{errors}}, "Path \"$path\" is not a file"); 
			return FALSE ; 
		}

		# if the file should be executable on its own, confirm that it is.
		if(!$self->getExecuteVia() && ! -x $path){
			push(@{$tool_status{$self->getPackage()}{errors}}, "Path \"$path\" is not executable"); 
			return FALSE ; 
		}
	}


	# check for a file version error
	log_msg4($my_name, "510", "checking", "", "for file version error" ) ;
	if(!$self->versionOK()){
		push(@{$tool_status{$self->getPackage()}{errors}}, $self->getVersionError() ); 
		log_msg4($my_name, "511", "exiting", "returning", "FALSE" ) ;
		return FALSE ; 
	}

	# if a file set is specified, make certain that all the elements are present

	log_msg4($my_name, "520", "checking", "fileset" ) ;
	if(!$self->filesetComplete()){ 
		log_msg4($my_name, "521", "info", "", "file set is complete" ) ;
		log_msg4($my_name, "522", "exiting", "returning", "FALSE" ) ;
		return FALSE ; 
	}

	# additional tests can be defined within the module.  This can be used to:
	#  1) look for additional files associated with the primary 
	#  2) run arbitrary commands and check their exit codes, output
	#  3) anything that would make a good test for the module
	log_msg4($my_name, "530", "running", "method", "$self" . "->functionTest()" ) ;
	my $testResult = $self->functionTest(); 
	unless($testResult eq TRUE){
		push(@{$tool_status{$self->getPackage()}{errors}}, $testResult); 
		log_msg4($my_name, "532", "exiting", "returning", "FALSE" ) ;
		return FALSE ; 
	} 

	# save status
	push(@{$tool_status{$self->getPackage()}{notes}}, "Path \"$path\" Found OK");

	log_msg4($my_name, "590", "exiting", "returning", "TRUE" ) ;
	return TRUE ; 
}

# if the module failed, give the user a chance to re-state the path and try again
sub retryCheckModule { 
	my $self = shift ; 

	my $result = FALSE ; 
	my $continue = TRUE ; 
	

	# loop until the user gives up
	while($continue){
		print "Failed for the following reasons:\n"; 
		foreach my $reason ( @{$tool_status{$self->getPackage()}{errors}} ){
			print "\t$reason\n"; 
		}

		if(askUser("Try Again")){

			# display information about the module to aid the user in locating the files
			print $self->getDescription(); 

			# collect a new path
			$self->{path} = getPathFromUser($self->{path}); 
			delete $tool_status{$self->getPackage()}; 

			# rerun the check
			$result = $self->checkModule(); 

			# succeed or ask to try again
			if($result){ 
				print "OK\n"; 
				$continue = FALSE; 
			} else { 
				print "Failed\n"; 
			}

		} else {
			# the user gave up 
			$tool_status{$self->getPackage()}{skip} = TRUE ; 
			$continue = FALSE ; 
		}
	}

	return $result ; 
}

# user prompt
sub askUser { 
	my $prompt = shift ; 
	my $retry = undef ; 

	while(){ 
		print "$prompt (y/n): "; 
		my $choice = <STDIN>; 
		chomp($choice);
		$choice = lc($choice);

		if($choice eq 'yes'){ return TRUE ; }
		if($choice eq 'y'){ return TRUE ; }
		if($choice eq 'no'){ return FALSE ; }
		if($choice eq 'n'){ return FALSE ; }
	}
}

# collect a new path from the user
sub getPathFromUser { 
	my $old_path = shift ; 
	print "Please specify a path ($old_path): "; 
	my $path = <>; 

	chomp($path);
	$path =~ s/\/+$//;

	if($path eq ""){ $path = $old_path ; }
 
	return $path ; 
}

#### 
#### Methods for looking up where to find module resources
####

# Collects a putative path from the module, toolinfo_file, or asks the user if one is not defined.
# Then checks the filesystem via getFirstMatchingPath.
sub getPath {
	my $self = shift ; 

	# if the path is already known just return it
	if(defined($self->{path})){ return $self->{path}; }

	my $filename = $self->getFilename(); 
	my $directory = $self->getDirectory(); 

	# check that the module a single path, file or directory
	if(!defined($filename) && !defined($directory)){ 
		print "Module " . $self->getPackage() . " failed to define either a filename or a directory.\n"; 
		print "Please choose one or the other.\n"; 
		exit 1 ; 
	}

	# check that the module only chooses a single path, file or directory, not both.
	if(defined($filename) && defined($directory)){ 
		print "Module " . $self->getPackage() . " defined both a filename (\"$filename\") and a directory (\"$directory\").\n"; 
		print "Please choose one or the other.\n"; 
		exit 1 ; 
	}

	my $type = undef ; 
	my $path = undef ; 

	# set the default directory path from the module
	if(defined($directory)){ 
		$type = "Directory"; 
		$path = $directory ; 
	}

	# set the default filename path from the module
	if(defined($filename)){ 
		$type = "File"; 
		$path = $filename ; 
	}


	# if there's a value in the toolinfo file, use it instead of the module default
	my $configLabel = $self->getConfigLabel(); 
	my $configType = $self->getConfigType(); 

	if(defined($configType) && defined($configLabel)){ 
		my $configValue = $initial_toolinfo_values{$configLabel}; 
		if(defined($configValue) && $configValue ne ""){ 


			# the path should include the full file being sought, even if the toolinfo file only records 
			# the directory. The following appends the module supplied filename to the  toolinfo supplied
			# directory. If toolinfo specifies a full file path, it uses that. 

			if($configType eq "dir" && -d $configValue){ $path = $configValue . "/" . $path ; }

			if($configType eq "file" && -d $configValue){ $path = $configValue . "/" . $path ; }

			if($configType eq "file" && -f $configValue){ $path = $configValue ; }

			if($configType eq "wildcard"){ $path = $configValue ; }
		}
	}

	# if we still have nothing, ask the user.
	if($path eq ""){
		print "$type path for " . $self->getName() . " unknown.\n"; 
		$path = getPathFromUser($path); 
	}

	# once a putative path has been provided, check for it on the filesystem 
	$self->{path} = $self->getFirstMatchingPath($path); 

	# if getFirstMatchingPath came up with nothing, revert to the version originally specified
	# the module check based on this path may fail, but then the user will be prompted to try again.
	if($self->{path} eq ""){ $self->{path} = $path ; }

	return $self->{path}; 
}

# collect paths from the environment and supplied by the user
# these are used to look for module resources
sub mergePossiblePaths { 
	my $self = shift ; 
	my $toolinfoPath = shift ; 

	# search path specified by the user
        my @paths = $self->getSearchPath();
	
	# search path specified by the environment
        push(@paths, split(/:/,$ENV{PATH}));

	# add an empty path in case the user has supplied a full path already
	unshift(@paths, ""); 

        # add the toolinfo file supplied a path, check it first. 
        if(defined($toolinfoPath)){ unshift(@paths, $toolinfoPath); }

	return @paths ; 
}

# searches for the path 
sub getFirstMatchingPath { 
	my $self = shift ; 
	my $filename = shift ; 

	# modules fail if they do not supply a filename
	if(!defined($filename) || $filename eq ""){ die("No filename supplied for " . $self->getPackage() . "\n"); }

	# collect values from the toolinfo file
	my $toolinfoLabel = $self->getConfigLabel(); 
	my $configType = $self->getConfigType(); 
	my $toolinfoValue = undef ; 
	if(defined($toolinfoLabel)){
		if(defined($initial_toolinfo_values{$toolinfoLabel})){
			$toolinfoValue = $initial_toolinfo_values{$toolinfoLabel}; 
		}
	}

	# look for a file by default (usually specified in the module)
	if(!defined($configType)){ $configType = "file"; }

	# toolinfo file expects a file
	if($configType eq "file"){ 
		# checks the toolinfoValue first
		return $self->getFirstMatchingFilePath($filename, $toolinfoValue); 
	}

	# toolinfo file expects a directory
	if($configType eq "dir"){ 

		# Makes the toolinfo value a directory if it's not already, then checks it first
		if(defined($toolinfoValue)){
			if(-f $toolinfoValue){
				$toolinfoValue = dirname($toolinfoValue); 
			}
		}

		return $self->getFirstMatchingFilePath($filename, $toolinfoValue); 
	}

	# toolinfo file expects a set of files that start with a particular substring
	if($configType eq "wildcard"){ 
		return $self->getFirstMatchingWildcardPath($filename, $toolinfoValue); 
	}

	die("Module " . $self->getPackage() . " specified an invalid config type \"$configType\"\n"); 
}

# looks for files or directories that match the provided filename, starting with the 
# entry provided in the toolinfo_file
sub getFirstMatchingFilePath { 
	my $self = shift ; 
	my $filename = shift ; 
	my $toolinfo_value = shift ; 

	# identify where to look
	my @paths = $self->mergePossiblePaths(); 

	my @possiblePaths ; 

	# place the toolinfo value first
	if(defined($toolinfo_value)){
		unshift(@possiblePaths, $toolinfo_value); 
	}

	# construct a set of paths to check
	foreach my $possiblePath (@paths){

        	# remove the trailing slash
                $possiblePath =~ s/\/+$// ;

		# construct the file path
                my $filePath = $possiblePath . "/" . $filename ;

                # remove duplicate slashes in "/path/to////some/file" -> "/path/to/some/file" 
                $filePath =~ s/\/\/+/\// ;

		push(@possiblePaths, $filePath); 
	}

	# check them, save the first match
	foreach my $testPath (@possiblePaths){
                # if the file does not exist, try the next      
                if(! -e $testPath){ next ; }

                # if this must be executed      
                if($self->mustExecute()){

                        # it should not be a directory, skip
                        if(-d $testPath){ next ; }

                        # if it's a standalone executable but not chmod +x, skip
                        if(!$self->getExecuteVia() && ! -x $testPath){ next ; }
                }

                # likely we have it
                $filename = $testPath ;
                last ;
	}	

	return $filename ; 
}

# looks for files or directories that match the provided starting substring, beginning with the 
# entry provided in the toolinfo_file
sub getFirstMatchingWildcardPath { 
	my $self = shift ; 
	my $filename = shift ; 
	my $toolinfo_value = shift ; 

	# remove wildcards from the toolinfo_value and filename. This code supplies it's own
	if($filename =~ /\*/){ $filename =~ s/\*//g } ; 
	if(defined($toolinfo_value) && $toolinfo_value =~ /\*/){ $toolinfo_value =~ s/\*//g } ; 

	# collect paths from the environment
	my @paths = $self->mergePossiblePaths(); 

	my @possiblePaths ; 

	# add the toolinfo supplied path first
	if(defined($toolinfo_value)){ push(@possiblePaths, $toolinfo_value); }

	# construct the set of possible paths
	foreach my $possiblePath ( @paths ){ 

		# remove the trailing slash
		$possiblePath =~ s/\/+$// ; 
	
		# construct the file path glob
		my $filePath = $possiblePath . "/" . $filename ; 	
                
		# remove duplicate slashes in "/path/to////some/file" -> "/path/to/some/file" 
                $filePath =~ s/\/\/+/\// ;

		push(@possiblePaths, $filePath); 		
	}
	

	# test to see if any are valid
	foreach my $testPath ( @possiblePaths ) { 

		# look for matching files
		my @files = glob($testPath . "*"); 

		if(@files < 1){ next ; }

		$filename = $testPath ; 
		last ; 
	}	

	return $filename ; 
}

# the newPathEnv variable contains all the paths necessary to find binaries that do not have a corresponding configLabel in the toolinfo file
# this method reports the $PATH necessary so they can be found by the workflow
sub getPathEnv { 

	my $currentPath = $ENV{'PATH'}; 

	my @paths = split(":", $currentPath); 

	push(@paths, @newPathEnv); 

	my %uniq_paths ; 
	foreach(@paths){ $uniq_paths{$_} = 1 ; }

	return keys %uniq_paths ; 
}


#### 
#### Methods for testing module
####

# compares version value to min/max/specific version, both supplied by the module
sub versionOK { 
	my $self = shift ; 

	my $version_raw = $self->getVersion(); 

	# if no version is set, then any version if fine :)
	if(!defined($version_raw)){ return TRUE; } 

	my $version = version->parse($version_raw); 

	if(defined($self->{min_version}) && defined($self->{max_version})){ 
		my $min_version = version->parse($self->{min_version}); 
		my $max_version = version->parse($self->{max_version}); 

		if($min_version > $max_version){ 
			print "Module error for " . ref($self) . 
				": Min (" . $self->{min_version} . ") and Max (" . $self->{max_version} . ") versions inverted.\n"; 
			exit 1 ; 
		}

		if($version >= $min_version && $version <= $max_version){ return TRUE ; }
	}

	if(defined($self->{required_versions}) && ref($self->{required_versions}) eq 'ARRAY'){

		if($#{$self->{required_versions}} == 0){ 
			if(!defined(${$self->{required_versions}}[0])){ 
				return TRUE ; 
			}
		}

		foreach my $thisVersion (@{$self->{required_versions}}){ 
			if($thisVersion && $version == version->parse($thisVersion)){ return TRUE ; }
		}
	}

	if(defined($self->{min_version}) && ! defined($self->{max_version})){ 
		my $min_version = version->parse($self->{min_version}); 
		if($version >= $min_version){ return TRUE ; }
	}

	if(defined($self->{max_version}) && ! defined($self->{min_version})){ 
		my $max_version = version->parse($self->{max_version}); 
		if($version <= $max_version){ return TRUE ; }
	}


	if(!defined($self->{min_version}) && !defined($self->{max_version})){

		if(!defined($self->{required_versions})){ 
			return TRUE ; 
		}

		if(defined($self->{required_versions})){
			if(ref($self->{required_versions}) ne 'ARRAY'){ return TRUE ; }

			if($#{$self->{required_versions}} == -1){ return TRUE ; }

			return FALSE ; 
		}

		return TRUE ; 
	}

	return FALSE ; 
}

# return the type of version error (this assumes there is an error, relying on a previous call to versionOK to identify the contition)
sub getVersionError { 
	my $self = shift ; 

	if($self->versionOK()){ return undef ; }

	my $version_err = "Version " . $self->getVersion() . " is not compatible." ; 

	if(defined($self->{min_version}) && defined($self->{max_version})){ 
		$version_err .= " Must be between v" . $self->{min_version} . " and " . $self->{max_version} ; 
	}

	if(defined($self->{min_version}) && ! defined($self->{max_version})){ 
		$version_err .= " Must be v" . $self->{min_version} . " or greater" ; 
	}

	if(defined($self->{max_version}) && ! defined($self->{min_version})){ 
		$version_err .= " Must be v" . $self->{max_version} . " or lower" ; 
	}

	if(defined($self->{required_versions})){
		$version_err .= " Must be one of the following: v" . join(', v' , @{$self->{required_versions}} ) ; 
	}

	return $version_err ; 
}

# checks that all the files required by the module are present in the specified path
sub filesetComplete { 
	my $self = shift ; 

	# if no fileset was defined this test succeeds automatically
	if(!defined($self->{fileset})){ return TRUE ; }

	# check that a path contains all specified files

	# collect the list of files
	my @files = (); 
	if(ref($self->{fileset}) eq 'ARRAY'){ 
		push(@files , @{$self->{fileset}}); 
	} else { 
		push(@files , $self->{fileset});
	}

	# confirm that the path is a directory
	if(! -d $self->getPath() ){ 
		push(
			@{$tool_status{$self->getPackage()}{errors}}, 
			"Supplied path \"" . $self->getPath() . 
			"\" is not a directory. Cannot contain required files: " . 
			join(', ', @files)
		); 
		return FALSE ; 
	}

	# identify any missing or unreadable files required by the set
	my @missing = ();
	my @unreadable = (); 
	foreach my $thisFile (@files){ 
		my $fullPath = $self->getPath() ; 
		$fullPath =~ s/\/+$//; 
		$fullPath .= "/" . $thisFile ; 

		if(!-e $fullPath){ 
			push(@missing, $thisFile); 
		} else { 
			if(!-r $fullPath){
				push(@unreadable, $thisFile); 
			}
		}
	}

	# record any issues in the tool status
	if($#missing > -1){ 
		push(
			@{$tool_status{$self->getPackage()}{errors}}, 
			"Supplied path \"" . $self->getPath() . "\" does not contain:\n\t" . 
			join("\n\t", sort @missing)
		); 
		return FALSE ;
	}

	# record any issues in the tool status
	if($#unreadable > -1){ 
		push(
			@{$tool_status{$self->getPackage()}{errors}}, 
			"Files at path \"" . $self->getPath() . "\" are not readable:\n\t" . 
			join("\n\t", @unreadable)
		); 
		return FALSE ; 
	}
	
	return TRUE ; 
}

# stub to provide a method modules can define for extended tests. May also be used to identify sets of files (per a wildcard config type)
sub functionTest { return TRUE ; }

# executes a command, saves exit code, stdout, stderr
sub execute { 
	my $self = shift ; 
	my $command = shift ; 

	# some command still return useful results with a non-zero exit code.
	my $ignoreNonZeroExit = shift ; 
	if(!$ignoreNonZeroExit){ $ignoreNonZeroExit = FALSE ; }

	if($self->getDebug()){ print "\n" . $command . "\n"; }

	# save stderr and stdout
	$command .= " 2>&1";

	my $result = `$command` ;
	my $exit = $? >> 8 ;

	if($self->getDebug() && $self->getDebug > 1){ print $result . "\n"; }

        if ( $exit != 0 && $ignoreNonZeroExit == FALSE ) {
		print "\nFailed to execute \"$command\":\n"; 
		if(defined($result)){ print $result . "\n"; } else { print "No output.\n"; }

		return undef ; 
        }
	return $result ;
}

# assumes no execution test if not specified
sub mustExecute { 
	my $self = shift ; 
	if(defined($self->{executable})){ return $self->{executable}; } else { return FALSE ; }
}

# how to execute a command ( via Java, Perl, Python, Base, etc )
sub getExecuteVia { 
	my $self = shift ; 
	if(defined($self->{execute_via})){ return $self->{execute_via}; } else { return FALSE ; }
}

# command line options for executing a command 
sub getExecuteViaOptions { 
	my $self = shift ; 
	if(defined($self->{execute_via_options})){ return $self->{execute_via_options}; } else { return "" ; }
}


####
#### Toolinfo file manipulation
####

# reads the toolinfo file into a hash
sub parseToolInfo { 
	my $self = shift ; 
	
	my $toolinfo = $self->getToolinfo();

	if(!$toolinfo){ return ; } 

	unless(open(TOOLINFO, $toolinfo)){ die "Unable to open \"$toolinfo\"."; } 

	while(<TOOLINFO>){
		my $line = $_ ; 
		chomp($line); 

		if($line eq ""){ next ; }
		if($line =~ /^#/){ next ; }

		my ($key, $value) = split(/=/, $line); 
		$initial_toolinfo_values{$key} = $value ; 
	}
}

sub getToolInfoValues { 
	my $self = shift ; 
	return %initial_toolinfo_values ; 
}

# rewrites the specified toolinfo file with module supplied paths
sub updateToolInfo { 
	my $self = shift ; 

	# make certain the user requested this and the file defined is usable
	my $toolinfo = $self->getToolinfo(); 

	if(!$toolinfo){ return ; } # not requested by the user

	if(!-e $toolinfo || ! -f $toolinfo || ! -r $toolinfo){ 
		print "Unable to update the toolinfo file at $toolinfo\n"; 
		return ;  
	}


	# collect all the toolinfo values from each completed module
	my %toolinfoValues ; 
	my %toolinfoValuesRecorded ; 

	foreach my $thisModule (@modules){ 
		my $module = $thisModule->new(); 

		my $label = $module->getConfigLabel(); 
		my $type = $module->getConfigType(); 

		if(defined($label) && defined($type)){

			if($tool_status{$module->getPackage()}{complete}){

				my $path = $tool_status{$module->getPackage()}{path} ; 

				# per module path customization
				$path = $module->getPackage()->getPathModification($path);

				if($type =~ /^dir/ && !-d $path){ $path = dirname($path); }

				$toolinfoValues{$label} = $path ; 

			} else { 
				$toolinfoValues{$label} = "" ; 
			}
		}
	}


	# open the current toolinfo file and collect the values
	my $new_toolinfo = ""; 
	
	unless(open(TOOLINFO,$toolinfo)){
		print "Unable to open $toolinfo\n"; 
		return ; 
	}

	while(<TOOLINFO>){
		my $line = $_ ; 
		chomp($line); 

		# if the line is a comment, pass it unchanged
		if($line =~ /^#/){ 
			$new_toolinfo .= $line . "\n"; 
			next ; 
		}

		# if the line doesn't include an assignment, pass it unchanged
		if($line !~ /=/){ 
			$new_toolinfo .= $line . "\n"; 
			next ; 
		}

		# if the line has an assignment that is overridden by the the module check, rewrite it
		# otherwise, pass it unchanged
		my ($key, $value) = split(/=/, $line); 
		if(defined($toolinfoValues{$key})){ 
			$new_toolinfo .= "${key}=" . $toolinfoValues{$key} . "\n"; 
			$toolinfoValuesRecorded{$key} = TRUE ; 
		} else { 
			$new_toolinfo .= "$line\n";
			$toolinfoValuesRecorded{$key} = TRUE ; 
		}
	}

	close(TOOLINFO); 


	# add values not already specified in the toolinfo file
	foreach my $key ( keys %toolinfoValues ){
		if(! $toolinfoValuesRecorded{$key} ){ 
			$new_toolinfo .= "${key}=" . $toolinfoValues{$key} . "\n"; 
			$toolinfoValuesRecorded{$key} = TRUE ; 
		}
	}


	# create a temp file for the new config
	$File::Temp::KEEP_ALL = 1;
	my ($fh, $tmpfile) = tempfile(); 
	print $fh $new_toolinfo ; 
	close($fh); 

	
	# create a backup of the original toolinfo file
	if(!-d "toolinfo_backup"){
		mkdir("toolinfo_backup"); 
	}

	my $backupPath ; 

	if(-d "toolinfo_backup"){
		$backupPath = "toolinfo_backup/${toolinfo}.bak"; 
	} else { 
		$backupPath = "${toolinfo}.bak"; 
	}

	my $n = 0 ; 
	while(-e $backupPath){

		if(-d "toolinfo_backup"){ 
			$backupPath = "toolinfo_backup/${toolinfo}.bak.${n}"; 
		} else { 
			$backupPath = "${toolinfo}.bak.${n}"; 
		}

		$n++ ; 
	}
	move($toolinfo, $backupPath); 

	
	# save the new config and remove the temporary copy
	copy($tmpfile, $toolinfo); 
	unlink($tmpfile)

}


####
#### Module get/set methods
####

# module name
sub getName { 
	my $self = shift ; 
	return $self->{name}; 
}

# file the module is looking for
sub getFilename { 
	my $self = shift ; 
	return $self->{filename}; 
}

# directory the module is looking for
sub getDirectory { 
	my $self = shift ; 
	return $self->{directory}; 
}

# module package name ( Tools::Verify::ModuleName )
sub getPackage { 
	my $self = shift ; 
	return $self->{package}; 
}

# tool description
sub getDescription { print "No description supplied for package\n"; }

# gives the module an opportunity to customize the path
sub getPathModification { my $self = shift ; my $path = shift ; return $path ; }

# the label use in toolinfo files
sub getConfigLabel { return undef ; }

# file, dir, wildcard ( the type of entry expected in the toolinfo file)
# file - a discrete file
# dir - a directory
# wildcard - a substring beginning at the start of the filename that matches a set of files in a directory
sub getConfigType { return undef ; }

# if no version is supplied by the module the version isn't considered
sub getVersion { return undef ; }

# locations to search for binaries
sub getSearchPath {
	my $self = shift ;
	return @searchPath;
}

# user supplied search path
sub setSearchPath {
	my $self = shift ; 

	my $searchPath = shift ; 

	if(defined($searchPath)){ 

		my @paths = split(/:/, $searchPath); 

		foreach my $thisPath (@paths){ 
			$thisPath =~ s/\/+$//; 
		}

		@searchPath = @paths ; 
	} 
}

# install directory path 
sub getPrefix {
	my $self = shift ; 

	if(!$self->{prefix}){ return ""; }

	return $self->{prefix} ;
}

# user supplied install prefix
sub setPrefix {
	my $self = shift ; 
	my $newPrefix = shift ; 

	if(!defined($newPrefix) || $newPrefix eq ""){
		print "Please specify a prefix path.\n"; 
		exit 1 ;
	}

	if(!-e $newPrefix){ 
		print "Specified Prefix \"$newPrefix\" does not exist.\n"; 
		exit 1 ;
	}

	if(!-d $newPrefix){ 
		print "Specified Prefix \"$newPrefix\" is not a directory.\n"; 
		exit 1 ;
	}

	print "newprefix: $newPrefix \n"; 

	$self->{prefix} = $newPrefix ; 
}

# workflow configuration file path
sub getToolinfo {
	my $self = shift ; 
	return $self->{toolinfo} ;
}

# user supplied toolinfo file path
sub setToolinfo {
	my $self = shift ; 
	my $newToolinfo = shift ; 

	if(!defined($newToolinfo) || $newToolinfo eq ""){
		print "Please specify a toolinfo.txt path.\n"; 
		exit 1 ;
	}

	if(!-e $newToolinfo){ 
		print "Specified ToolInfo file \"$newToolinfo\" does not exist.\n"; 
		exit 1 ;
	}

	if(!-f $newToolinfo){ 
		print "Specified ToolInfo \"$newToolinfo\" is not a file.\n"; 
		exit 1 ;
	}

	$self->{toolinfo} = $newToolinfo ; 
}

# debug mode
sub setDebug { 
	my $self = shift ; 
	$debug = shift ; 
}

sub getDebug { 
	my $self = shift ; 
	return $debug ; 
}

# flag determines whether to run a module's download method
sub enableDownload { 
	my $self = shift ; 
	$self->{download} = TRUE ; 
}

sub downloadOK { 
	my $self = shift ; 
	if(defined($self->{download})){
		return $self->{download} ; 
	}

	return FALSE ; 
}

# flag determines whether to run a module's install method
sub enableInstall { 
	my $self = shift ; 
	$self->{install} = TRUE ; 
}

sub installOK { 
	my $self = shift ; 
	if(defined($self->{install})){
		return $self->{install} ; 
	}

	return FALSE ; 
}

sub runDependencies { 
	my $self = shift ; 

	if(defined($self->{run_dependencies})){ 
		if(ref($self->{run_dependencies}) eq 'ARRAY'){
			return @{$self->{run_dependencies}}; 
		}
	}

	return (); 
}

sub buildDependencies { 
	my $self = shift ; 

	if(defined($self->{build_dependencies})){ 
		if(ref($self->{build_dependencies}) eq 'ARRAY'){
			return @{$self->{build_dependencies}}; 
		}
	}

	return (); 
}

1; 
