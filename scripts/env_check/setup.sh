
PATH=/usr/local/mirdeep2:$PATH
PATH=/usr/local/bowtie:$PATH
PATH=/usr/local/FastQC:$PATH
PATH=/usr/local/picard:$PATH
PATH=/usr/local/bedtools:$PATH
PATH=/usr/local/bin:$PATH
PATH=/usr/local:$PATH
PATH=/usr/local/vcftools:$PATH

export PATH
