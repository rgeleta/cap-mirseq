#!/bin/sh
# log_msg3 - write log messages with 3 parameters

# #####################################################################
# Description:
#   Writes formatted log messages from shell scripts to stdout
#
#   Message is formatted with 
#   ... a standard prefix (to make message stand out in log)
#   ... a date+time stamp 
#   ... up to three parameters (not edited) 
#
# Parameters:
#   @param string $1 calling script/program - name
#   @param string $2 calling script/program - script area locator number
#   @param string $3 message text
#
# @author    Robert Geleta www.rgeleta.com
# @copyright 2015 Robert Geleta 
# @license   cc BY-SA 3.0 US
# 
# #####################################################################
# Note: 
#   the marker "XXXXX" is for message visibility in the log
#   feel free to change it to suit your preferences
#
echo 'XXXXX'"\t"$(date '+%Y-%m-%d %H:%M:%S')"\t"'p='$1"\t"l=$2"\t"$3
# format used by: cat run.log | grep '*[program' > runtimes.tsv

# echo "XXXXX" $(date '+%Y-%m-%d %H:%M:%S') '*['$1']' $2 $3
# or with way more time detail that we need: echo "******" $(date '+%Y-%m-%d_%H:%M:%S:%N') $1 $2 $3
#
# end
