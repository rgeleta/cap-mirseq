#!/bin/bash
# consolidate all stats into one summary file

############################################################################
## Description:
## Generate a table summarizing various alignment and expression stats across samples
##
## Author: Jared Evans
## Date: 5/22/14
##
## Parameters:
## <output_dir> - Path to where CAP-miRSeq output is located
## <sample names(s1:s2)> - Colon seperated list of sample names
## <trim_adapter(YES/NO)> - YES or NO indicated whether CAP-miRSeq performed adapter trimming
##
############################################################################

if [ $# != 3 ];
then
        echo "usage: <workflow output_dir> <samples> <trim_adapter(YES/NO)>";
else
set -x
output_dir=$1
samples=$2
trim_adapter=$3
sample_count=$(echo $samples | tr ":" "\n" | wc -l)

header="Sample\tTotal Reads\tTrimmed Reads\tToo Short After Trimming(<17bps)\tReads sent to Aligner\tAligned Reads\tPrecursor miRNA Reads\tMature miRNA Reads\tKnown miRNA with >= 5x coverage"
if [ $trim_adapter == "NO" ]; then
	header="Sample\tReads sent to Aligner\tAligned Reads\tPrecursor miRNA Reads\tMature miRNA Reads\tKnown miRNA with >= 5x coverage"
fi

echo -e $header > $output_dir/SampleSummary.xls

for sample in $(echo $samples | tr ":" " ")
do
	if [ $trim_adapter == "YES" ]; then
		# adapter trimming stats
		file=$output_dir/fastqs/$sample.cutadapt.log
		total=$(grep "Processed reads:" $file | cut -d ":" -f2 | sed -e 's/^[\t]*//')
		trimmed=$(grep "Trimmed reads:" $file | cut -d ":" -f2 | sed -e 's/^[ \t]*//' | awk '{print $1}')
		tooshort=$(grep "Too short reads:" $file | cut -d ":" -f2 | sed -e 's/^[ \t]*//' | awk '{print $1}')
	fi

	# alignment stats
	file=$output_dir/bams/$sample.bowtie.log
	total_reads=$(grep "reads processed:" $file | cut -d ":" -f2 | sed -e 's/^[ \t]*//')
	aligned=$(grep "reads with at least one reported alignment:" $file | cut -d ":" -f2 | sed -e 's/^[ \t]*//' | awk '{print $1}')

	# expression stats
	file=$output_dir/expression/miRNA_expression_raw.xls
	i=$(echo $samples | tr ":" "\n" | grep -n -w $sample | cut -d":" -f1)
	let col=$i+3
	mature_reads=$(cat $file | cut -f$col | sed 1d | awk '{sum+=$1}END{frac=sum-int(sum); if(frac == 0){print sum}else{if(frac >= .5){print int(sum)+1}else{print int(sum)}}}' )
	known_mirna=$(cat $file | cut -f1,2,$col | sed 1d | awk '($3 >=5){print }' | sort | uniq | wc -l )
	# tricky to extract precursor read counts
	grep nowrap $output_dir/mirdeep2/$sample/expression_*.html | awk -F"\t" '{print $1}' | grep pdfs | awk -F"</a>" '{print $1}' | cut -d">" -f4 > $output_dir/$sample.precursor.reads.txt.tmp
	grep nowrap $output_dir/mirdeep2/$sample/expression_*.html | awk -F"\t" '{print $4}' | grep nowrap | cut -d">" -f2 | cut -d"<" -f1 | paste $output_dir/$sample.precursor.reads.txt.tmp - > $output_dir/$sample.precursor.reads.txt
	precursor_reads=$(cat $output_dir/$sample.precursor.reads.txt | awk '{sum+=$2}END{frac=sum-int(sum); if(frac == 0){print sum}else{if(frac >= .5){print int(sum)+1}else{print int(sum)}}}')
	rm $output_dir/$sample.precursor.reads.txt.tmp
	rm $output_dir/$sample.precursor.reads.txt

	if [ $trim_adapter == "YES" ]; then
		echo -e "$sample\t$total\t$trimmed\t$tooshort\t$total_reads\t$aligned\t$precursor_reads\t$mature_reads\t$known_mirna" >> $output_dir/SampleSummary.xls
	else
		echo -e "$sample\t$total_reads\t$aligned\t$precursor_reads\t$mature_reads\t$known_mirna" >> $output_dir/SampleSummary.xls
	fi
done

fi
